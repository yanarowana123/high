<?php

namespace App\Models;

use App\Traits\Multilingual;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory, Multilingual;

    protected $guarded = ['id'];

    const MOBILE = 0;
    const ADDRESS = 1;
    const TEXT = 2;
    const INTSA = 3;
    const YOUTUBE = 4;
    const FACEBOOK = 5;


    public function getTypeTitleAttribute()
    {
        switch ($this->attributes['type']) {
            case self::MOBILE:
                return 'Телефон';
                break;
            case self::ADDRESS:
                return 'Адрес';
                break;
            case self::TEXT:
                return 'Текст';
                break;
            case self::INTSA:
                return 'Instagram';
                break;
            case self::FACEBOOK:
                return 'Facebook';
                break;
            case self::YOUTUBE:
                return 'Youtube';
                break;
        }

        return $this->type;
    }


    public static function getTypeList()
    {
        return [
            self::MOBILE => 'Телефон',
            self::TEXT => 'Текст',
            self::ADDRESS => 'Адрес',
            self::INTSA => 'Instagram',
            self::FACEBOOK => 'Facebook',
            self::YOUTUBE => 'Youtube',
        ];
    }
}
