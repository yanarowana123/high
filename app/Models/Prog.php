<?php

namespace App\Models;

use App\Traits\Multilingual;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prog extends Model
{
    use HasFactory, Multilingual;

    const VIDEO = 0;
    const ART = 1;
    const GOOD = 2;
    const ADVANTAGE = 3;
    const IQ = 4;
    const ML = 5;
    const CORP = 6;
    const FEATURE = 7;
    const FOOTER = 8;

    protected $guarded = ['id'];
}
