<?php

namespace App\Models;

use App\Traits\Multilingual;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Engineer extends Model
{
    use HasFactory, Multilingual;

    protected $guarded = ['id'];
    const VIDEO = 0;
    const BLOCK1 = 1;
    const BLOCK2 = 2;
    const BLOCK3 = 3;
    const BLOCK4 = 4;
    const BLOCK5 = 5;
    const BLOCK6 = 6;
    const BLOCK7 = 7;
    const FEATURE = 8;
    const ADVANTAGE = 9;
    const FOOTER = 10;

}
