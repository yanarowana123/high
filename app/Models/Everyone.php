<?php

namespace App\Models;

use App\Traits\Multilingual;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Everyone extends Model
{
    use HasFactory, Multilingual;
    protected $guarded = ['id'];

    const VIDEO = 0;
    const FEATURE = 5;
    const BLOCK1 = 1;
    const BLOCK2 = 2;
    const BLOCK3 = 3;
    const BLOCK4 = 4;
    const FOOTER = 6;

}
