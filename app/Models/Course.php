<?php

namespace App\Models;

use App\Traits\Multilingual;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory, Multilingual;

    protected $guarded = ['id'];

    public function schedules()
    {
        return $this->hasMany(CourseSchedule::class);
    }
}
