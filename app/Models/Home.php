<?php

namespace App\Models;

use App\Traits\Multilingual;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    use HasFactory, Multilingual;

    const VIDEO = 0;
    const PROGRAM = 1;
    const WELCOME = 2;
    const HELP = 3;
    const FEATURE = 4;
    const FOOTER = 5;
    const LOGO = 6;

    protected $guarded = ['id'];


}
