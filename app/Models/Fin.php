<?php

namespace App\Models;

use App\Traits\Multilingual;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fin extends Model
{
    use HasFactory, Multilingual;

    const VIDEO = 0;
    const FEATURE = 1;
    const INFO = 2;
    const DEMAND = 3;
    const PROGS = 4;
    const PROGRAM = 5;
    const FOOTER = 6;

    protected $guarded = ['id'];
}
