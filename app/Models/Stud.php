<?php

namespace App\Models;

use App\Traits\Multilingual;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stud extends Model
{
    use HasFactory, Multilingual;
    protected $guarded = ['id'];
    const VIDEO = 0;
    const  TRUTH = 1;
    const  OFFER = 2;
    const  ADVANTAGE = 3;
    const HELP = 4;
    const  HIGH = 5;
    const  STUDENT = 6;
    const  GRADUATE = 7;
    const FEATURE = 8;
    const FOOTER = 9;

}
