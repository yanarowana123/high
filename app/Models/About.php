<?php

namespace App\Models;

use App\Traits\Multilingual;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    use HasFactory, Multilingual;
    protected $guarded = ['id'];
    const VIDEO = 0;
    const BEAUTY = 1;
    const EASY = 2;
    const DEV = 3;
    const AVAILABLE = 4;
    const FEATURE = 5;
    const STAFF = 6;
}
