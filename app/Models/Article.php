<?php

namespace App\Models;

use App\Traits\Multilingual;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory, Multilingual;

    const NEWS = 0;
    const ARTICLE = 1;
    protected $guarded = ['id'];

    public function contents()
    {
        return $this->hasMany(ArticleContent::class);
    }
}
