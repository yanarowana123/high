<?php

namespace App\Models;

use App\Traits\Multilingual;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory, Multilingual;
    protected $guarded = ['id'];

    const TABLETITLE = 0;
    const TABLEINFO = 1;
    const VIDEO = 2;
    const VIDEONEWS = 3;
}
