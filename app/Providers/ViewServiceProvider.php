<?php

namespace App\Providers;

use App\Models\Contact;
use App\Models\Home;
use App\Page\Page;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        View::composer(['layouts.main'], function ($view) {
            $contacts = \App\Models\Contact::all();
            $youtube = $contacts->where('type', Contact::YOUTUBE)->first();
            $facebook = $contacts->where('type', Contact::FACEBOOK)->first();
            $instagram = $contacts->where('type', Contact::INTSA)->first();
            $phones = $contacts->where('type', Contact::MOBILE);
            $addresses = $contacts->where('type', Contact::ADDRESS);
            $footer = Home::where('block', Home::FOOTER)->first();
            $logos = Home::where('block', Home::LOGO)->get();
            $pageTitle = $this->getPageTitle();
            $view->with(['phones' => $phones, 'addresses' => $addresses,
                'footer' => $footer, 'logos' => $logos,
                'pageTitle' => $pageTitle, 'instagram' => $instagram, 'youtube' => $youtube, 'facebook' => $facebook]);
        });

    }

    private function getPageTitle()
    {
        switch (\request()->route()->getName()) {
            case 'all':
                return __('main.Anyone who wants to know HM');
            case 'fin':
                return __('main.Financiers and Economists');
            case 'contacts':
                return __('main.Contacts');
            case 'math':
                return __('main.Mathematical Engineering');
            case 'news':
                return __('main.Articles and news');
            case 'prog':
                return __('main.Programmers');
            case 'about':
                return __('main.About school');
            case 'stud':
                return __('main.High school students');
        }
        return null;
    }
}
