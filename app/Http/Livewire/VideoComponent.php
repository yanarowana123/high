<?php

namespace App\Http\Livewire;

use App\Models\Home;
use App\Models\Schedule;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class VideoComponent extends Component
{
    use WithFileUploads;
    public $model;
    public $video;
    public $videoUrl;
    public $param;

    public function render()
    {
        return view('livewire.video-component')->layout('admin.layouts.app');
    }


    public function mount($name)
    {
        throw_unless(in_array(mb_strtoupper($name),
            ['HOME', 'ABOUT', 'ENGINEER', 'EVERYONE', 'FIN', 'PROG', 'STUD', 'SCHEDULE', 'NEWS']), NotFoundHttpException::class);
        if (mb_strtoupper($name) == 'SCHEDULE') {
            $this->model = Schedule::where('block', Schedule::VIDEO)->first();
            if (!$this->model) {
                $new = new Schedule();
                $new->block = Schedule::VIDEO;
                $new->save();
                $this->model = $new;
            }
        } elseif (mb_strtoupper($name) == 'NEWS') {
            $this->model = Schedule::where('block', Schedule::VIDEONEWS)->first();
            if (!$this->model) {
                $new = new Schedule();
                $new->block = Schedule::VIDEONEWS;
                $new->save();
                $this->model = $new;
            }
        } else {
            $this->model = ('App\\Models\\' . ucfirst($name))::where('block', Home::VIDEO)->first();
        }
        $this->videoUrl = $this->model->file;
        $this->param = $name;


    }

    public function store()
    {
        if ($this->video) {
            Storage::delete($this->videoUrl);
            $this->model->file = $this->video->store('uploads');
            $this->model->save();
        }

        session()->flash('message', 'Успех!');
        return redirect()->route('admin.video.index', $this->param);
    }
}
