<?php

namespace App\Http\Livewire\About;

use App\Models\About;
use App\Models\Engineer;
use App\Models\Everyone;
use App\Models\Fin;
use App\Models\Prog;
use App\Models\Schedule;
use App\Models\Stud;
use Livewire\Component;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultComponent extends Component
{
    public $model;
    public $title_ru;
    public $title_en;
    public $content_en;
    public $content_ru;

    protected $rules = [
        'title_ru' => 'nullable|string|max:255',
        'title_en' => 'nullable|string|max:255',
        'content_ru' => 'nullable|string',
        'content_en' => 'nullable|string',
    ];

    public function render()
    {
        return view('livewire.about.default-component')->layout('admin.layouts.app');
    }

    public function mount($model, $block)
    {

        throw_unless(in_array(mb_strtoupper($model), ['ABOUT', 'FIN', 'PROG', 'STUD', 'EVERYONE', 'ENGINEER', 'SCHEDULE']), NotFoundHttpException::class);
        if (mb_strtoupper($model) == 'ABOUT') {
            throw_unless(in_array(mb_strtoupper($block), [About::BEAUTY, About::EASY, About::DEV, About::AVAILABLE]), NotFoundHttpException::class);
            $item = About::where('block', $block)->first();
        } elseif (mb_strtoupper($model) == 'FIN') {
            throw_unless(in_array(mb_strtoupper($block), [Fin::INFO, Fin::DEMAND, Fin::PROGS]), NotFoundHttpException::class);
            $item = Fin::where('block', $block)->first();
        } elseif (mb_strtoupper($model) == 'PROG') {
            throw_unless(in_array(mb_strtoupper($block), [Prog::ART, Prog::GOOD, Prog::IQ, Prog::ML, Prog::CORP]), NotFoundHttpException::class);
            $item = PROG::where('block', $block)->first();
        } elseif (mb_strtoupper($model) == 'STUD') {
            throw_unless(in_array(mb_strtoupper($block), [Stud::TRUTH, Stud::OFFER, Stud::HELP, Stud::HIGH, Stud::STUDENT]), NotFoundHttpException::class);
            $item = Stud::where('block', $block)->first();
        } elseif (mb_strtoupper($model) == 'ENGINEER') {
            throw_unless(in_array(mb_strtoupper($block), [1, 2, 3, 5]), NotFoundHttpException::class);
            $item = ENGINEER::where('block', $block)->first();
        } elseif (mb_strtoupper($model) == 'EVERYONE') {
            throw_unless(in_array(mb_strtoupper($block), [1, 2, 3]), NotFoundHttpException::class);
            $item = EVERYONE::where('block', $block)->first();
        } elseif (mb_strtoupper($model) == 'SCHEDULE') {
            throw_unless(in_array(mb_strtoupper($block), [0]), NotFoundHttpException::class);
            $item = Schedule::where('block', $block)->first();
        }


        $this->model = $item;
        $this->title_ru = $item->title_ru;
        $this->title_en = $item->title_en;
        $this->content_ru = $item->content_ru;
        $this->content_en = $item->content_en;

    }

    public function store()
    {
        $this->model->update($this->validate());
        session()->flash('message', 'Успех!');
    }
}
