<?php

namespace App\Http\Livewire\About;

use App\Models\About;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class StaffComponent extends Component
{
    use WithFileUploads;
    public $employees = [];
    public $images = [];
    public $empIds = [];

    protected $rules = [
        'employees' => 'array|min:1',
        'employees.*.title_ru' => 'required|string|max:255',
        'employees.*.title_en' => 'required|string|max:255',
        'employees.*.content_ru' => 'required|string',
        'employees.*.content_en' => 'required|string',
    ];

    public function render()
    {

        return view('livewire.about.staff-component')->layout('admin.layouts.app');
    }

    public function mount()
    {
        $employees = About::where('block', About::STAFF)->get();
        if ($employees->isNotEmpty()) {
            $employees->each(function ($item) {
                $this->employees[] =
                    [
                        'id' => $item->id,
                        'title_ru' => $item->title_ru,
                        'title_en' => $item->title_en,
                        'content_ru' => $item->content_ru,
                        'content_en' => $item->content_en,
                        'file' => $item->file
                    ];
                $this->images[] = asset($item->file);

                $this->empIds[] = $item->id;
            })->toArray();

        } else {
            $this->add();
        }


//        foreach ($this->employees as $program) {
//            $this->programsIds[] = ['id' => $program['id']];
//        }


    }

    public function remove($key)
    {
        if (array_key_exists('id', $this->employees[$key])) {
            $emp = About::find($this->employees[$key]['id']);
            Storage::delete($emp->file);
            $emp->delete();

            unset($this->empIds[$key]);
            $this->empIds = array_values($this->empIds);
        }
        unset($this->employees[$key]);
        $this->employees = array_values($this->employees);
    }

    public function add()
    {
        $this->employees[] = [
            'title_ru' => '',
            'title_en' => '',
            'content_ru' => '',
            'content_en' => '',
            'file' => ''
        ];


        $this->emit('itemAdded', count($this->employees) - 1);

    }


    public function store()
    {
        $validatedData = $this->validate();
        foreach ($this->employees as $key => $emp) {
            if (array_key_exists('id', $emp)) {
                $old = About::find($this->empIds[$key]);
                if (!is_string($emp['file'])) {
                    Storage::delete($old->file);
                    $imagePath = $emp['file']->store('uploads');
                    $validatedData['employees'][$key]['file'] = $imagePath;
                }
                $old->update($validatedData['employees'][$key]);
                $old->save();
            } else {
                $new = new About();
                $new->title_ru = $emp['title_ru'];
                $new->title_en = $emp['title_en'];
                $new->content_ru = $emp['title_ru'];
                $new->content_en = $emp['title_en'];
                $new->image = $emp['image'];
                $new->block = About::STAFF;
                $new->save();
            }
        }
        session()->flash('message', 'Успех!');
        Storage::deleteDirectory('livewire-tmp');
        return redirect()->route('admin.about.staff');
    }


}
