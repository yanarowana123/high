<?php

namespace App\Http\Livewire\About;

use App\Models\About;
use Livewire\Component;

class FeatureComponent extends Component
{
    public $features = [];

    protected $rules = [
        'features' => 'array|min:1',
        'features.*.title_ru' => 'required|string|max:255',
        'features.*.title_en' => 'required|string|max:255',
    ];


    public function render()
    {
        return view('livewire.about.feature-component')->layout('admin.layouts.app');
    }

    public function mount()
    {
        $features = About::where('block', About::FEATURE)->get();
        if ($features->isNotEmpty()) {
            $features->each(function ($item) {
                $this->features[] =
                    [
                        'id' => $item->id,
                        'title_ru' => $item->title_ru,
                        'title_en' => $item->title_en,
                    ];
            })->toArray();

        } else {
            $this->add();
        }
    }

    public function remove($key)
    {
        if (array_key_exists('id', $this->features[$key])) {
            About::find($this->features[$key]['id'])->delete();
        }
        unset($this->features[$key]);
        $this->features = array_values($this->features);
    }

    public function add()
    {
        $this->features[] = [
            'title_ru' => '',
            'title_en' => ''
        ];
    }

    public function store()
    {
        $validatedData = $this->validate();

        foreach ($this->features as $key => $feature) {
            if (array_key_exists('id', $feature)) {
                $old = About::find($this->features[$key]['id']);
                $old->update($validatedData['features'][$key]);
                $old->save();
            } else {
                $new = new About();
                $new->title_ru = $feature['title_ru'];
                $new->title_en = $feature['title_en'];
                $new->block = About::FEATURE;
                $new->save();
            }
        }

        session()->flash('message', 'Успех!');
        return redirect()->route('admin.about.feature');

    }


}
