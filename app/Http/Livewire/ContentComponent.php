<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ContentComponent extends Component
{
    public $contents;
    public $model;


    public function render()
    {
        return view('livewire.content-component')->layout('admin.layouts.app');
    }

    public function mount($model)
    {
        throw_unless(in_array(mb_strtoupper($model), ['STUD', 'STUDENT', 'ENGINEER', 'ENG', 'EVERYONE']), NotFoundHttpException::class);
        if ($this->model == 'eng') {
            $this->model = 'App\\Models\\' . ucfirst(mb_strtolower('ENGINEER'));
        } elseif ($this->model == 'student') {
            $this->model = 'App\\Models\\' . ucfirst(mb_strtolower('STUD'));
        } else {
            $this->model = 'App\\Models\\' . ucfirst(mb_strtolower($model));
        }
        $this->param = mb_strtolower($model);
        if (mb_strtoupper($model) == 'STUD')
            $contents = $this->model::where('block', $this->model::GRADUATE)->get();
        if (mb_strtoupper($model) == 'STUDENT')
            $contents = $this->model::where('block', $this->model::ADVANTAGE)->get();
        if (mb_strtoupper($model) == 'ENGINEER')
            $contents = $this->model::where('block', $this->model::BLOCK4)->get();
        if (mb_strtoupper($model) == 'ENG')
            $contents = $this->model::where('block', $this->model::ADVANTAGE)->get();
        if (mb_strtoupper($model) == 'EVERYONE')
            $contents = $this->model::where('block', $this->model::BLOCK4)->get();

        $this->contents = $contents->toArray();
    }


    public function store()
    {
        foreach ($this->contents as $content) {
            $updContent = $this->model::find($content['id']);
            $updContent->update($content);
        }
        session()->flash('message', 'Успех!');

    }
}
