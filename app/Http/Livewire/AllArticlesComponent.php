<?php

namespace App\Http\Livewire;

use App\Models\Article;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AllArticlesComponent extends Component
{
    public $search;
    public $type;


    public function render()
    {
        $articles = $this->getAll();

        return view('livewire.all-articles-component', compact('articles'))->layout('admin.layouts.app');
    }

    protected function getAll()
    {
        if ($this->type == 'articles') {
            if ($this->search) {
                return Article::where('type', Article::ARTICLE)
                    ->where('title_ru', 'like', '%' . $this->search . '%')
                    ->orWhere('title_en', 'like', '%' . $this->search . '%')->paginate(10);
            }
            return Article::where('type', Article::ARTICLE)->paginate(10);
        } else {
            if ($this->search) {
                return Article::where('type', Article::NEWS)
                    ->where('title_ru', 'like', '%' . $this->search . '%')
                    ->orWhere('title_en', 'like', '%' . $this->search . '%')->paginate(10);
            }
            return Article::where('type', Article::NEWS)->paginate(10);
        }
    }

    public function mount($type)
    {
        throw_unless(in_array($type, ['articles', 'news']), NotFoundHttpException::class);
        $this->type = $type;
    }

    public function delete($id)
    {
        $article = Article::find($id);
        Storage::delete($article->image);
        $article->delete();
        session()->flash('message', 'Успех!');
    }

}
