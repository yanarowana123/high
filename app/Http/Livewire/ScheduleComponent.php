<?php

namespace App\Http\Livewire;

use App\Models\CourseSchedule;
use Livewire\Component;

class ScheduleComponent extends Component
{
    public $schedules;
    public $courseId;

    protected $rules = [
        'schedules' => 'array|min:1',
        'schedules.*.title_ru' => 'required|string|max:255',
        'schedules.*.title_en' => 'required|string|max:255',
        'schedules.*.content_ru' => 'required|string|max:255',
        'schedules.*.content_en' => 'required|string|max:255',
        'schedules.*.course_id' => 'nullable|integer',
    ];

    public function render()
    {
        return view('livewire.schedule-component')->layout('admin.layouts.app');
    }

    public function mount($course)
    {
        $this->courseId = $course;
        $schedules = CourseSchedule::where('course_id', $course)->get();
        if ($schedules->isNotEmpty()) {
            $schedules->each(function ($item) {
                $this->schedules[] =
                    [
                        'id' => $item->id,
                        'title_ru' => $item->title_ru,
                        'title_en' => $item->title_en,
                        'content_ru' => $item->content_ru,
                        'content_en' => $item->content_en,
                    ];
            })->toArray();

        } else {
            $this->add();
        }
    }


    public function remove($key)
    {
        if (array_key_exists('id', $this->schedules[$key])) {
            CourseSchedule::find($this->schedules[$key]['id'])->delete();
        }
        unset($this->schedules[$key]);
        $this->schedules = array_values($this->schedules);
    }

    public function add()
    {
        $this->schedules[] = [
            'title_ru' => '',
            'title_en' => '',
            'content_ru' => '',
            'content_en' => '',
        ];
    }

    public function store()
    {
        $validatedData = $this->validate();

        foreach ($this->schedules as $key => $schedule) {
            if (array_key_exists('id', $schedule)) {
                $old = CourseSchedule::find($this->schedules[$key]['id']);
                $validatedData['schedules'][$key]['course_id'] = $this->courseId;
                $old->update($validatedData['schedules'][$key]);
                $old->save();
            } else {
                $new = new CourseSchedule();
                $new->title_ru = $schedule['title_ru'];
                $new->title_en = $schedule['title_en'];
                $new->content_ru = $schedule['content_ru'];
                $new->content_en = $schedule['content_en'];
                $new->course_id = $this->courseId;
                $new->save();
            }
        }

        session()->flash('message', 'Успех!');
        return redirect()->route('admin.course.index');

    }

}
