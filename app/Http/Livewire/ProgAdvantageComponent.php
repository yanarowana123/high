<?php

namespace App\Http\Livewire;

use App\Models\Prog;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class ProgAdvantageComponent extends Component
{
    use WithFileUploads;
    public $advantages;

    protected $rules = [
        'advantages' => 'array',
        'advantages.*.title_ru' => 'required|string|max:255',
        'advantages.*.title_en' => 'required|string|max:255',
        'advantages.*.content_ru' => 'required|string',
        'advantages.*.content_en' => 'required|string',
        'advantages.*.image' => 'nullable|image|max:2048'
    ];

    public function render()
    {
        return view('livewire.prog-advantage-component')->layout('admin.layouts.app');
    }

    public function mount()
    {

        $advantages = Prog::where('block', Prog::ADVANTAGE)->get();
        $this->advantages = $advantages->toArray();

        foreach ($this->advantages as $key => $adv) {
            $this->advantages[$key]['image'] = '';
            $this->advantages[$key]['id'] = (string)$adv['id'];
        }

//        $advantages->each(function ($item){
//           $this->advantages[] = [
//               'id' => $item['id'],
//               'title'
//           ];
//        });
    }

    public function store()
    {
        $validatedData = $this->validate();
        foreach ($this->advantages as $key => $advantage) {
            $updAdv = Prog::find((int)$advantage['id']);
            if ($advantage['image'] != '') {
                Storage::delete($advantage['file']);
                $imagePath = $advantage['image']->store('uploads');
                $validatedData['advantages'][$key]['file'] = $imagePath;
            }
            $validatedData['advantages'][$key]['block'] = Prog::ADVANTAGE;
            $updAdv->update($validatedData['advantages'][$key]);
        }

        session()->flash('message', 'Успех!');

        return redirect()->route('admin.prog.advantage');
    }
}
