<?php

namespace App\Http\Livewire\Home;

use App\Models\Home;
use Livewire\Component;

class FooterComponent extends Component
{
    public $content_ru;
    public $content_en;
    public $footer;
    protected $rules = [
        'content_ru' => 'required|string',
        'content_en' => 'required|string',
    ];

    public function render()
    {
        return view('livewire.home.footer-component')->layout('admin.layouts.app');
    }

    public function mount()
    {
        $footer = Home::where('block', Home::FOOTER)->first();
        $this->footer = $footer;
        $this->content_ru = $footer->content_ru;
        $this->content_en = $footer->content_en;
    }

    public function store()
    {
        $this->footer->update($this->validate());
        session()->flash('message', 'Успех!');
    }
}
