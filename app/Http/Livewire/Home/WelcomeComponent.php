<?php

namespace App\Http\Livewire\Home;

use App\Models\Home;
use Livewire\Component;

class WelcomeComponent extends Component
{
    public $welcome;
    public $title_ru;
    public $title_en;
    public $text_ru;
    public $text_en;
    public $content_ru;
    public $content_en;
    public $block;
    protected $rules = [
        'title_ru' => 'required|string|max:255',
        'title_en' => 'required|string|max:255',
        'text_ru' => 'required|string|max:255',
        'text_en' => 'required|string|max:255',
        'content_ru' => 'required|string',
        'content_en' => 'required|string',
        'block' => 'required|integer',
    ];

    public function render()
    {
        return view('livewire.home.welcome-component')->layout('admin.layouts.app');
    }

    public function mount()
    {
        $welcome = Home::where('block', Home::WELCOME)->first();
        $this->exist = 0;
        $this->block = Home::WELCOME;
        if ($welcome) {
            $this->title_ru = $welcome->title_ru;
            $this->title_en = $welcome->title_en;
            $this->text_ru = $welcome->text_ru;
            $this->text_en = $welcome->text_en;
            $this->content_ru = $welcome->content_ru;
            $this->content_en = $welcome->content_en;
            $this->welcome = $welcome;
        }
    }

    public function store()
    {
        if ($this->welcome) {
            $this->welcome->update($this->validate());
        } else {
            Home::create($this->validate());
        }
        session()->flash('message', 'Успех!');
    }


}
