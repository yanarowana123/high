<?php

namespace App\Http\Livewire\Home;

use App\Models\Home;

use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\TemporaryUploadedFile;
use Livewire\WithFileUploads;

class ProgramComponent extends Component
{
    public $programs = [];
    public $programsIds = [];

    use WithFileUploads;

    public function render()
    {
        return view('livewire.home.program-component')->layout('admin.layouts.app');
    }


    protected $rules = [
        'programs' => 'array|min:1',
        'programs.*.title_ru' => 'required|string|max:255',
        'programs.*.title_en' => 'required|string|max:255',
        'programs.*.icon' => 'nullable|image|max:2048',
    ];

    protected $listeners = [
        'submitForm' => 'save'
    ];


    public function mount()
    {

        $programs = Home::where('block', Home::PROGRAM)->get();

        if ($programs->isNotEmpty()) {
            $programs->each(function ($item) {
                $this->programs[] = [
                    'id' => $item->id,
                    'title_ru' => $item->title_ru,
                    'title_en' => $item->title_en,
                    'icon' => '',
                    'icon_url' => $item->file,
                ];
            })->toArray();
        } else {
            $this->programs[] = [
                'id' => '',
                'title_ru' => '',
                'title_en' => '',
                'icon' => '',
            ];
        }

        foreach ($this->programs as $program) {
            $this->programsIds[] = ['id' => $program['id']];
        }
    }

    public function add()
    {
        $this->programs[] = [
            'title_ru' => '',
            'title_en' => '',
            'icon' => '',
        ];

        $this->programsIds[] = ['id' => ''];
    }

    public function remove($key)
    {
        if (array_key_exists('id', $this->programs[$key])) {
            $program = Home::find($this->programs[$key]['id']);
            Storage::delete($program->file);
            $program->delete();
            unset($this->programsIds[$key]);
            $this->programsIds = array_values($this->programsIds);
        }
        unset($this->programs[$key]);
        $this->programs = array_values($this->programs);
    }

    public function save()
    {
//        dd($this);
        $this->validate();
        foreach ($this->programs as $key => $program) {
            if (array_key_exists('id', $program)) {
                $oldProgram = Home::where('id', $this->programsIds[$key])->first();
                $oldProgram->title_ru = $program['title_ru'];
                $oldProgram->title_en = $program['title_en'];
                if ($program['icon'] != '') {
                    $iconPath = $program['icon']->store('uploads');
                    Storage::delete($oldProgram->file);
                    $oldProgram->file = $iconPath;
                }
                $oldProgram->save();
            } else {

                $newProgram = new Home();
                $newProgram->title_ru = $program['title_ru'];
                $newProgram->title_en = $program['title_en'];
                if ($program['icon'] != '') {
                    $iconPath = $program['icon']->store('uploads');
                    $newProgram->file = $iconPath;
                }
                $newProgram->block = Home::PROGRAM;
                $newProgram->save();
            }
        }
        session()->flash('message', 'Успех!');
        Storage::deleteDirectory('livewire-tmp');
        return redirect()->route('admin.home.program');
    }

}
