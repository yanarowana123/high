<?php

namespace App\Http\Livewire\Home;

use App\Models\Home;
use Livewire\Component;

class HelpComponent extends Component
{
    public $help;
    public $title_ru;
    public $title_en;
    public $content_ru;
    public $content_en;
    public $block;

    public function render()
    {
        return view('livewire.home.help-component')->layout('admin.layouts.app');;
    }

    protected $rules = [
        'title_ru' => 'required|string|max:255',
        'title_en' => 'required|string|max:255',
        'content_ru' => 'required|string',
        'content_en' => 'required|string',
        'block' => 'required|integer',
    ];


    public function mount()
    {
        $help = Home::where('block', Home::HELP)->first();
        $this->exist = 0;
        $this->block = Home::HELP;
        if ($help) {
            $this->title_ru = $help->title_ru;
            $this->title_en = $help->title_en;
            $this->content_ru = $help->content_ru;
            $this->content_en = $help->content_en;
            $this->help = $help;
        }
    }

    public function store()
    {
        if ($this->help) {
            $this->help->update($this->validate());
        } else {
            Home::create($this->validate());
        }
        session()->flash('message', 'Успех!');
    }

}
