<?php

namespace App\Http\Livewire\Home;

use App\Models\Home;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class LogoComponent extends Component
{
    use WithFileUploads;
    public $logos = [];
    public $logosIds = [];

    protected $rules = [
        'image' => 'nullable|image|max:2048'
    ];

    public function render()
    {
        return view('livewire.home.logo-component')->layout('admin.layouts.app');
    }

    public function mount()
    {
        $logos = Home::where('block', Home::LOGO)->get();

        if ($logos->isNotEmpty()) {
            $logos->each(function ($item) {
                $this->logos[] = [
                    'id' => $item->id,
                    'image_url' => $item->file,
                    'image' => ''
                ];
                $this->logosIds[] = $item->id;
            })->toArray();

        } else {
            $this->add();
        }


    }

    public function add()
    {
        $this->logos[] = [
            'image' => ''
        ];
        $this->logosIds[] = ['id' => ''];

    }

    public function remove($key)
    {
        if (array_key_exists('id', $this->logos[$key])) {
            $logo = Home::find($this->logos[$key]['id']);
            Storage::delete($logo->file);
            $logo->delete();
            unset($this->logosIds[$key]);
            $this->logosIds = array_values($this->logosIds);
        }
        unset($this->logos[$key]);
        $this->logos = array_values($this->logos);
    }

    public function store()
    {
        foreach ($this->logos as $key => $logo) {
            if (array_key_exists('id', $logo)) {
                $oldLogo = Home::where('id', $this->logosIds[$key])->first();
                if ($logo['image'] != '') {
                    Storage::delete($oldLogo->file);
                    $oldLogo->file = $logo['image']->store('uploads');
                    $oldLogo->save();
                }

            } else {
                if ($logo['image'] != '') {
                    $newLogo = new Home();
                    $newLogo->block = Home::LOGO;
                    $newLogo->file = $logo['image']->store('uploads');
                    $newLogo->save();
                }

            }
        }

        session()->flash('message', 'Успех!');
        return redirect()->route('admin.home.logo');
    }
}
