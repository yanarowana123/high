<?php

namespace App\Http\Livewire\Home;

use App\Models\Engineer;
use App\Models\Fin;
use App\Models\Home;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FeatureComponent extends Component
{

    public $features = [];
    public $model;
    public $param;

    public function render()
    {
        return view('livewire.home.feature-component')->layout('admin.layouts.app');
    }


    protected $rules = [
        'features' => 'array|min:1',
        'features.*.title_ru' => 'required|string|max:255',
        'features.*.title_en' => 'required|string|max:255',
        'features.*.content_ru' => 'required|string',
        'features.*.content_en' => 'required|string',
        'features.*.block' => 'required|integer',
    ];

    public function mount($model)
    {
        throw_unless(in_array(mb_strtoupper($model), ['HOME', 'FIN', 'PROG', 'STUD', 'EVERYONE', 'ENGINEER', 'ENG']), NotFoundHttpException::class);
        if (strtoupper($model) == strtoupper('eng')) {
            $this->model = 'App\\Models\\' . ucfirst(mb_strtolower('engineer'));
        } else {
            $this->model = 'App\\Models\\' . ucfirst(mb_strtolower($model));
        }
        $this->param = mb_strtolower($model);
        if (strtoupper($model) == strtoupper('engineer')) {
            $features = Engineer::where('block', Engineer::ADVANTAGE)->get();
        } elseif (strtoupper($model) == strtoupper('home')) {
            $features = $this->model::where('block', $this->model::FEATURE)->get();
        } else {
            $features = $this->model::where('block', $this->model::FOOTER)->get();
        }
        if ($features->isNotEmpty()) {
            $features->each(function ($item) {
                $this->features[] = [
                    'id' => $item->id,
                    'title_ru' => $item->title_ru,
                    'title_en' => $item->title_en,
                    'content_ru' => $item->content_ru,
                    'content_en' => $item->content_en,
                    'block' => $item->block,
                ];
            })->toArray();
        } else {
            $this->add();
        }
    }

    public function remove($key)
    {
        if (array_key_exists('id', $this->features[$key])) {
            $feature = $this->model::find($this->features[$key]['id']);
            $feature->delete();
        }
        unset($this->features[$key]);
        $this->features = array_values($this->features);
    }

    public function add()
    {
        $this->features[] = [
            'title_ru' => '',
            'title_en' => '',
            'content_ru' => '',
            'content_en' => '',
            'block' => $this->model::FEATURE,
        ];
    }

    public function store()
    {
        $validatedData = $this->validate();
        foreach ($this->features as $key => $feature) {
            if (array_key_exists('id', $feature)) {
                $oldProgram = $this->model::where('id', $feature['id'])->first();
                $oldProgram->update($validatedData['features'][$key]);
            } else {
                $this->model::create($validatedData['features'][$key]);
            }
        }

        session()->flash('message', 'Успех!');
        return redirect()->route('admin.feature', $this->param);

    }


}
