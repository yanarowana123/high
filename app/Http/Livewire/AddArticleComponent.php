<?php

namespace App\Http\Livewire;

use App\Models\Article;
use App\Models\ArticleContent;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Validator;


class AddArticleComponent extends Component
{
    public $article;
    public $title_ru;
    public $title_en;
    public $slug;
    public $content_ru;
    public $content_en;
    public $image;
    public $imageUrl;
    public $type;

    public $articleContents = [];

    use WithFileUploads;

//    protected $rules = [
//        'title_ru' => 'required|string|max:255',
//        'title_en' => 'required|string|max:255',
//        'slug' => 'required|string|max:255|unique:articles,slug' ,
//        'content_ru' => 'required',
//        'content_en' => 'required',
//        'image' => 'nullable|image|max:4096',
//        'type' => 'required|integer|digits:1',
//        'articleContents' => 'array|min:1',
//        'articleContents.*.title_ru' => 'required|string|max:255',
//        'articleContents.*.title_en' => 'required|string|max:255',
//        'articleContents.*.content_ru' => 'required',
//        'articleContents.*.content_en' => 'required',
//    ];

    public function render()
    {
        $this->slug = Str::slug($this->title_en);
        return view('livewire.add-article-component')->layout('admin.layouts.app');
    }

    public function mount(Article $article)
    {
        if (!empty($article->getAttributes())) {
            $this->article = $article;
            $this->title_ru = $article->title_ru;
            $this->title_en = $article->title_en;
            $this->slug = $article->slug;
            $this->content_ru = $article->content_ru;
            $this->content_en = $article->content_en;
            $this->imageUrl = $article->image;
            $this->type = $article->type;
            foreach ($article->contents as $content) {
                $this->articleContents[] = [
                    'id' => (string)$content['id'],
                    'title_ru' => $content['title_ru'],
                    'title_en' => $content['title_en'],
                    'content_ru' => $content['content_ru'],
                    'content_en' => $content['content_en'],
                ];
            }
        } else {
            $this->type = '';
        }
    }

    public function add()
    {
        $this->articleContents[] = [
            'title_ru' => '',
            'title_en' => '',
            'content_ru' => '',
            'content_en' => ''
        ];

        $this->emit('itemAdded', count($this->articleContents) - 1);
    }

    public function remove($key)
    {
        if (array_key_exists('id', $this->articleContents[$key])) {
            ArticleContent::find((int)$this->articleContents[$key]['id'])->delete();
        }
        unset($this->articleContents[$key]);
        array_values($this->articleContents);
    }

    public function store(Request $request)
    {
        if ($this->article) {
            $validatedData = Validator::make(collect($this)->toArray(),
                [
                    'title_ru' => 'required|string|max:255',
                    'title_en' => 'required|string|max:255',
                    'slug' => 'required|string|max:255|unique:articles,slug,' . $this->article->id,
                    'content_ru' => 'required',
                    'content_en' => 'required',
                    'image' => 'nullable',
                    'type' => 'required|integer|digits:1',
                    'articleContents' => 'nullable|array',
                    'articleContents.*.title_ru' => 'nullable|string|max:255',
                    'articleContents.*.title_en' => 'nullable|string|max:255',
                    'articleContents.*.content_ru' => 'nullable|string',
                    'articleContents.*.content_en' => 'nullable|string',
                ], [
                    'title_ru.required' => 'Введите титул рус',
                    'title_en.required' => 'Введите титул англ',
                    'slug.required' => 'Введите слаг',
                    'content_ru.required' => 'Введите контент ру',
                    'content_en.required' => 'Введите контент англ',
                    'image.required' => 'Введите картинку',
                    'type.required' => 'Введите тип',
                ]
            )->validate();
        } else {
            $validatedData = Validator::make(collect($this)->toArray(),
                [
                    'title_ru' => 'required|string|max:255',
                    'title_en' => 'required|string|max:255',
                    'slug' => 'required|string|max:255|unique:articles',
                    'content_ru' => 'required',
                    'content_en' => 'required',
                    'image' => 'required',
                    'type' => 'required|integer|digits:1',
                    'articleContents' => 'nullable|array',
                    'articleContents.*.title_ru' => 'nullable|string|max:255',
                    'articleContents.*.title_en' => 'nullable|string|max:255',
                    'articleContents.*.content_ru' => 'nullable|string',
                    'articleContents.*.content_en' => 'nullable|string',
                ], [
                    'title_ru.required' => 'Введите титул рус',
                    'title_en.required' => 'Введите титул англ',
                    'slug.required' => 'Введите слаг',
                    'content_ru.required' => 'Введите контент ру',
                    'content_en.required' => 'Введите контент англ',
                    'image.required' => 'Введите картинку',
                    'type.required' => 'Введите тип',
                ]
            )->validate();
        }


        if ($this->article) {
            if ($this->image) {
                Storage::delete($this->imageUrl);
                $imagePath = $this->image->store('uploads');
                $validatedData['image'] = $imagePath;
            } else {
                $validatedData['image'] = $this->imageUrl;
            }
            $this->article->update($validatedData);
            foreach ($this->articleContents as $key => $articleContent) {
                if (array_key_exists('id', $articleContent)) {
                    ArticleContent::find($articleContent['id'])->update($articleContent);
                } else {

                }
            }
        } else {
            $imagePath = $validatedData['image']->store('uploads');

            $validatedData['image'] = $imagePath;
            $newArticle = Article::create($validatedData);

            foreach ($this->articleContents as $key => $articleContent) {
                $validatedData['articleContents'][$key]['article_id'] = $newArticle->id;
                ArticleContent::create($validatedData['articleContents'][$key]);
            }
        }
        session()->flash('message', 'Успех!');
        Storage::deleteDirectory('livewire-tmp');
        return redirect()->back();
    }


}

