<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\CourseCategory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use PhpParser\Node\Stmt\DeclareDeclare;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $courses = Course::latest()->paginate(10);

        return view('admin.course.index',
            [
                'courses' => $courses
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('admin.course.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return RedirectResponse
     */
    public function store()
    {
        Course::create($this->validateCategory());
        return redirect()
            ->route('admin.course.index')
            ->with('success', __('position.messages.success_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Course $course
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Course $course)
    {
        return view('admin.course.edit',
            [
                'course' => $course
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CourseCategory $category
     * @return RedirectResponse
     */
    public function update(Course $course)
    {
        $course->update($this->validateCategory());
        return redirect()
            ->route('admin.course.index')
            ->with('success', __('position.messages.success_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Course $course
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Course $course)
    {
        $course->delete();
        return redirect()->route('admin.course.index');
    }

    protected function validateCategory()
    {
        return request()->validate(
            [
                'title_en' => 'nullable|string|max:255',
                'title_ru' => 'nullable|string|max:255',
                'hours' => 'nullable|string|max:255',
                'price_month' => 'nullable|string|max:255',
                'price_year' => 'nullable|string|max:255',
                'course_category_id' => 'required|integer',
            ]
        );
    }
}
