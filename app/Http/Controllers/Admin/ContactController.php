<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\ContactInformation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ContactController extends Controller
{
    public function index()
    {
        Storage::deleteDirectory('livewire-tmp');

        return view('admin.contact.index',
            [
                'contacts' => Contact::all()]);
    }

    public function create()
    {
        return view('admin.contact.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'content_ru' => 'nullable|string|max:255',
            'content_en' => 'nullable|string|max:255',
            'type' => 'nullable|integer|digits:1',
        ]);

        Contact::create($validatedData);

        return redirect()->route('admin.contact.index');

    }

    public function edit(Contact $contact)
    {
        return view('admin.contact.edit',
            [
                'contact' => $contact
            ]);

    }

    public function update(Request $request, Contact $contact)
    {
        $validatedData = $request->validate([
            'content_ru' => 'nullable|string|max:255',
            'content_en' => 'nullable|string|max:255',
            'type' => 'nullable|integer',
        ]);

        $contact->update($validatedData);

        return redirect()->route('admin.contact.index');
    }

    public function delete(Contact $contact)
    {
        $contact->delete();
        return redirect()->route('admin.contact.index');
    }

}
