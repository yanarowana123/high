<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CourseCategory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use PhpParser\Node\Stmt\DeclareDeclare;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $categories = CourseCategory::latest()->paginate(10);

        return view('admin.category.index',
            [
                'categories' => $categories
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return RedirectResponse
     */
    public function store()
    {
        CourseCategory::create($this->validateCategory());
        return redirect()
            ->route('admin.category.index')
            ->with('success', __('position.messages.success_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param CourseCategory $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(CourseCategory $category)
    {
        return view('admin.category.edit',
            [
                'category' => $category
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CourseCategory $category
     * @return RedirectResponse
     */
    public function update(CourseCategory $category)
    {
        $category->update($this->validateCategory());
        return redirect()
            ->route('admin.category.index')
            ->with('success', __('position.messages.success_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param CourseCategory $category
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(CourseCategory $category)
    {
        $category->delete();
        return redirect()->route('admin.category.index');
    }

    protected function validateCategory()
    {
        return request()->validate(
            [
                'title_en' => 'required|string|max:255',
                'title_ru' => 'required|string|max:255',
            ]
        );
    }
}
