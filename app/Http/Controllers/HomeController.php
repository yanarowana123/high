<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Article;
use App\Models\Contact;
use App\Models\CourseCategory;
use App\Models\Engineer;
use App\Models\Everyone;
use App\Models\Fin;
use App\Models\Home;
use App\Models\Prog;
use App\Models\Schedule;
use App\Models\Stud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class HomeController extends Controller
{
    public function index()
    {
        $video = Home::where('block', Home::VIDEO)->first()->file;
        $welcome = Home::where('block', Home::WELCOME)->first();
        $help = Home::where('block', Home::HELP)->first();
        $features = Home::where('block', Home::FEATURE)->get();
        $footer = Home::where('block', Home::FOOTER)->first();
        $logos = Home::where('block', Home::LOGO)->get();
        return view('pages.index', compact('video', 'welcome', 'help', 'features', 'footer', 'logos'));
    }

    public function about()
    {
        $video = About::where('block', Home::VIDEO)->first()->file;
        $dev = About::where('block', About::DEV)->first();
        $beauty = About::where('block', About::BEAUTY)->first();
        $easy = About::where('block', About::EASY)->first();
        $available = About::where('block', About::AVAILABLE)->first();
        $features = Home::where('block', Home::FEATURE)->get();
        $staff = About::where('block', About::STAFF)->get();
        $aboutFeatures = About::where('block', About::FEATURE)->get();


        return view('pages.about', compact('aboutFeatures', 'video', 'dev', 'beauty', 'easy', 'available', 'features', 'staff'));
    }


    public function fin()
    {
        $video = Fin::where('block', Fin::VIDEO)->first()->file;
        $features = Home::where('block', Home::FEATURE)->get();
        $info = Fin::where('block', Fin::INFO)->first();
        $demand = Fin::where('block', Fin::DEMAND)->first();
        $progs = Fin::where('block', Fin::PROGS)->first();
        $courses = Fin::where('block', Fin::FOOTER)->get();

        return view('pages.fin', compact('video', 'features', 'info', 'demand', 'progs', 'courses'));
    }


    public function stud()
    {
        $video = Stud::where('block', Stud::VIDEO)->first()->file;
        $truth = Stud::where('block', Stud::TRUTH)->first();
        $offer = Stud::where('block', Stud::OFFER)->first();
        $advantages = Stud::where('block', Stud::ADVANTAGE)->get();
        $help = Stud::where('block', Stud::HELP)->first();
        $high = Stud::where('block', Stud::HIGH)->first();
        $students = Stud::where('block', Stud::STUDENT)->first();
        $graduates = Stud::where('block', Stud::GRADUATE)->get();
        $features = Home::where('block', Home::FEATURE)->get();
        $courses = Stud::where('block', Stud::FOOTER)->get();


        return view('pages.stud', compact('video', 'truth', 'offer', 'features',
            'advantages', 'help', 'high', 'students', 'graduates', 'courses'));
    }

    public function math()
    {
        $video = Engineer::where('block', Stud::VIDEO)->first()->file;
        $features = Home::where('block', Home::FEATURE)->get();
        $block1 = Engineer::where('block', Engineer::BLOCK1)->first();
        $block2 = Engineer::where('block', Engineer::BLOCK2)->first();
        $block3 = Engineer::where('block', Engineer::BLOCK3)->first();
        $block4 = Engineer::where('block', Engineer::BLOCK4)->get();
        $block5 = Engineer::where('block', Engineer::BLOCK5)->first();
        $advantages = Engineer::where('block', Engineer::ADVANTAGE)->get();
        $courses = Engineer::where('block', Engineer::FOOTER)->get();


        return view('pages.math', compact('courses', 'video', 'features', 'block1', 'block2', 'block3', 'block4', 'block5', 'advantages'));
    }


    public function all()
    {
        $video = Everyone::where('block', Everyone::VIDEO)->first()->file;
        $features = Home::where('block', Home::FEATURE)->get();
        $block1 = Everyone::where('block', Everyone::BLOCK1)->first();
        $block2 = Everyone::where('block', Everyone::BLOCK2)->first();
        $block3 = Everyone::where('block', Everyone::BLOCK3)->first();
        $block4 = Everyone::where('block', Everyone::BLOCK4)->get();
        $courses = Everyone::where('block', Everyone::FOOTER)->get();


        return view('pages.all', compact('courses', 'video', 'features', 'block1', 'block2', 'block3', 'block4'));
    }

    public function contacts()
    {
        $contacts = Contact::all();
        $phones = $contacts->where('type', Contact::MOBILE);
        $addresses = $contacts->where('type', Contact::ADDRESS);
        $texts = $contacts->where('type', Contact::TEXT);
        $features = Home::where('block', Home::FEATURE)->get();

        return view('pages.maps', compact('phones', 'addresses', 'texts', 'features'));
    }

    public function schedule()
    {
        $video = Schedule::where('block', Schedule::VIDEO)->first()->file;
        $categories = CourseCategory::with('courses', 'courses.schedules')->get();
        $features = Home::where('block', Home::FEATURE)->get();
        $content = Schedule::where('block', Schedule::TABLETITLE)->first();
        return view('pages.schedule', compact('categories', 'features', 'content', 'video'));
    }


    public function prog()
    {
        $video = Prog::where('block', Prog::VIDEO)->first()->file;
        $features = Home::where('block', Home::FEATURE)->get();
        $block1 = Prog::where('block', Prog::ART)->first();
        $block2 = Prog::where('block', Prog::GOOD)->first();
        $block3 = Prog::where('block', Prog::IQ)->first();
        $block4 = Prog::where('block', Prog::ML)->first();
        $block5 = Prog::where('block', Prog::CORP)->first();
        $advantages1 = Prog::where('block', Prog::ADVANTAGE)->take(3)->get();
        $advantages2 = Prog::where('block', Prog::ADVANTAGE)->orderBy('id', 'DESC')->take(3)->get();
        $courses = Prog::where('block', Prog::FOOTER)->get();

        return view('pages.prog', compact('courses', 'video', 'features', 'block1', 'block2', 'block3', 'block4', 'block5',
            'advantages1', 'advantages2'));
    }

    public function news()
    {
        $video = Schedule::where('block', Schedule::VIDEONEWS)->first() ? Schedule::where('block', Schedule::VIDEONEWS)->first()->file : null;
        $articles = Article::latest()->paginate(10);
//        return view('pages.news_tmp');
        return view('pages.news', compact('articles', 'video'));
    }

    public function newsShow($slug)
    {
        throw_unless($article = Article::with('contents')->where('slug', $slug)->first(), NotFoundHttpException::class);

        $similarArticles = Article::where('id', '<>', $article->id)->latest()->take(4)->get();
//        if ($article->type == Article::NEWS) {
//            $similarArticles = Article::where(['type', Article::NEWS])
//                ->where('id', '<>', $article->id)->latest()->take(4)->get();
//        } else {
//            $similarArticles = Article::where('type', Article::ARTICLE)
//                ->where('id', '<>', $article->id)->latest()->take(4)->get();
//        }

//        return view('pages.news-in', compact('article'));
        return view('pages.news-in', compact('article', 'similarArticles'));
    }

}
