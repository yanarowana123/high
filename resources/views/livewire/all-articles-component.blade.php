<div>
    <div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
    </div>
    <div class="form-group">
        <input type="text" class="form-control" wire:model="search"
               placeholder="Поиск"
        >
    </div>

    <div class="card">
        <div class="card-header">
            <div class="d-flex justify-content-between mt-3">
                <h2>{{$type=='articles'?'Статьи':'Новости'}}</h2>
                <a href="{{route('admin.article.create')}}" class="btn btn-success mb-2">Добавить</a>

            </div>
        </div>
        <div class="card-body">
            <div class="table table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Заголовок</th>
                        <th scope="col">Дата создания</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody class="">
                    @foreach($articles as $index=>$article)
                        <tr>
                            <th scope="row"><a href="{{route('admin.article.edit',$article)}}">{{$index+1}} </a>
                            </th>
                            <td>
                                <a href="{{route('admin.article.edit',$article)}}">{{$article->title}}</a>
                            </td>
                            <td>
                                <a href="{{route('admin.article.edit',$article)}}">{{$article->created_at->format('d.m.Y H:i')}}</a>
                            </td>

                            <td>
                                <a href="{{route('admin.article.edit',$article)}}"><i
                                        class="fa fa-pen fa-fw"></i></a>
                            </td>
                            <td>
                                <a onclick="return confirm('Are you sure?')"
                                   href="#"
                                   wire:click="delete({{$article->id}})"><i
                                        class="fa fa-trash fa-fw"></i></a>
                            </td>

                        </tr>

                    @endforeach
                    </tbody>

                </table>

            </div>

        </div>
        <div class="card-footer">
            {{ $articles->links() }}
        </div>
    </div>


</div>
