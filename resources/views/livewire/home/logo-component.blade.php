<div>
    <div class="card">
        <div class="card-header">
            <h1>Преимущества</h1>
        </div>

        <form wire:submit.prevent="store">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div>
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
            </div>

            @foreach($logos as $key=>$logo)
                <div class="card-body  border border-bottom-1">
                    <div class="d-flex align-items-center">
                        <h3>Логотип {{$key+1}}</h3>
                        <i class="fa fa-trash fa-fw ml-5 text-danger"
                           style="cursor:pointer"
                           wire:click="remove({{$key}})"></i>
                    </div>

                    <div class="form-group d-flex flex-column">


                        @if ($logos[$key]['image'])
                            Photo Preview:
                            <img src="{{ $logos[$key]['image']->temporaryUrl() }}"
                                 style="max-width: 400px;object-fit: contain">
                        @elseif(array_key_exists('image_url',$logos[$key]))
                            <img src="{{asset($logos[$key]['image_url'])}}"
                                 style="max-width: 400px;object-fit: contain">
                        @endif

                        <input type="file" wire:model="logos.{{$key}}.image">
                        @error("logos.$key.image")
                        <div class="alert alert-danger">{{ $message }}</div> @enderror


                    </div>


                </div>
            @endforeach

            <div class="card-footer">
                <div class="form-group">
                    <div class="control">
                        <button class="btn btn-success" type="submit">Сохранить</button>
                        <button class="btn btn-outline-success" wire:click.prevent="add">+</button>
                    </div>
                </div>
            </div>
        </form>

    </div>

</div>

@push('scripts')
    <script>
        Livewire.on('submitForm', () => {
            // alert('f');
            // $("html, body").animate({scrollTop: 0}, "slow");
        })

    </script>
@endpush
