<div>

    <div class="card">
        <div class="card-header">
            <h1>Подвал</h1>
        </div>
        <form wire:submit.prevent="store">
            <div class="card-body">
                <div>
                    @if (session()->has('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label class="label " for="content_ru">
                        Контент
                    </label>
                    <div class="control">
                        <textarea wire:model="content_ru"
                                  class="form-control  @error('content_ru') is-invalid @enderror"
                                  required></textarea>
                    </div>

                    @error("content_ru")
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="label " for="content_en">
                        Content
                    </label>
                    <div class="control">
                        <textarea wire:model="content_en"
                                  class="form-control  @error('content_en') is-invalid @enderror"
                                  required></textarea>
                    </div>

                    @error("content_en")
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>


            </div>

            <div class="card-footer">
                <div class="form-group">
                    <div class="control">
                        <button class="btn btn-success" type="submit">Сохранить</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
