<div>

    <div class="card">
        <div class="card-header">
        </div>
        <form wire:submit.prevent="store" novalidate>
            <div class="card-body">
                <div>
                    @if (session()->has('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label class="label " for="title_ru">
                        Заголовок
                    </label>
                    <div class="control">
                        <input type="text"
                               class="form-control @error("title_ru") is-invalid @enderror"
                               wire:model="title_ru"
                               required>
                    </div>

                    @error("title_ru")
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="label " for="title_en">
                        Title
                    </label>
                    <div class="control">
                        <input type="text"
                               class="form-control @error("title_en") is-invalid @enderror"
                               wire:model="title_en"
                               required>
                    </div>

                    @error("title_en")
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>


                <div class="form-group" wire:ignore>
                    <label class="label " for="content_ru">
                        Контент
                    </label>
                    <div class="control">
                        <textarea wire:model="content_ru"
                                  id="content_ru"
                                  class="form-control  @error('content_ru') is-invalid @enderror"
                                  required></textarea>
                    </div>

                    @error("content_ru")
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group" wire:ignore>
                    <label class="label " for="content_en">
                        Content
                    </label>
                    <div class="control">
                        <textarea wire:model="content_en"
                                  {{--                                  wire:model.debounce.9999999ms="description"--}}
                                  id="content_en"
                                  class="form-control  @error('content_en') is-invalid @enderror"
                                  required></textarea>
                    </div>

                    @error("content_en")
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>


            </div>

            <div class="card-footer">
                <div class="form-group">
                    <div class="control">
                        <button class="btn btn-success" type="submit">Сохранить</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>

@push('scripts')
    <script>
        let config = {
            filebrowserImageBrowseUrl: '/elfinder/ckeditor',
            allowedContent: true,
            removeFormatAttributes: ''
        };
        CKEDITOR.replace('content_en', config);
        CKEDITOR.replace('content_ru', config);
        CKEDITOR.instances['content_en'].on('change', function (e) {
        @this.set('content_en', e.editor.getData());
        });
        CKEDITOR.instances['content_ru'].on('change', function (e) {
        @this.set('content_ru', e.editor.getData());
        });
    </script>
@endpush
