<div>
    <div class="card">
        <div class="card-header">
            <h1>Сотрудники</h1>
        </div>

        <form wire:submit.prevent="store">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div>
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
            </div>

            @foreach($employees as $key=>$emp)

                <div class="card-body  border border-bottom-1">
                    <div class="d-flex align-items-center">
                        <h3>Сотрудник {{$key+1}}</h3>
                        <i class="fa fa-trash fa-fw ml-5 text-danger"
                           style="cursor:pointer"
                           wire:click="remove({{$key}})"></i>
                    </div>


                    <div class="form-group">
                        <label class="label " for="title_ru">
                            Имя
                        </label>
                        <div class="control">
                            <input type="text"
                                   class="form-control @error("employees.$key.title_ru") is-invalid @enderror"
                                   wire:model="employees.{{$key}}.title_ru"
                                   required>
                        </div>

                        @error("employees.$key.title_ru")
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="label " for="title">
                            Name
                        </label>
                        <div class="control">
                            <input type="text"
                                   class="form-control @error("employees.$key.title_en") is-invalid @enderror"
                                   wire:model="employees.{{$key}}.title_en"
                                   required>
                        </div>

                        @error("employees.$key.title_en")
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>


                    <div class="form-group" wire:ignore>
                        <label class="label " for="content_ru">
                            Контент
                        </label>
                        <div class="control">
                        <textarea wire:model="employees.{{$key}}.content_ru"
                                  id="content_ru{{$key}}"
                                  class="form-control  @error("employees.$key.content_ru") is-invalid @enderror"
                                  required></textarea>
                        </div>

                        @error("employees.$key.content_ru")
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group" wire:ignore>
                        <label class="label " for="content_en">
                            Content
                        </label>
                        <div class="control">
                        <textarea wire:model="employees.{{$key}}.content_en"
                                  id="content_en{{$key}}"
                                  class="form-control  @error("employees.$key.content_en") is-invalid @enderror"
                                  required></textarea>
                        </div>

                        @error("employees.$key.content_en")
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>


                    <div class="form-group" wire:ignore>
                        <input type="file"
                               wire:model="employees.{{$key}}.file"
                               name="" id="image{{$key}}">
                    </div>
                </div>
            @endforeach

            <div class="card-footer">
                <div class="form-group">
                    <div class="control">
                        <button
                            wire:loading.attr="disabled"
                            class="btn btn-success" type="submit">Сохранить
                        </button>
                        <button class="btn btn-outline-success" wire:click.prevent="add">+</button>
                        <div wire:loading>
                            Один момент...
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>

</div>


@push('scripts')
    <script>
        let config = {
            filebrowserImageBrowseUrl: '/elfinder/ckeditor',
            allowedContent: true,
            removeFormatAttributes: ''
        };
        let images =
            <?php
            echo json_encode($images);
            ?>


        for (let i = 0; i <={{$key}}; i++) {

            CKEDITOR.replace('content_ru' + i, config);
            CKEDITOR.replace('content_en' + i, config);

            CKEDITOR.instances['content_en' + i].on('change', function (e) {
            @this.set('employees.' + i + '.content_en', e.editor.getData());
            });
            CKEDITOR.instances['content_ru' + i].on('change', function (e) {
            @this.set('employees.' + i + '.content_ru', e.editor.getData());
            });

            $("#image" + i).fileinput({
                theme: "fas",
                showUpload: false,
                showRemove: false,
                showCancel: false,
                showClose: false,
                initialPreview: [
                    '<img src="' + images[i] + '" class="file-preview-image" alt="" title="" style="width:100%;height:100%" title="thumbnail">'

                ]
            });
        }

        Livewire.on('itemAdded', (index) => {

            CKEDITOR.replace('content_ru' + index, config);
            CKEDITOR.replace('content_en' + index, config);

            CKEDITOR.instances['content_en' + index].on('change', function (e) {
            @this.set('employees.' + index + '.content_en', e.editor.getData());
            });
            CKEDITOR.instances['content_ru' + index].on('change', function (e) {
            @this.set('employees.' + index + '.content_ru', e.editor.getData());
            });
            $("#image" + index).fileinput({
                theme: "fas",
                showUpload: false,
                showRemove: false,
                showCancel: false,
                showClose: false,
            });
        })

    </script>
@endpush
