<div>
    <div class="card">
        <div class="card-header">
            <h1>Программы</h1>
        </div>

        <form wire:submit.prevent="store">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div>
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
            </div>

            @foreach($features as $key=>$feature)
                <div class="card-body  border border-bottom-1">
                    <div class="d-flex align-items-center">
                        <h3>Преимущество {{$key+1}}</h3>
                        <i class="fa fa-trash fa-fw ml-5 text-danger"
                           style="cursor:pointer"
                           wire:click="remove({{$key}})"></i>
                    </div>


                    <div class="form-group">
                        <label class="label " for="title_ru">
                            Контент
                        </label>
                        <div class="control">
                            <input type="text"
                                   class="form-control @error("features.$key.title_ru") is-invalid @enderror"
                                   wire:model="features.{{$key}}.title_ru"
                                   required>
                        </div>

                        @error("features.$key.title_ru")
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="label " for="title">
                            Content
                        </label>
                        <div class="control">
                            <input type="text"
                                   class="form-control @error("features.$key.title_en") is-invalid @enderror"
                                   wire:model="features.{{$key}}.title_en"
                                   required>
                        </div>

                        @error("features.$key.title_en")
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                </div>
            @endforeach

            <div class="card-footer">
                <div class="form-group">
                    <div class="control">
                        <button class="btn btn-success" type="submit">Сохранить</button>
                        <button class="btn btn-outline-success" wire:click.prevent="add">+</button>
                    </div>
                </div>
            </div>
        </form>

    </div>

</div>

@push('scripts')
    <script>
        Livewire.on('submitForm', () => {
            // alert('f');
            // $("html, body").animate({scrollTop: 0}, "slow");
        })

    </script>
@endpush
