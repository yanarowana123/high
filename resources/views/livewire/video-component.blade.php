<div>
    <div class="card">
        <div class="card-header">
            <h1>Видео</h1>
        </div>
        <form wire:submit.prevent="store">
            <div class="card-body">
                <div>
                    @if (session()->has('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif
                </div>


                @if($videoUrl)
                    <video class="main__video w-100" autoplay="" muted="" loop="" id="myVideo">
                        <source src="{{asset($videoUrl)}}" type="video/mp4">
                    </video>
                @endif

                <div class="form-group">
                    <label class="label " for="title_ru">
                        Видео
                    </label>
                    <div class="control">
                        <input type="file" wire:model="video">
                    </div>
                    @error("video")
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>


            </div>

            <div class="card-footer">
                <div class="form-group">
                    <div class="control">
                        <button wire:loading.attr="disabled" class="btn btn-success" type="submit">Сохранить</button>
                        <div wire:loading>
                            Загрузка видео...
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
