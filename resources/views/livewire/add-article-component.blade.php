<div>

    <div class="card">
        <div class="card-header">
            <h1>Новость/Статья</h1>
        </div>

        <form wire:submit.prevent="store">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div>
                    @if (session()->has('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label class="label " for="title_ru">
                        Заголовок
                    </label>
                    <div class="control">
                        <input type="text"
                               class="form-control @error("title_ru") is-invalid @enderror"
                               wire:model="title_ru"
                               required>
                    </div>

                    @error("title_ru")
                    <div class="alert alert-danger">Ошибка</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="label " for="title">
                        Title
                    </label>
                    <div class="control">
                        <input type="text"
                               class="form-control @error("title_en") is-invalid @enderror"
                               wire:model="title_en"
                               required>
                    </div>

                    @error("title_en")
                    <div class="alert alert-danger"> Англ ошибка</div>
                    @enderror
                </div>

                <div class="form-group" wire:ignore>
                    <input type="file"

                           wire:model="image"
                           name="image" id="image">
                </div>
                <div class="form-group" wire:ignore>
                    <label class="label " for="content_ru">
                        Контент
                    </label>
                    <div class="control">
                        <textarea wire:model="content_ru"
                                  id="content_ru"
                                  class="form-control  @error("content_ru") is-invalid @enderror"
                                  required></textarea>
                    </div>

                    @error("content_ru")
                    <div class="alert alert-danger">Контент ошибка</div>
                    @enderror
                </div>

                <div class="form-group" wire:ignore>
                    <label class="label " for="content_en">
                        Content
                    </label>
                    <div class="control">
                        <textarea wire:model="content_en"
                                  id="content_en"
                                  class="form-control  @error("content_en") is-invalid @enderror"
                                  required></textarea>
                    </div>

                    @error("content_en")
                    <div class="alert alert-danger">Англ конт ошибка</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="label" for="course_category_id">
                        Тип
                    </label>
                    <div class="control">
                        <select class="form-control" wire:model="type" required>
                            <option value="" selected disabled>Выберите тип контента</option>
                            <option value="0">Новость</option>
                            <option value="1">Статья</option>
                        </select>
                    </div>
                    @error('type')
                    <div class="alert alert-danger">тип ошибка</div>
                    @enderror
                </div>


{{--                @foreach($articleContents as $key=>$content)--}}

{{--                    <div class="card-body  border border-bottom-1">--}}
{{--                        <div class="d-flex align-items-center">--}}
{{--                            <h3>Абзац {{$key+1}}</h3>--}}
{{--                            <i class="fa fa-trash fa-fw ml-5 text-danger"--}}
{{--                               style="cursor:pointer"--}}
{{--                               wire:click="remove({{$key}})"></i>--}}
{{--                        </div>--}}


{{--                        <div class="form-group">--}}
{{--                            <label class="label " for="title_ru">--}}
{{--                                Заголовок--}}
{{--                            </label>--}}
{{--                            <div class="control">--}}
{{--                                <input type="text"--}}
{{--                                       class="form-control @error("articleContents.$key.title_ru") is-invalid @enderror"--}}
{{--                                       wire:model="articleContents.{{$key}}.title_ru"--}}
{{--                                       required>--}}
{{--                            </div>--}}

{{--                            @error("articleContents.$key.title_ru")--}}
{{--                            <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                            @enderror--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <label class="label " for="title">--}}
{{--                                Title--}}
{{--                            </label>--}}
{{--                            <div class="control">--}}
{{--                                <input type="text"--}}
{{--                                       class="form-control @error("articleContents.$key.title_en") is-invalid @enderror"--}}
{{--                                       wire:model="articleContents.{{$key}}.title_en"--}}
{{--                                       required>--}}
{{--                            </div>--}}

{{--                            @error("articleContents.$key.title_en")--}}
{{--                            <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                            @enderror--}}
{{--                        </div>--}}


{{--                        <div class="form-group" wire:ignore>--}}
{{--                            <label class="label " for="content_ru">--}}
{{--                                Контент--}}
{{--                            </label>--}}
{{--                            <div class="control">--}}
{{--                        <textarea wire:model="articleContents.{{$key}}.content_ru"--}}
{{--                                  id="content_ru{{$key}}"--}}
{{--                                  class="form-control  @error("articleContents.$key.content_ru") is-invalid @enderror"--}}
{{--                                  required></textarea>--}}
{{--                            </div>--}}

{{--                            @error("articleContents.$key.content_ru")--}}
{{--                            <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                            @enderror--}}
{{--                        </div>--}}

{{--                        <div class="form-group" wire:ignore>--}}
{{--                            <label class="label " for="content_en">--}}
{{--                                Content--}}
{{--                            </label>--}}
{{--                            <div class="control">--}}
{{--                        <textarea wire:model="articleContents.{{$key}}.content_en"--}}
{{--                                  id="content_en{{$key}}"--}}
{{--                                  class="form-control  @error("articleContents.$key.content_en") is-invalid @enderror"--}}
{{--                                  required></textarea>--}}
{{--                            </div>--}}

{{--                            @error("articleContents.$key.content_en")--}}
{{--                            <div class="alert alert-danger">{{ $message }}</div>--}}
{{--                            @enderror--}}
{{--                        </div>--}}


{{--                    </div>--}}

{{--                @endforeach--}}

            </div>

            <div class="card-footer">
                <div class="form-group">
                    <div class="control">
                        <button
                            wire:loading.attr="disabled"
                            class="btn btn-success" type="submit">Сохранить
                        </button>
                        <button class="btn btn-outline-success" wire:click.prevent="add">+</button>
                        <div wire:loading>
                            Один момент...
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>

</div>

@push('scripts')
    <script>
        CKEDITOR.plugins.addExternal('ckeditor_wiris', 'https://ckeditor.com/docs/ckeditor4/4.15.1/examples/assets/plugins/ckeditor_wiris/', 'plugin.js');

        var mathElements = [
            'math',
            'maction',
            'maligngroup',
            'malignmark',
            'menclose',
            'merror',
            'mfenced',
            'mfrac',
            'mglyph',
            'mi',
            'mlabeledtr',
            'mlongdiv',
            'mmultiscripts',
            'mn',
            'mo',
            'mover',
            'mpadded',
            'mphantom',
            'mroot',
            'mrow',
            'ms',
            'mscarries',
            'mscarry',
            'msgroup',
            'msline',
            'mspace',
            'msqrt',
            'msrow',
            'mstack',
            'mstyle',
            'msub',
            'msup',
            'msubsup',
            'mtable',
            'mtd',
            'mtext',
            'mtr',
            'munder',
            'munderover',
            'semantics',
            'annotation',
            'annotation-xml'
        ];

        let config = {
            filebrowserImageBrowseUrl: '/elfinder/ckeditor',
            allowedContent: true,
            extraPlugins: 'ckeditor_wiris',

            removePlugins: 'uploadimage,uploadwidget,uploadfile,filetools,filebrowser',
            extraAllowedContent: mathElements.join(' ') + '(*)[*]{*};img[data-mathml,data-custom-editor,role](Wirisformula)'
        };

        CKEDITOR.replace('content_ru', config);
        CKEDITOR.replace('content_en', config);

        CKEDITOR.instances['content_en'].on('change', function (e) {
        @this.set('content_en', e.editor.getData());
        });
        CKEDITOR.instances['content_ru'].on('change', function (e) {
        @this.set('content_ru', e.editor.getData());
        });

        $("#image").fileinput({
            theme: "fas",
            showUpload: false,
            showRemove: false,
            showCancel: false,
            showClose: false,
            @if($this->imageUrl)
            initialPreview: [
                '<img src="{{asset($this->imageUrl)}}" class="file-preview-image" alt="" title="" style="width:100%;height:100%" title="thumbnail">'

            ]
            @endif
        });

            @if(isset($key))
        for (let i = 0; i <= {{$key}}; i++) {

            CKEDITOR.replace('content_ru' + i, config);
            CKEDITOR.replace('content_en' + i, config);

            CKEDITOR.instances['content_en' + i].on('change', function (e) {
            @this.set('articleContents.' + i + '.content_en', e.editor.getData());
            });
            CKEDITOR.instances['content_ru' + i].on('change', function (e) {
            @this.set('articleContents.' + i + '.content_ru', e.editor.getData());
            });

        }
        @endif

        Livewire.on('itemAdded', (index) => {

            CKEDITOR.replace('content_ru' + index, config);
            CKEDITOR.replace('content_en' + index, config);

            CKEDITOR.instances['content_en' + index].on('change', function (e) {
            @this.set('articleContents.' + index + '.content_en', e.editor.getData());
            });
            CKEDITOR.instances['content_ru' + index].on('change', function (e) {
            @this.set('articleContents.' + index + '.content_ru', e.editor.getData());
            });
        })

    </script>
@endpush
