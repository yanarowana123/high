<div>
    <div class="card">
        <div class="card-header">
            <h1>Преимущества</h1>
        </div>

        <form wire:submit.prevent="store">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div>
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
            </div>

            @foreach($advantages as $key=>$advantage)
                <div class="card-body  border border-bottom-1">
                    <div class="d-flex align-items-center">
                        <h3>Преимущество {{$key+1}}</h3>
                    </div>


                    <div class="form-group">
                        <label class="label " for="title_ru">
                            Наименование
                        </label>
                        <div class="control">
                            <input type="text"
                                   class="form-control @error("advantages.$key.title_ru") is-invalid @enderror"
                                   wire:model="advantages.{{$key}}.title_ru"
                                   required>
                        </div>

                        @error("advantages.$key.title_ru")
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="label " for="title">
                            Title
                        </label>
                        <div class="control">
                            <input type="text"
                                   class="form-control @error("advantages.$key.title_en") is-invalid @enderror"
                                   wire:model="advantages.{{$key}}.title_en"
                                   required>
                        </div>

                        @error("advantages.$key.title_en")
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group" wire:ignore>
                        <label class="label " for="content_ru">
                            Контент
                        </label>
                        <div class="control">
                        <textarea wire:model="advantages.{{$key}}.content_ru"
                                  id="content_ru{{$key}}"
                                  class="form-control  @error("advantages.$key.content_ru") is-invalid @enderror"
                                  required></textarea>
                        </div>

                        @error("advantages.$key.content_ru")
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group" wire:ignore>
                        <label class="label " for="content_en">
                            Content
                        </label>
                        <div class="control">
                        <textarea wire:model="advantages.{{$key}}.content_en"
                                  id="content_en{{$key}}"
                                  class="form-control  @error("advantages.$key.content_en") is-invalid @enderror"
                                  required></textarea>
                        </div>

                        @error("advantages.$key.content_en")
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>


                    <div class="form-group d-flex flex-column">
                        <label class="label d-block" for="icon">
                            Изобажение
                        </label>


                        @if ($advantages[$key]['image'])
                            Photo Preview:
                            <img src="{{ $advantages[$key]['image']->temporaryUrl() }}"
                                 style="max-width: 400px;object-fit: contain">
                        @elseif(array_key_exists('file',$advantages[$key]))
                            <img src="{{asset($advantages[$key]['file'])}}"
                                 style="max-width: 400px;object-fit: contain">
                        @endif

                        <input type="file" wire:model="advantages.{{$key}}.image">
                        @error("advantages.$key.image")
                        <div class="alert alert-danger">{{ $message }}</div> @enderror


                    </div>

                </div>
            @endforeach

            <div class="card-footer">
                <div class="form-group">
                    <div class="control">
                        <button
                            wire:loading.attr="disabled"
                            class="btn btn-success" type="submit">Сохранить
                        </button>
                        <div wire:loading>
                            Один момент...
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>

</div>

@push('scripts')
    <script>
        let config = {
            filebrowserImageBrowseUrl: '/elfinder/ckeditor',
            allowedContent: true,
            removeFormatAttributes: ''
        };

        for (let i = 0; i <={{$key}}; i++) {
            CKEDITOR.replace('content_ru' + i, config);
            CKEDITOR.replace('content_en' + i, config);

            CKEDITOR.instances['content_en' + i].on('change', function (e) {
            @this.set('advantages.' + i + '.content_en', e.editor.getData());
            });
            CKEDITOR.instances['content_ru' + i].on('change', function (e) {
            @this.set('advantages.' + i + '.content_ru', e.editor.getData());
            });
        }
    </script>
@endpush
