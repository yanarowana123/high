<div>
    <div class="card">
        <div class="card-header">
            <h1>Расписание</h1>
        </div>

        <form wire:submit.prevent="store">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div>
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
            </div>

            @foreach($schedules as $key=>$program)
                <div class="card-body  border border-bottom-1">
                    <div class="d-flex align-items-center">
                        <h3>Программа {{$key+1}}</h3>
                        <i class="fa fa-trash fa-fw ml-5 text-danger"
                           style="cursor:pointer"
                           wire:click="remove({{$key}})"></i>
                    </div>



                    <div class="form-group">
                        <label class="label " for="title_ru">
                            Наименование
                        </label>
                        <div class="control">
                            <input type="text"
                                   class="form-control @error("schedules.$key.title_ru") is-invalid @enderror"
                                   wire:model="schedules.{{$key}}.title_ru"
                                   required>
                        </div>

                        @error("schedules.$key.title_ru")
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="label " for="title_en">
                            Title
                        </label>
                        <div class="control">
                            <input type="text"
                                   class="form-control @error("schedules.$key.title_en") is-invalid @enderror"
                                   wire:model="schedules.{{$key}}.title_en"
                                   required>
                        </div>

                        @error("schedules.$key.title_en")
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>


                    <div class="form-group">
                        <label class="label " for="content_ru">
                            Контент
                        </label>
                        <div class="control">
                            <textarea wire:model="schedules.{{$key}}.content_ru"
                                      class="form-control @error("schedules.$key.content_ru") is-invalid @enderror"
                                      required
                            ></textarea>
                        </div>

                        @error("schedules.$key.content_ru")
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="label " for="content_en">
                            Content
                        </label>
                        <div class="control">
                            <textarea wire:model="schedules.{{$key}}.content_en"
                                      class="form-control @error("schedules.$key.content_en") is-invalid @enderror"
                                      required
                            ></textarea>
                        </div>

                        @error("schedules.$key.content_en")
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>


                </div>
            @endforeach

            <div class="card-footer">
                <div class="form-group">
                    <div class="control">
                        <button class="btn btn-success" type="submit">Сохранить</button>
                        <button class="btn btn-outline-success" wire:click.prevent="add">+</button>
                    </div>
                </div>
            </div>
        </form>

    </div>

</div>

@push('scripts')
    <script>
        Livewire.on('submitForm', () => {
            // alert('f');
            // $("html, body").animate({scrollTop: 0}, "slow");
        })

    </script>
@endpush
