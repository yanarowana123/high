<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <meta name="description" content="Школа высшей математики"/>
    <meta name="description" content="Обучение математики в Алматы"/>
    <meta name="description" content="Курсы высшей математики"/>
    <meta name="description" content="Курсы Высшая Математика для программистов, финансистов, экономистов"/>
    <meta name="description" content="Математика для старшеклассников"/>
    <meta name="description" content="Поступить в РФМШ, НИШ (NIS)"/>
    <meta name="description" content="Сдать экзамены SAT, GRE и GMAT"/>


    @switch(\Illuminate\Support\Facades\Route::currentRouteName())
        @case('about')
        <title>High Math - информация о школе высшей математики Казахстан.</title>
        <meta
            name="description"
            content="От Вас не требуется наизусть знать все математические законы и формулы, но мы научим ими пользоваться
Нами создана уникальная методика преподавания Высшей Математики, которая делает ее доступной и привлекательной для всех">
        @case('news')
        <title>Полезные статьи о высшей математики школы High Math
        </title>
        <meta name="description" content="Занятия разработаны на основе курсов Высшей Математики университетов">
        @case('schedule')
        <title>Расписание. Весенний семестр 2021 (4 мес)
        </title>
        <meta name="description"
              content="Набор Старшеклассников в семестр — только 3 группы, минимальный возраст — 15 лет. Занятия разработаны на основе курсов Высшей Математики университетов">
        @case('contacts')
        <title>Контакты Школы Высшей математики - High Math School

        </title>
        <meta name="description"
              content="
        Мы находимся в городе  Алматы, пр. Достык 202 (уг. Аль-Фараби), БЦ Форум-Достык">


        @default
        <title>Школа высшей математики в Алматы Нур Султане и других городах Казахстана.
        </title>
        <meta name="description"
              content="Мы предлагаем обучение Высшей математике в простой и доступной форме профессионалам в любых областях">
    @endswitch


    <link
        href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@400;500;600;700&display=swap"
        rel="stylesheet"
    />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}"/>
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '170264234664983');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1"
             src="https://www.facebook.com/tr?id=170264234664983&ev=PageView
&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
@stack('styles')

<section class="active__header">
    <div class="active__menu active__menu-left">
        <div class="active__header-inner">
            <div class="active__header-wrap">
                <div class="active__header-title">@lang('main.Courses')</div>
                <div class="active__header-display">
                    <a href="{{route('math')}}" class="active__header-link"
                    >@lang('main.Mathematical Engineering')</a
                    >
                    <a href="{{route('prog')}}" class="active__header-link">@lang('main.Programmers')</a>
                    <a href="{{route('fin')}}" class="active__header-link"
                    >@lang('main.Financiers and Economists')</a
                    >
                    <a href="{{route('stud')}}" class="active__header-link"
                    >@lang('main.High school students')</a
                    >
                    <a href="{{route('all')}}" class="active__header-link"
                    >@lang('main.Anyone who wants to know HM')</a
                    >
                </div>
            </div>
            <div class="active__header-wrap2">
                <a href="{{route('home')}}" class="active__header-title-link">@lang('main.Home')</a>
                <a href="{{route('about')}}" class="active__header-title-link">@lang('main.About school')</a>
                <a href="{{route('news')}}" class="active__header-title-link"
                >@lang('main.Articles and news')</a
                >
                <a href="{{route('schedule')}}" class="active__header-title-link"
                >@lang('main.Schedule')</a
                >
                <a href="{{route('contacts')}}" class="active__header-title-link">@lang('main.Contacts')</a>
                <a href="{{route('schedule')}}">
                    <button class="active__header-btn">@lang('main.Choose course')</button>
                </a>
                <div class="active__header-social">
                    @if($instagram)
                        <a href="https://www.instagram.com/highmathschool/">
                            <img src="{{asset('img/instagram.svg')}}" alt=""/>
                        </a>
                    @endif

                    @if($youtube)
                        <a href="{{$youtube->content}}">
                            <img src="{{asset('img/youtube.svg')}}" alt=""/>
                        </a>
                    @endif

                    @if($facebook)
                        <a href="https://www.facebook.com/High-Math-School-107090548005684">
                            <img src="{{asset('img/facebook.svg')}}" alt=""/>
                        </a>
                    @endif

                </div>
                {{--                <button class="active__header-btn">Напишите нам</button>--}}
            </div>
        </div>
    </div>
</section>
<header class="header">
    <div class="container-fluid">
        <div class="header__inner">
            <div class="header__language">
                <span>@lang('main.Site language'):</span>
                <a class="header__link"
                   href="
                   {{request()->route()->named('home')?
'/ru'
:'/ru/'.substr(Request::getRequestUri(), 4)}}"
                ><img src="{{asset('img/russian-language.png')}}" alt="rus-language"
                    /></a>

                <a class="header__link"
                   href="
                   {{request()->route()->named('home')?
'/en'
:'/en/'.substr(Request::getRequestUri(), 4)}}"
                ><img src="{{asset('img/american-language.jpg')}}" alt="usa-language"
                    /></a>
            </div>
            <a href="{{route('home')}}" class="header__logo">
                <img src="{{asset('img/logo.svg')}}" alt="logo"/>
            </a>
            <div class="header__menu">

                @if($pageTitle)
                    <div class="hamburger__link">{{$pageTitle}}</div>
                @endif
                <div class="hamburger">
                    <span></span>
                </div>
            </div>
        </div>
    </div>
</header>
@yield('content')

@unless(request()->route()->named('math'))
    <section class="methods">
        <div class="container-fluid">
            <div class="methods__inner">
                <div class="methods__title">
                    {!! $footer->content !!}
                </div>
                <div class="methods__university">
                    @foreach($logos as $logo)
                        <img src="{{asset($logo->file)}}" alt="university"/>

                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endunless


<footer class="footer">
    <div class="container-fluid">
        <div class="footer__inner">
            <a href="" class="footer__arrow"
            ><img src="{{asset('img/footer-arrow.png')}}" alt=""
                /></a>
            <div class="footer__descr">
                <div class="footer__title">@lang('main.High math school')</div>
                <div class="footer__safe">@lang('main.All rights reserved') {{date('Y')}}</div>
            </div>
            @foreach($phones as $phone)
                <div class="d-flex">
                    <a href="tel:{{$phone->content}}" class="footer__phone">{{$phone->content}}</a>
                </div>
            @endforeach
        </div>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="{{asset('js/libs.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
@stack('scripts')
</body>
</html>
