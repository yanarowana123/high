@extends('layouts.main')
@section('content')
    <section class="main">
        <video class="main__video" autoplay muted loop id="myVideo">
            <source
                src="{{asset('img/School-page/depositphotos_203145164-stock-video-robot-hand-and-butterfly.mp4')}}"
                type="video/mp4"
            />
        </video>
    </section>
    <section class="news-content">
        <div class="container-fluid">
            <div class="news-content__inner">
                <div class="news-content__btns">
                    <div
                        class="news-content__check1 news-content__btn news-content__btn-active"
                    >
                        Все
                    </div>
                    <div class="news-content__check2 news-content__btn">Статьи</div>
                    <div class="news-content__check3 news-content__btn">Новости</div>
                </div>
                <div class="news-content__items">
                    <div class="news-content__item news_content_reports">
                        <img
                            class="news-content__item-img"
                            src="{{asset('img/news-page/img1.png')}}"
                            alt="1"
                        />
                        <div class="news-content__item-inner">
                            <div class="news-content__item-title">
                                Натуральный логарифм нормально распределен выражение
                            </div>
                            <div class="news-content__item-wrap">
                                <a class="news-content__item-link">Читать дальше</a>
                                <div class="news-content__item-date">29.09.2020</div>
                            </div>
                            <div class="news-content__item-sort1 news-content__sort">
                                Новости
                            </div>
                        </div>
                    </div>
                    <div class="news-content__item news_content_news">
                        <img
                            class="news-content__item-img"
                            src="{{asset('img/news-page/img2.png')}}"
                            alt="1"
                        />
                        <div class="news-content__item-inner">
                            <div class="news-content__item-title">
                                Прямоугольная матрица позитивно соответствует скачок
                            </div>
                            <div class="news-content__item-wrap">
                                <a class="news-content__item-link">Читать дальше</a>
                                <div class="news-content__item-date">29.09.2020</div>
                            </div>
                            <div class="news-content__item-sort2 news-content__sort">
                                Статьи
                            </div>
                        </div>
                    </div>
                    <div class="news-content__item news_content_reports">
                        <img
                            class="news-content__item-img"
                            src="{{asset('img/news-page/img1.png')}}"
                            alt="1"
                        />
                        <div class="news-content__item-inner">
                            <div class="news-content__item-title">
                                Натуральный логарифм нормально распределен выражение
                            </div>
                            <div class="news-content__item-wrap">
                                <a class="news-content__item-link">Читать дальше</a>
                                <div class="news-content__item-date">29.09.2020</div>
                            </div>
                            <div class="news-content__item-sort1 news-content__sort">
                                Новости
                            </div>
                        </div>
                    </div>
                    <div class="news-content__item news_content_news">
                        <img
                            class="news-content__item-img"
                            src="{{asset('img/news-page/img3.png')}}"
                            alt="1"
                        />
                        <div class="news-content__item-inner">
                            <div class="news-content__item-title">
                                Эпсилон окрестность, исключая очевидный случай
                            </div>
                            <div class="news-content__item-wrap">
                                <a class="news-content__item-link">Читать дальше</a>
                                <div class="news-content__item-date">29.09.2020</div>
                            </div>
                            <div class="news-content__item-sort2 news-content__sort">
                                Статьи
                            </div>
                        </div>
                    </div>
                    <div class="news-content__item news_content_news">
                        <img
                            class="news-content__item-img"
                            src="{{asset('img/news-page/img2.png')}}"
                            alt="1"
                        />
                        <div class="news-content__item-inner">
                            <div class="news-content__item-title">
                                Прямоугольная матрица позитивно соответствует скачок
                            </div>
                            <div class="news-content__item-wrap">
                                <a class="news-content__item-link">Читать дальше</a>
                                <div class="news-content__item-date">29.09.2020</div>
                            </div>
                            <div class="news-content__item-sort2 news-content__sort">
                                Статьи
                            </div>
                        </div>
                    </div>
                    <div class="news-content__item news_content_reports">
                        <img
                            class="news-content__item-img"
                            src="{{asset('img/news-page/img1.png')}}"
                            alt="1"
                        />
                        <div class="news-content__item-inner">
                            <div class="news-content__item-title">
                                Натуральный логарифм нормально распределен выражение
                            </div>
                            <div class="news-content__item-wrap">
                                <a class="news-content__item-link">Читать дальше</a>
                                <div class="news-content__item-date">29.09.2020</div>
                            </div>
                            <div class="news-content__item-sort1 news-content__sort">
                                Новости
                            </div>
                        </div>
                    </div>
                    <div class="news-content__item news_content_news">
                        <img
                            class="news-content__item-img"
                            src="{{asset('img/news-page/img3.png')}}"
                            alt="1"
                        />
                        <div class="news-content__item-inner">
                            <div class="news-content__item-title">
                                Эпсилон окрестность, исключая очевидный случай
                            </div>
                            <div class="news-content__item-wrap">
                                <a class="news-content__item-link">Читать дальше</a>
                                <div class="news-content__item-date">29.09.2020</div>
                            </div>
                            <div class="news-content__item-sort2 news-content__sort">
                                Статьи
                            </div>
                        </div>
                    </div>
                </div>
                <div class="news-content__pagination">
                    <div class="news-content__pagination-wrap">
                        <div class="news-content__pagination-numbers">
                            <div
                                class="news-content__pagination-number news-content__pagination-number_active"
                            >
                                1
                            </div>
                            <div class="news-content__pagination-number">2</div>
                            <div class="news-content__pagination-number">3</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
