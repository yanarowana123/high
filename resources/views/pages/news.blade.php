@extends('layouts.main')
@section('content')
    <section class="main">
        <video class="main__video" autoplay muted loop id="myVideo"
               preload
               playsinline
               disablepictureinpicture="true"
               src="{{asset($video)}}"
               type="video/mp4"
        ></video>
    </section>
    <section class="news-content">
        <div class="container-fluid">
            <div class="news-content__inner">
                <div class="news-content__btns">
                    <div
                        class="news-content__check1 news-content__btn news-content__btn-active"
                    >
                        Все
                    </div>
                    <div class="news-content__check2 news-content__btn">@lang('main.Articles')</div>
                    <div class="news-content__check3 news-content__btn">@lang('main.News')</div>
                </div>
                <div class="main-about_us__wrap-title">
                    Mal
                </div>
                <div class="news-content__items">
                    @foreach($articles as $article)
                        <div class="news-content__item
                            {{$article->type == \App\Models\Article::NEWS?'news_content_reports':'news_content_news'}}
                            news_content_reports">
                            <img
                                class="news-content__item-img"
                                src="{{asset($article->image)}}"
                                alt="1"
                            />
                            <div class="news-content__item-inner">
                                <div class="news-content__item-title">
                                    {{$article->title}}
                                </div>
                                <div class="news-content__item-wrap">
                                    <a href="{{route('news.show',$article->slug)}}" class="news-content__item-link">@lang('main.More')</a>
                                    <div
                                        class="news-content__item-date">{{$article->created_at->format('d.m.Y')}}</div>
                                </div>
                                <div
                                    class="news-content__item-sort{{$article->type == \App\Models\Article::NEWS?'1':'2'}} news-content__sort">
                                    {{$article->type == \App\Models\Article::NEWS?'Новости':'Статьи'}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="news-content__pagination">
                    <div class="news-content__pagination-wrap">
                        <div class="news-content__pagination-numbers">
                            {{$articles->links()}}
                        </div>
                        {{--                        <div class="news-content__pagination-btn">--}}
                        {{--                            <button class="button">Показать еще</button>--}}
                        {{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection



@push('styles')
    <style>

        .news-content__pagination-wrap {
            align-items: center;
        }

        .news-content__pagination-numbers nav .pagination li:first-child {
            display: none;
        }

        .news-content__pagination-numbers nav .pagination li:last-child {
            display: none;
        }

        .news-content__pagination-numbers nav .pagination li {
            margin-left: 20px;
        }

        .news-content__pagination-numbers nav .pagination li a {
            border: 1px solid #466386;
            box-sizing: border-box;
            border-radius: 4px;
            padding: 15px 20px;
            color: #466386;
        }

        .news-content__pagination-numbers nav .pagination li.active span {
            background: #466386;
            color: #fff;
            border: 1px solid #466386;
            box-sizing: border-box;
            border-radius: 4px;
            padding: 15px 20px;
        }
    </style>
@endpush
