@extends('layouts.main')
@section('content')
    <div class="cover_class"></div>
    <section class="main">
        <video class="main__video" autoplay muted loop id="myVideo"
               preload
               playsinline
               disablepictureinpicture="true"
               src="{{asset($video)}}"
               type="video/mp4"
        ></video>
    </section>
    <section class="main-services">
        <div class="main-services__inner">
            <div class="main-services__items">
                <a class="main-services__item" href="{{route('math')}}">
                    <img src="{{asset('img/Main-page/services-1.png')}}" alt="1"/>
                    <div class="main-services__title">@lang('main.Mathematical Engineering')</div>
                </a>
                <a class="main-services__item" href="{{route('prog')}}">
                    <img src="{{asset('img/Main-page/services-5.png')}}" alt="2"/>
                    <div class="main-services__title">@lang('main.Programmers')</div>
                </a>
                <a class="main-services__item" href="{{route('fin')}}">
                    <img src="{{asset('img/Main-page/services-2.png')}}" alt="3"/>
                    <div class="main-services__title">@lang('main.Financiers and Economists')</div>
                </a>
                <a class="main-services__item" href="{{route('stud')}}">
                    <img src="{{asset('img/Main-page/services-3.png')}}" alt="4"/>
                    <div class="main-services__title">@lang('main.High school students')</div>
                </a>
                <a class="main-services__item" href="{{route('all')}}">
                    <img src="{{asset('img/Main-page/services-4.png')}}" alt="5"/>
                    <div class="main-services__title">@lang('main.Anyone who wants to know HM')</div>
                </a>
            </div>
            <div class="main-services__select">
                <a href="{{route('schedule')}}">
                    @lang('main.Choose course')
                    <img src="{{asset('img/Main-page/arrow-select.png')}}" alt="arrow"/>
                </a>
            </div>
        </div>
    </section>
    <section class="main-about_us">
        <div class="main-about_us__inner">
            <div class="main-about_us__wrap">
                <div class="main-about_us__wrap-suptitle">{{$welcome->text}}</div>
                <div class="main-about_us__wrap-title">{{$welcome->title}}</div>
                <div class="main-about_us__wrap-subtitle">
                    {{$welcome->content}}
                </div>
            </div>
            <div class="main-about_us__wrap2">
                <div class="main-about_us__descr">
                    <div class="main-about_us__title">
                        {{$help->title}}
                        {{--                        Мы хотим помочь людям <br/>--}}
                        {{--                        повысить свои знания--}}
                    </div>
                    <div class="main-about_us__subtitle">
                        {{$help->content}}
                    </div>
                </div>
                <div class="main-about_us__course">
                    <div class="main-about_us__items">
                        @foreach($features as $feature)
                            <div class="main-about_us__item">
                                <div class="main-about_us__item-number">{{$feature->title}}</div>
                                <div class="main-about_us__item-descr">
                                    {{$feature->content}}
                                </div>
                            </div>
                        @endforeach
                        <div class="main-about_us__button">
                            <a href="{{route('schedule')}}" class="button">@lang('main.Choose course')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="whatsapp">
            <a href="https://api.whatsapp.com/send?phone=+77051112334&amp;text=Здравствуйте! Хочу записаться на курсы."></a>
            <div class="circlephone" style="transform-origin: center;"></div>
            <div class="circle-fill" style="transform-origin: center;"></div>
        </div>
    </section>
@endsection
@push('styles')
    <style>
        .main-services__item {
            display: flex;
        }
        .whatsapp { background:url({{asset('images/whatsapp.png')}}) center center no-repeat; background-size: 100%; width: 50px; height: 50px; position: fixed; bottom: 10px; right: 10px;
            z-index: 999; }
        .whatsapp a { display: block; width: 100%; height: 100%;
            position: relative;
            z-index: 999; }
        .circlephone{box-sizing:content-box;-webkit-box-sizing:content-box;border: 2px solid #44bb6e;width:80px;height:80px;bottom:-15px;left:-15px;position:absolute;-webkit-border-radius:100%;-moz-border-radius: 100%;border-radius: 100%;opacity: .5;-webkit-animation: circle-anim 2.4s infinite ease-in-out !important;-moz-animation: circle-anim 2.4s infinite ease-in-out !important;-ms-animation: circle-anim 2.4s infinite ease-in-out !important;-o-animation: circle-anim 2.4s infinite ease-in-out !important;animation: circle-anim 2.4s infinite ease-in-out !important;-webkit-transition: all .5s;-moz-transition: all .5s;-o-transition: all .5s;transition: all 0.5s;}
        .circle-fill{box-sizing:content-box;-webkit-box-sizing:content-box;background-color:#44bb6e;width:50px;height:50px;bottom:-2px;left:-2px;position:absolute;-webkit-border-radius: 100%;-moz-border-radius: 100%;border-radius: 100%;border: 2px solid transparent;-webkit-animation: circle-fill-anim 2.3s infinite ease-in-out;-moz-animation: circle-fill-anim 2.3s infinite ease-in-out;-ms-animation: circle-fill-anim 2.3s infinite ease-in-out;-o-animation: circle-fill-anim 2.3s infinite ease-in-out;animation: circle-fill-anim 2.3s infinite ease-in-out;-webkit-transition: all .5s;-moz-transition: all .5s;-o-transition: all .5s;transition: all 0.5s;}
        @keyframes pulse {0% {transform: scale(0.9);opacity: 1;}
            50% {transform: scale(1); opacity: 1; }
            100% {transform: scale(0.9);opacity: 1;}}
        @-webkit-keyframes pulse {0% {-webkit-transform: scale(0.95);opacity: 1;}
            50% {-webkit-transform: scale(1);opacity: 1;}
            100% {-webkit-transform: scale(0.95);opacity: 1;}}
        @keyframes tossing {
            0% {transform: rotate(-8deg);}
            50% {transform: rotate(8deg);}
            100% {transform: rotate(-8deg);}}
        @-webkit-keyframes tossing {
            0% {-webkit-transform: rotate(-8deg);}
            50% {-webkit-transform: rotate(8deg);}
            100% {-webkit-transform: rotate(-8deg);}}
        @-moz-keyframes circle-anim {
            0% {-moz-transform: rotate(0deg) scale(0.5) skew(1deg);opacity: .1;-moz-opacity: .1;-webkit-opacity: .1;-o-opacity: .1;}
            30% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .5;-moz-opacity: .5;-webkit-opacity: .5;-o-opacity: .5;}
            100% {-moz-transform: rotate(0deg) scale(1) skew(1deg);opacity: .6;-moz-opacity: .6;-webkit-opacity: .6;-o-opacity: .1;}}
        @-webkit-keyframes circle-anim {
            0% {-webkit-transform: rotate(0deg) scale(0.5) skew(1deg);-webkit-opacity: .1;}
            30% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);-webkit-opacity: .5;}
            100% {-webkit-transform: rotate(0deg) scale(1) skew(1deg);-webkit-opacity: .1;}}
        @-o-keyframes circle-anim {
            0% {-o-transform: rotate(0deg) kscale(0.5) skew(1deg);-o-opacity: .1;}
            30% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);-o-opacity: .5;}
            100% {-o-transform: rotate(0deg) scale(1) skew(1deg);-o-opacity: .1;}}
        @keyframes circle-anim {
            0% {transform: rotate(0deg) scale(0.5) skew(1deg);opacity: .1;}
            30% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .5;}
            100% {transform: rotate(0deg) scale(1) skew(1deg);
                opacity: .1;}}
        @-moz-keyframes circle-fill-anim {
            0% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
            50% {-moz-transform: rotate(0deg) -moz-scale(1) skew(1deg);opacity: .2;}
            100% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
        @-webkit-keyframes circle-fill-anim {
            0% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;  }
            50% {-webkit-transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;  }
            100% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
        @-o-keyframes circle-fill-anim {
            0% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
            50% {-o-transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;}
            100% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
        @keyframes circle-fill-anim {
            0% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
            50% {transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;}
            100% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
    </style>
@endpush
