@extends('layouts.main')
@section('content')
    <section class="main">
        <video class="main__video" autoplay muted loop id="myVideo"
               preload
               playsinline
               disablepictureinpicture="true"
               src="{{asset($video)}}"
               type="video/mp4"
        ></video>
    </section>
    <section class="school-about">
        <div class="container-fluid">
            <div class="school-about__inner">
                <div class="school-about__title">{{$beauty->title}}</div>
                <div class="school-about__subtitle">
                    {!!  $beauty->content!!}
                </div>
            </div>
        </div>
    </section>
    <section class="offer school-offer">
        <div class="container-fluid">
            <div class="offer__inner">
                <div class="offer__title">
                    {{$easy->title}}
                </div>
                <div class="offer__descr">
                    {!! $easy->content !!}
                </div>
            </div>
        </div>
    </section>
    <section class="school-seven">
        <div class="container-fluid">
            <div class="school-seven__inner">
                <div class="school-seven__descr">
                    {!! $dev->content !!}
                </div>
            </div>
        </div>
    </section>
    <section class="school-offer2">
        <div class="container-fluid">
            <div class="school-offer2__inner">
                <div class="school-offer2__title">
                    {{$available->title}}
                </div>
                <div class="school-offer2__items">
                    @foreach($aboutFeatures as $key=>$feature)
                        <div class="school-offer2__item">
                            @if($key ==2)
                                <div class="school-offer2__descr">
                                    {{$feature->title}}
                                </div>
                            @else
                                <div class="school-offer2__descr border-bottom{{$key+1}}">
                                    {{$feature->title}}
                                </div>
                            @endif
                        </div>
                    @endforeach


                </div>
            </div>
        </div>
    </section>
    <section class="school-staff">
        <div class="school-staff__bg">
            <img
                src="{{asset('img/School-page/staff-bg1.png')}}"
                alt=""
                class="school-staff__bg1"
            />
            <img
                src="{{asset('img/School-page/staff-bg2.png')}}"
                alt=""
                class="school-staff__bg2"
            />
        </div>
        <div class="school-staff__inner">
            <div class="container-fluid">
                <div class="school-staff__title">
                    @lang('main.Our Kazakhstan founders')
                    <br/> @lang('main.and teachers')

                </div>

            </div>
        </div>
        <div class="school-staff__wrapper">
            <div class="container-fluid">
                <div class="school-staff__items">
                    @foreach($staff as $emp)
                        <div class="school-staff__item">
                            <div class="school-staff__item-photo">
                                {{--                                @foreach($staff as $emp)--}}
                                <img class="school-staff__item-img" src="{{asset($emp->file)}}"/>
                                {{--                                @endforeach--}}
                            </div>
                            <div class="school-staff__item-title">
                                {{$emp->title}}
                            </div>
                            <div class="school-staff__item-descr">
                                {!! $emp->content !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
