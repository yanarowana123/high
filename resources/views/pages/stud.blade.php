@extends('layouts.main')
@section('content')
    @push('styles')
        <style>
            .stud-promotion__item .stud-promotion__item-active p {
                color: #fff !important;;
            }

            .stud-program__subtitle p{
                font-size:17px;
            }
        </style>
    @endpush
    <section class="main">
        <video class="main__video" autoplay muted loop id="myVideo"
               preload
               playsinline
               disablepictureinpicture="true"
               src="{{asset($video)}}"
               type="video/mp4"
        ></video>
    </section>
    <section class="course">
        <div class="container-fluid">
            <div class="course__inner">
                @foreach($features as $feature)
                    <div class="course__item">
                        <div class="course__num">{{$feature->title}}</div>
                        <div class="course__subtitle">{{$feature->content}}</div>
                    </div>
                @endforeach
                <div class="course__btn">
                    <a href="{{route('schedule')}}" class="button">@lang('main.Choose course')</a>
                </div>
            </div>
        </div>
    </section>
    <section class="stud-promotion">
        <div class="stud-promotion__bg">
            <img src="{{asset('img/stud-page/promotions-bg.png')}}" alt=""/>
        </div>
        <div class="stud-promotion__inner">
            <div class="container-fluid">
                <div class="stud-promotion__title offset-md-6">
                    {{$truth->title}}
                </div>
                <div class="stud-promotion__subtitle offset-md-6">
                    {!!$truth->content!!}
                </div>
            </div>
        </div>
        <div class="stud-promotion__wrap">
            <div class="container-fluid">
                <div class="stud-promotion__title2">{{$offer->title}}</div>
                <div class="stud-promotion__subtitle2">
                    {!! $offer->content !!}
                </div>
                <div class="stud-promotion__items">
                    @foreach($advantages as $key=>$advantage)
                        <div class="stud-promotion__item pr-icon{{$key+1}}">
                            {!! $advantage->content !!}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <section class="quote">
        <div class="container-fluid">
            <div class="quote__inner">
                <div class="quote__title">
                    {!! $help->content !!}
                </div>
            </div>
        </div>
    </section>
    <section class="stud-program">
        <div class="container-fluid">
            <div class="stud-program__inner">
                <div class="stud-program__title">
                    {{$high->title}}
                </div>
                {!! $high->content !!}
                {{--                                <div class="stud-program__subtitle">--}}
                {{--                                    <i>--}}
                {{--                                        В связи с тем, что курсы Для Старшеклассников экспериментальные и--}}
                {{--                                        не являются основным направлением центра, количество групп для--}}
                {{--                                        школьников <span>ограничено</span>--}}
                {{--                                    </i>--}}
                {{--                                </div>--}}
                {{--                <div class="stud-program__descr">--}}
                {{--                    <p>--}}
                {{--                        В курсах, наряду со школьной программой, будут даны основы Высшей--}}
                {{--                        Математики. При этом, знакомство и освоение элементов Высшей--}}
                {{--                        Математики будет происходить естественным образом. Например, как--}}
                {{--                        решая алгебраическое уравнение можно использовать принципы Теории--}}
                {{--                        Чисел или как легко решается геометрическая задача при помощи--}}
                {{--                        векторов Аналитической Геометрии.--}}
                {{--                    </p>--}}
                {{--                    <p>--}}
                {{--                        Основной упор в курсах сделан на решение ключевых задач и--}}
                {{--                        примеров, что позволит легко освоить сложные математические методы--}}
                {{--                        и приемы без заучивания теорем и их доказательств.--}}
                {{--                    </p>--}}
                {{--                    <p>--}}
                {{--                        Курсы <span>HM Teen I, HM Teen II</span> отличаются друг от друга--}}
                {{--                        по углубленности изучаемых модулей Высшей Математики.--}}
                {{--                    </p>--}}
                {{--                </div>--}}
            </div>
        </div>
    </section>
    <section class="stud-about">
        <div class="stud-about__bg">
            <img src="img/stud-page/stud-about__bg.png" alt=""/>
        </div>
        <div class="stud-about__before">
            <div class="container-fluid">
                <div class="stud-about__before-title">@lang('main.Students')</div>
                <div class="stud-about__before-wrap">
                    <div class="stud-about__before-descr">
                        {{$students->title}}
                    </div>
                    <div class="stud-about__before-descr2">
                        {!! $students->content !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="stud-about__after">
            <div class="container-fluid">
                <div class="stud-about__after-title">@lang('main.After completing courses')</div>
                <div class="stud-promotion__items">
                    @foreach($graduates as $key=>$graduate)
                        @if($key !=3)
                            <div class="stud-promotion__item pr-icon{{$key+5}}">
                                {!! $graduate->content !!}
                            </div>
                        @else
                            <div class="stud-promotion__item">
                                <div class="stud-promotion__item-active" style="color:#fff">
                                    {!! $graduate->content !!}
                                </div>
                            </div>
                        @endif
                    @endforeach

                </div>
            </div>
        </div>
    </section>
    <section class="fin-about">
        <div class="fin-about__wrap">
            <div class="container-fluid">
                <div class="fin-about__title">@lang('main.Courses for Students')</div>
                <div class="fin-about__items">
                    @foreach($courses as $course)
                        <div class="fin-about__item">
                            <div class="fin-about__item-title">{{$course->title}}</div>
                            <div class="fin-about__item-subtitle">
                                {!! $course->content!!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

@endsection
