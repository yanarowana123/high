@extends('layouts.main')
@section('content')
    <section class="main">
        <video class="main__video" autoplay muted loop id="myVideo"
               preload
               playsinline
               disablepictureinpicture="true"
               src="{{asset($video)}}"
               type="video/mp4"
        ></video>
    </section>
    <section class="course">
        <div class="container-fluid">
            <div class="course__inner">
                @foreach($features as $feature)
                    <div class="course__item">
                        <div class="course__num">{{$feature->title}}</div>
                        <div class="course__subtitle">
                            {!! $feature->content !!}
                        </div>
                    </div>
                @endforeach

                <div class="course__btn">
                    <a href="{{route('schedule')}}" class="button">@lang('main.Choose course')</a>
                </div>
            </div>
        </div>
    </section>
    <section class="math-about">
        <div class="stud-promotion__wrap">
            <div class="container-fluid">
                <div class="stud-promotion__suptitle2">{{$block2->title}}</div>
                <div class="stud-promotion__title2">{!! $block1->content !!}</div>
                <div class="stud-promotion__subtitle2">
                    {!! $block2->content !!}
                </div>
            </div>
        </div>
    </section>
    <section class="offer all-offer">
        <div class="container-fluid">
            <div class="offer__inner">
                <div class="offer__title all-offer__title">
                    {{$block3->title}}
                </div>
                <div class="offer__descr">
                    {!! $block3->content !!}
                </div>
            </div>
        </div>
    </section>
    <section class="questions">
        <div class="container-fluid">
            <div class="questions__inner">
                <div class="questions__items">
                    @foreach($block4 as $key=>$item)
                        <div class="questions__item {{$key==0?'questions__item--active':''}}">
                            <h4 class="questions__item-title">
                                @lang('main.History') <span>{{$item->title}}:</span>
                            </h4>
                            <div class="questions__item-text">
                                {!! $item->content !!}
                            </div>
                        </div>

                    @endforeach

                </div>
            </div>
        </div>
    </section>
    <section class="fin-about">
        <div class="fin-about__wrap">
            <div class="container-fluid">
                <div class="fin-about__title">
                    @lang('main.Courses for everyone who wants to learn HM')
                </div>
                <div class="fin-about__items">
                    @foreach($courses as $course)
                        <div class="fin-about__item">
                            <div class="fin-about__item-title">{{$course->title}}</div>
                            <div class="fin-about__item-subtitle">
                                {!! $course->content!!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
