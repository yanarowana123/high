@extends('layouts.main')
@section('content')
    <section class="main">
        <video class="main__video" autoplay muted loop id="myVideo"
               preload
               playsinline
               disablepictureinpicture="true"
               src="{{asset($video)}}"
               type="video/mp4"
        ></video>
    </section>
    <section class="course">
        <div class="container-fluid">
            <div class="course__inner">
                @foreach($features as $feature)
                    <div class="course__item">
                        <div class="course__num">{{$feature->title}}</div>
                        <div class="course__subtitle">
                            {!! $feature->content !!}
                        </div>
                    </div>
                @endforeach
                <div class="course__btn">
                    <a href="{{route('schedule')}}" class="button">@lang('main.Choose course')</a>
                </div>
            </div>
        </div>
    </section>
    <section class="prog-about  math-bg__bg">
        <div class="container-fluid">
            <div class="prog-about__inner">
                <div class="prog-about__title math-bg__title">{{$block1->title}}</div>
                <div class="prog-about__subtitle math-bg__subtitle">
                    {!! $block1->content !!}
                </div>
            </div>
        </div>
    </section>
    <section class="math-about">
        <div class="stud-promotion__wrap">
            <div class="container-fluid">
                <div class="stud-promotion__title2">{{$block2->title}}</div>
                <div class="stud-promotion__subtitle2">
                    {!! $block2->content !!}
                </div>
            </div>
        </div>
    </section>
    <section class="math-txt2">
        <div class="container-fluid">
            <div class="math-txt2__inner">
                @foreach($advantages as $key=>$advantage)
                    <div class="math-txt2__item">
                        <img class="{{$key==0?'widthMath':''}}" src="/img/math-page/txt{{$key+1}}.png" alt=""/>
                        <span>{{$advantage->title}}</span>
                        {!! $advantage->content!!}
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="math-offer">
        <div class="container-fluid">
            <div class="math-offer__inner">
                <div class="math-offer__wrap">
                    <div class="math-offer__title">
                        {!! $block3->content !!}
                    </div>
                    <div class="math-offer__items">
                        @foreach($block4 as $item)
                            <div class="math-offer__item">
                                {!! $item->content !!}
                            </div>
                        @endforeach
                    </div>
                    <div class="math-offer__descr">
                        {!! $block5->content !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="fin-about">
        <div class="fin-about__wrap">
            <div class="container-fluid">
                <div class="fin-about__title">@lang('main.Mathematical Engineering courses')</div>
                <div class="fin-about__items">
                    @foreach($courses as $course)
                        <div class="fin-about__item">
                            <div class="fin-about__item-title">{{$course->title}}</div>
                            <div class="fin-about__item-subtitle">
                                {!! $course->content!!}
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>
@endsection
