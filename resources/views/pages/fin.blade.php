@extends('layouts.main')
@section('content')
    <section class="main">
        <video class="main__video" autoplay muted loop id="myVideo"
               preload
               playsinline
               disablepictureinpicture="true"
               src="{{asset($video)}}"
               type="video/mp4"
        ></video>
    </section>
    <section class="course">
        <div class="container-fluid">
            <div class="course__inner">
                @foreach($features as $feature)
                    <div class="course__item">
                        <div class="course__num">{{$feature->title}}</div>
                        <div class="course__subtitle">
                            {!!$feature->content !!}
                        </div>
                    </div>
                @endforeach
                <div class="course__btn">
                    <a href="{{route('schedule')}}" class="button">@lang('main.Choose course')</a>
                </div>
            </div>
        </div>
    </section>
    <section class="fin-texts">
        <div class="container-fluid">
            <div class="fin-texts__inner">
                {!! $info->content !!}
            </div>
        </div>
    </section>
    <section class="offer fin-offer">
        <div class="container-fluid">
            <div class="offer__inner">
                <div class="offer__title">
                    {!! $demand->title !!}
                </div>
                <div class="offer__descr">
                    {!! $demand->content !!}
                </div>
            </div>
        </div>
    </section>
    <section class="fin-about">
        <div class="fin-about__bg">
            <img src="{{asset('img/fin-page/fin-about-bg.png')}}" alt=""/>
        </div>
        <div class="fin-about__inner">
            <div class="container-fluid">
                <div class="fin-about__descr">
                    {!! $progs->content !!}
                </div>
            </div>
        </div>
        <div class="fin-about__wrap">
            <div class="container-fluid">
                <div class="fin-about__title">@lang('main.Financier / Economist Courses')</div>
                <div class="fin-about__items">
                    @foreach($courses as $course)
                        <div class="fin-about__item">
                            <div class="fin-about__item-title">{{$course->title}}</div>
                            <div class="fin-about__item-subtitle">
                                {!! $course->content!!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
