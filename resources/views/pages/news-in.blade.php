@extends('layouts.main')
@section('content')
{{--    <section class="main">--}}
{{--        <video class="main__video" autoplay muted loop id="myVideo">--}}
{{--            <source--}}
{{--                src="{{asset('img/School-page/depositphotos_203145164-stock-video-robot-hand-and-butterfly.mp4')}}"--}}
{{--                type="video/mp4"--}}
{{--            />--}}
{{--        </video>--}}
{{--    </section>--}}
    <section class="news-in-content">
        <div class="container-fluid">
            <div class="news-in-content__inner">
                <div class="news-in-content__wrap">
{{--                    <div class="news-in-content__btns">--}}
{{--                        @foreach($article->contents as $key=>$content)--}}
{{--                            <a href="#content-{{$key}}" class="news-in-content__btn" style="display:block">--}}
{{--                                --}}{{--                                <a href="#{{$key}}">--}}
{{--                                {{$content->title}}--}}
{{--                                --}}{{--                                </a>--}}
{{--                            </a>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}
                    <div class="news-in-content__item">
                        <div class="news-in-content__item-title">
                            {{$article->title}}
                        </div>
                        <div class="news-in-content__item-descr">
                            {!! $article->content !!}
                        </div>

                        @foreach($article->contents as $key=>$content)
                            <div class="news-in-content__item-wrap" id="content-{{$key}}">
                                <div class="news-in-content__item-suptitle">{{$content->title}}</div>
                                <div class="news-in-content__item-subtitle">
                                    {!! $content->content !!}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @if(isset($similarArticles) && $similarArticles->isNotEmpty())
                    <div class="news-in-content__similar">
                        <div class="news-in-content__similar-title">@lang('main.Similar news')</div>
                        <div class="news-content__items">
                            @if($article->type == \App\Models\Article::NEWS)

                                @foreach($similarArticles as $similar)
                                    <div class="news-content__item news_content_reports">
                                        <img
                                            class="news-content__item-img"
                                            src="{{asset($similar->image)}}"
                                            alt="1"
                                        />
                                        <div class="news-content__item-inner">
                                            <div class="news-content__item-title">
                                                {{$similar->title}}
                                            </div>
                                            <div class="news-content__item-wrap">
                                                <a href="{{route('news.show',$similar->slug)}}"
                                                   class="news-content__item-link">@lang('main.More')</a>
                                                <div
                                                    class="news-content__item-date">{{$similar->created_at->format('d.m.Y')}}</div>
                                            </div>
                                            <div class="news-content__item-sort1 news-content__sort">
                                                Новости
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                @foreach($similarArticles as $similar)
                                    <div class="news-content__item news_content_news">
                                        <img
                                            class="news-content__item-img"
                                            src="{{asset($similar->image)}}"
                                            alt="1"
                                        />
                                        <div class="news-content__item-inner">
                                            <div class="news-content__item-title">
                                                {{$similar->title}}
                                            </div>
                                            <div class="news-content__item-wrap">
                                                <a href="{{route('news.show',$similar->slug)}}"
                                                   class="news-content__item-link">Читать дальше</a>
                                                <div
                                                    class="news-content__item-date">{{$similar->created_at->format('d.m.Y')}}</div>
                                            </div>
                                            <div class="news-content__item-sort2 news-content__sort">
                                                @lang('main.Articles')
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif

                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection

@push('styles')
    <style>

    </style>
@endpush

@push('scripts')
    <script>

        $(document).on("click", "a[href*='#']", function () {
            $("body").removeClass("menu");
            $("html, body").animate({scrollTop: (($("#" + ($(this).prop("href").split('#')[1])).offset().top)) - ($('header').height())}, 1300);

            return false;
        });


    </script>
@endpush
