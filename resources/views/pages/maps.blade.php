@extends('layouts.main')
@section('content')
    <section class="main maps-main">

        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d572.6697866753909!2d76.96046688763367!3d43.23070068513662!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836f013da34019%3A0x45550ab9e34968e3!2z0JHQuNC30L3QtdGB0YEt0YbQtdC90YLRgCBGb3J1bSwg0L_RgNC-0YHQv9C10LrRgiDQlNC-0YHRgtGL0LosINCQ0LvQvNCw0YLRiyAwNTAwMDA!5e0!3m2!1sru!2skz!4v1604657117305!5m2!1sru!2skz"
            frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        <div class="maps-main__wrap">
            <div class="maps-main__subtitle">

                <div class="maps-main__item">
                    <div class="maps-main__title">@lang('main.Phone number'):</div>
                    @foreach($phones as $phone)
                        <a href="tel:{{$phone->content}}" class="maps-main__subtitle"
                        >{{$phone->content}}</a
                        >
                    @endforeach
                </div>
                <div class="maps-main__item">
                    <div class="maps-main__title">@lang('main.Address'):</div>
                    @foreach($addresses as $address)
                        <div class="maps-main__subtitle">
                            {{$address->content}}
                        </div>
                    @endforeach
                </div>
            </div>
            {{--            <div class="maps-main__item">--}}
            {{--                <div class="maps-main__title">Бесплатная стоянка:</div>--}}
            {{--                @foreach($texts as $text)--}}
            {{--                    <div class="maps-main__subtitle">--}}
            {{--                        {{$text->content}}--}}
            {{--                    </div>--}}
            {{--                @endforeach--}}

            {{--            </div>--}}
        </div>

    </section>
    <section class="course">
        <div class="container-fluid">
            <div class="course__inner">
                @foreach($features as $feature)
                    <div class="course__item">
                        <div class="course__num">{{$feature->title}}</div>
                        <div class="course__subtitle">
                            {!! $feature->content !!}
                        </div>
                    </div>
                @endforeach

                <div class="course__btn">
                    <a href="{{route('schedule')}}" class="button">@lang('main.Choose course')</a>
                </div>
            </div>
        </div>
        <div class="whatsapp">
            <a href="https://api.whatsapp.com/send?phone=+77051112334&amp;text=Здравствуйте! Хочу записаться на курсы."></a>
            <div class="circlephone" style="transform-origin: center;"></div>
            <div class="circle-fill" style="transform-origin: center;"></div>
        </div>
    </section>
@endsection
<style>
    .main-services__item {
        display: flex;
    }
    .whatsapp { background:url({{asset('images/whatsapp.png')}}) center center no-repeat; background-size: 100%; width: 50px; height: 50px; position: fixed; bottom: 10px; right: 10px;
        z-index: 999; }
    .whatsapp a { display: block; width: 100%; height: 100%;
        position: relative;
        z-index: 999; }
    .circlephone{box-sizing:content-box;-webkit-box-sizing:content-box;border: 2px solid #44bb6e;width:80px;height:80px;bottom:-15px;left:-15px;position:absolute;-webkit-border-radius:100%;-moz-border-radius: 100%;border-radius: 100%;opacity: .5;-webkit-animation: circle-anim 2.4s infinite ease-in-out !important;-moz-animation: circle-anim 2.4s infinite ease-in-out !important;-ms-animation: circle-anim 2.4s infinite ease-in-out !important;-o-animation: circle-anim 2.4s infinite ease-in-out !important;animation: circle-anim 2.4s infinite ease-in-out !important;-webkit-transition: all .5s;-moz-transition: all .5s;-o-transition: all .5s;transition: all 0.5s;}
    .circle-fill{box-sizing:content-box;-webkit-box-sizing:content-box;background-color:#44bb6e;width:50px;height:50px;bottom:-2px;left:-2px;position:absolute;-webkit-border-radius: 100%;-moz-border-radius: 100%;border-radius: 100%;border: 2px solid transparent;-webkit-animation: circle-fill-anim 2.3s infinite ease-in-out;-moz-animation: circle-fill-anim 2.3s infinite ease-in-out;-ms-animation: circle-fill-anim 2.3s infinite ease-in-out;-o-animation: circle-fill-anim 2.3s infinite ease-in-out;animation: circle-fill-anim 2.3s infinite ease-in-out;-webkit-transition: all .5s;-moz-transition: all .5s;-o-transition: all .5s;transition: all 0.5s;}
    @keyframes pulse {0% {transform: scale(0.9);opacity: 1;}
        50% {transform: scale(1); opacity: 1; }
        100% {transform: scale(0.9);opacity: 1;}}
    @-webkit-keyframes pulse {0% {-webkit-transform: scale(0.95);opacity: 1;}
        50% {-webkit-transform: scale(1);opacity: 1;}
        100% {-webkit-transform: scale(0.95);opacity: 1;}}
    @keyframes tossing {
        0% {transform: rotate(-8deg);}
        50% {transform: rotate(8deg);}
        100% {transform: rotate(-8deg);}}
    @-webkit-keyframes tossing {
        0% {-webkit-transform: rotate(-8deg);}
        50% {-webkit-transform: rotate(8deg);}
        100% {-webkit-transform: rotate(-8deg);}}
    @-moz-keyframes circle-anim {
        0% {-moz-transform: rotate(0deg) scale(0.5) skew(1deg);opacity: .1;-moz-opacity: .1;-webkit-opacity: .1;-o-opacity: .1;}
        30% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .5;-moz-opacity: .5;-webkit-opacity: .5;-o-opacity: .5;}
        100% {-moz-transform: rotate(0deg) scale(1) skew(1deg);opacity: .6;-moz-opacity: .6;-webkit-opacity: .6;-o-opacity: .1;}}
    @-webkit-keyframes circle-anim {
        0% {-webkit-transform: rotate(0deg) scale(0.5) skew(1deg);-webkit-opacity: .1;}
        30% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);-webkit-opacity: .5;}
        100% {-webkit-transform: rotate(0deg) scale(1) skew(1deg);-webkit-opacity: .1;}}
    @-o-keyframes circle-anim {
        0% {-o-transform: rotate(0deg) kscale(0.5) skew(1deg);-o-opacity: .1;}
        30% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);-o-opacity: .5;}
        100% {-o-transform: rotate(0deg) scale(1) skew(1deg);-o-opacity: .1;}}
    @keyframes circle-anim {
        0% {transform: rotate(0deg) scale(0.5) skew(1deg);opacity: .1;}
        30% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .5;}
        100% {transform: rotate(0deg) scale(1) skew(1deg);
            opacity: .1;}}
    @-moz-keyframes circle-fill-anim {
        0% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
        50% {-moz-transform: rotate(0deg) -moz-scale(1) skew(1deg);opacity: .2;}
        100% {-moz-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
    @-webkit-keyframes circle-fill-anim {
        0% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;  }
        50% {-webkit-transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;  }
        100% {-webkit-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
    @-o-keyframes circle-fill-anim {
        0% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
        50% {-o-transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;}
        100% {-o-transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
    @keyframes circle-fill-anim {
        0% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}
        50% {transform: rotate(0deg) scale(1) skew(1deg);opacity: .2;}
        100% {transform: rotate(0deg) scale(0.7) skew(1deg);opacity: .2;}}
</style>

