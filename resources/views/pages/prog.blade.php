@extends('layouts.main')
@section('content')
    <section class="main">
        <video class="main__video" autoplay muted loop id="myVideo"
               preload
               playsinline
               disablepictureinpicture="true"
               src="{{asset($video)}}"
               type="video/mp4"
        ></video>
    </section>
    <section class="course">
        <div class="container-fluid">
            <div class="course__inner">
                @foreach($features as $feature)
                    <div class="course__item">
                        <div class="course__num">{{$feature->title}}</div>
                        <div class="course__subtitle">
                            {!! $feature->content !!}
                        </div>
                    </div>
                @endforeach
                <div class="course__btn">
                    <a href="{{route('schedule')}}" class="button">@lang('main.Choose course')</a>
                </div>
            </div>
        </div>
    </section>
    <section class="prog-about">
        <div class="container-fluid">
            <div class="prog-about__inner">
                <div class="prog-about__title">
                    {{$block1->title}}
                </div>
                <div class="prog-about__subtitle">
                    {!! $block1->content !!}
                </div>
            </div>
        </div>
    </section>
    <section class="offer prog-offer">
        <div class="container-fluid">
            <div class="offer__inner">
                <div class="offer__title">
                    {{$block2->title}}
                </div>
                <div class="offer__descr">
                    {!! $block2->content !!}
                </div>
            </div>
        </div>
    </section>
    <section class="prog-products">
        <div class="container-fluid">
            <div class="prog-products__inner">
                <div class="prog-products__items">
                    @foreach($advantages1 as $advantage)
                        <div class="prog-products__item">
                            <img src="{{asset($advantage->file)}}" alt="1"/>
                            <div class="prog-products__title">{{$advantage->title}}</div>
                            <div class="prog-products__subtitle">
                                {!! $advantage->content !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <section class="quote">
        <div class="container-fluid">
            <div class="quote__inner">
                <div class="quote__title">
                    {!! $block3->content !!}
                </div>
            </div>
        </div>
    </section>
    <section class="prog-products">
        <div class="container-fluid">
            <div class="prog-products__inner">
                <div class="prog-products__items">
                    @foreach($advantages2 as $advantage)
                        <div class="prog-products__item">
                            <img src="{{asset($advantage->file)}}" alt="1"/>
                            <div class="prog-products__title">{{$advantage->title}}</div>
                            <div class="prog-products__subtitle">
                                {!! $advantage->content !!}
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>

    <section class="prog-meaching">
        <div class="container-fluid">
            <div class="prog-meaching__inner offset-md-5">
                <div class="prog-meaching__title">{{$block4->title}}</div>
                {!! $block4->content !!}
            </div>
        </div>
    </section>
    <section class="prog-analysis">
        <div class="container-fluid">
            <div class="prog-analysis__inner">
                <div class="prog-analysis__wrap">
                    <div class="prog-analysis__descr">
                        {!! $block5->content !!}
                    </div>
                </div>
                <div class="prog-analysis__wrap">
                    <div class="prog-analysis__title">
                        {{$block5->title}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="fin-about">
        <div class="fin-about__wrap">
            <div class="container-fluid">
                <div class="fin-about__title">@lang('main.Courses for Programmers')</div>
                <div class="fin-about__items">
                    @foreach($courses as $course)
                        <div class="fin-about__item">
                            <div class="fin-about__item-title">{{$course->title}}</div>
                            <div class="fin-about__item-subtitle">
                                {!! $course->content!!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
