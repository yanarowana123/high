@extends('admin.layouts.main')
@section('content')
    <div>
        <div class="card ">

            <form method="POST" action="{{route('admin.contact.index')}}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">

                    <div class="form-group">
                        <label for="content">Контент</label>
                        <input name="content_ru" type="text" class="form-control" id="content_ru"
                               value="{{old('content_ru')}}"
                        >
                        @error('content_ru')
                        <div class="text-red">{{$message}}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="content">Content</label>
                        <input name="content_en" type="text" class="form-control" id="content_en"
                               value="{{old('content_en')}}"
                        >
                        @error('content_en')
                        <div class="text-red">{{$message}}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label class="label" for="type">
                            Тип
                        </label>

                        <div class="control">
                            <select class="form-control" id="type" name="type" required>
                                <option value="" selected disabled>Тип</option>
                                @foreach(\App\Models\Contact::getTypeList() as $index=>$type)
                                    <option
                                        value="{{$index}}">{{$type}}</option>
                                @endforeach
                            </select>

                        </div>

                        @error('type')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>


{{--                    <div id="map" style="width: 100%; height: 250px"></div>--}}


                </div>
                <div class="card-footer">
                    <div class="form-group">
                        <button type="submit"
                                class="btn btn-success d-inline-flex align-items-center justify-content-center">
                            Сохранить
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection
{{--<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>--}}

{{--@push('scripts')--}}
{{--    <script>--}}
{{--        ymaps.ready(init);--}}
{{--        var center_map = [0, 0];--}}
{{--        var map = "";--}}

{{--        function init() {--}}
{{--            map = new ymaps.Map('map', {--}}
{{--                center: [43.235908367466486, 76.95276060852048],--}}
{{--                zoom: 12,--}}
{{--            });--}}


{{--            map.setCenter([43.235908367466486,76.95276060852048]);--}}

{{--            map.geoObjects.add(new ymaps.Placemark([43.235908367466486, 76.95276060852048], {--}}
{{--                balloonContent: '',--}}

{{--            }, {--}}
{{--                preset: 'islands#icon',--}}
{{--                apikey:'695b3c94-4e87-4f93-b7c7-a49c3cfb89d8\n',--}}
{{--                iconColor: '#0095b6'--}}
{{--            }));--}}


{{--            map.events.add('click', function (e) {--}}
{{--                console.log(e.get('coords'));--}}
{{--                map.geoObjects.removeAll();--}}
{{--                var coords = e.get('coords');--}}
{{--                map.geoObjects.add(new ymaps.Placemark(coords, {--}}
{{--                    balloonContent: ''--}}
{{--                }, {--}}
{{--                    preset: 'islands#icon',--}}
{{--                    iconColor: '#0095b6'--}}
{{--                }));--}}

{{--                $("#address-latitude").val(coords[0].toPrecision(9));--}}
{{--                $("#address-longitude").val(coords[1].toPrecision(9));--}}


{{--            });--}}
{{--        }--}}
{{--    </script>--}}

{{--@endpush--}}
