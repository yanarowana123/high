<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>AdminLTE 3 | Starter</title>

    <!-- Font Awesome Icons -->

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/admin.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    @livewireStyles

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>


    </nav>
    <!-- /.navbar -->


    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{asset('home')}}" class="brand-link">
            На главную
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">

            </div>

        @php

            $isProgActive = request()->route()->parameter('name') =='prog'
|| request()->route()->named('admin.prog.*')
||request()->route()->parameter('model') =='prog';

  $isHomeActive = request()->route()->parameter('name') =='home'
      || request()->route()->named('admin.home.*')
      ||request()->route()->parameter('model') =='home';

$isAboutActive = request()->route()->parameter('name') =='about'
|| request()->route()->named('admin.about.*')
||request()->route()->parameter('model') =='about';


$isFinActive = request()->route()->parameter('name') =='fin'
|| request()->route()->named('admin.fin.*')
||request()->route()->parameter('model') =='fin';

$isStudActive = request()->route()->parameter('name') =='stud'
|| request()->route()->named('admin.stud.*')
||request()->route()->parameter('model') =='stud';

    $isMathActive = request()->route()->parameter('name') =='engineer'
|| request()->route()->named('admin.engineer.*')
||request()->route()->parameter('model') =='engineer'
||request()->route()->parameter('model') =='eng';

    $isEveryoneActive = request()->route()->parameter('name') =='everyone'
|| request()->route()->named('admin.everyone.*')
||request()->route()->parameter('model') =='everyone';


        @endphp

        <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <li class="nav-item has-treeview
                    {{$isHomeActive?' menu-open':''}}
                        ">
                        <a href="#" class="nav-link
                    {{$isHomeActive?' active':''}}
                            ">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Главная
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('admin.video.index','home')}}"
                                   class="nav-link {{request()->route()->named('admin.video.index')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Видео</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.home.welcome')}}"
                                   class="nav-link {{request()->route()->named('admin.home.welcome')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>High Math School</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.home.help')}}"
                                   class="nav-link {{request()->route()->named('admin.home.help')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Миссия</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.feature','home')}}"
                                   class="nav-link {{request()->route()->named('admin.feature')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Преимущества</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.home.footer')}}"
                                   class="nav-link {{request()->route()->named('admin.home.footer')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Подвал</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.home.logo')}}"
                                   class="nav-link {{request()->route()->named('admin.home.logo')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Логотипы</p>
                                </a>
                            </li>
                        </ul>
                    </li>


                    <li class="nav-item has-treeview
                    {{$isAboutActive?' menu-open':''}}
                        ">
                        <a href="#" class="nav-link
                    {{$isAboutActive?' active':''}}
                            ">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                О нас
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('admin.video.index','about')}}"
                                   class="nav-link {{request()->route()->named('admin.video.index')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Видео</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.default',['about',1])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&request()->route()->parameters()['block'] == 1)?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>1 Блок</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.default',['about',2])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&request()->route()->parameters()['block'] == 2)?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>2 Блок</p>
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="{{route('admin.default',['about',3])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&request()->route()->parameters()['block'] == 3)?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>3 Блок</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.default',['about',4])}}"
                                   class="nav-link
                                   {{(request()->route()->named('admin.default')&&request()->route()->parameters()['block'] == 4)?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>4 Блок</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.about.feature')}}"
                                   class="nav-link {{request()->route()->named('admin.about.feature')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Преимущества</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.about.staff')}}"
                                   class="nav-link {{request()->route()->named('admin.about.staff')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Сотрудники</p>
                                </a>
                            </li>
                        </ul>
                    </li>


                    <li class="nav-item has-treeview
                    {{$isFinActive?' menu-open':''}}
                        ">
                        <a href="#" class="nav-link
                    {{$isFinActive?' active':''}}
                            ">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                FIN
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('admin.video.index','fin')}}"
                                   class="nav-link {{request()->route()->named('admin.video.index')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Видео</p>
                                </a>
                            </li>
                            {{--                            <li class="nav-item">--}}
                            {{--                                <a href="{{route('admin.feature','fin')}}"--}}
                            {{--                                   class="nav-link {{request()->route()->named('admin.feature')?'active':''}}">--}}
                            {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                            {{--                                    <p>Преимущества</p>--}}
                            {{--                                </a>--}}
                            {{--                            </li>--}}
                            <li class="nav-item">
                                <a href="{{route('admin.default',['fin',2])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')
&&request()->route()->parameters()['model'] == 'fin'
&&request()->route()->parameters()['block'] == 2)?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>2 Блок</p>
                                </a>
                            </li>
                            {{--                            @dd(request()->route()->parameters())--}}
                            <li class="nav-item">
                                <a href="{{route('admin.default',['fin',3])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&request()->route()->parameter('block') == 3&&request()->route()->parameter('model') == 'fin')?'active':''}}">
                                    <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>3 Блок</p>
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="{{route('admin.default',['fin',4])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&
request()->route()->parameters()['model']=='fin'&&
request()->route()->parameters()['block'] == 4)?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>4 Блок</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.feature','fin')}}"
                                   class="nav-link {{request()->route()->named('admin.feature')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Курсы</p>
                                </a>
                            </li>


                        </ul>
                    </li>


                    <li class="nav-item has-treeview
                    {{$isStudActive?' menu-open':''}}
                        ">
                        <a href="#" class="nav-link
                    {{$isStudActive?' active':''}}
                            ">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Студентам
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('admin.video.index','stud')}}"
                                   class="nav-link {{request()->route()->named('admin.video.index')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Видео</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.content','student')}}"
                                   class="nav-link {{request()->route()->named('admin.content')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Преимущества</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.default',['stud',1])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')
&&request()->route()->parameters()['model'] == 'stud'
&&request()->route()->parameters()['block'] == 1)?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>1 Блок</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.default',['stud',2])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&request()->route()->parameters()['block'] == 2 &&request()->route()->parameter('model') =='stud'  )?'active':''}}">
                                    <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>2 Блок</p>
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="{{route('admin.default',['stud',4])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&
request()->route()->parameters()['model']=='stud'&&
request()->route()->parameters()['block'] == 4)?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>3 Блок</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.default',['stud',5])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&
request()->route()->parameters()['model']=='stud'&&
request()->route()->parameters()['block'] == 5)?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>4 Блок</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.default',['stud',6])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&
request()->route()->parameters()['model']=='stud'&&
request()->route()->parameters()['block'] == 6)?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>5 Блок</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.content',['stud'])}}"
                                   class="nav-link
{{(request()->route()->named('admin.content')&&
request()->route()->parameters()['model']=='stud')?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>6 Блок</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.feature','stud')}}"
                                   class="nav-link {{request()->route()->named('admin.feature')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Курсы</p>
                                </a>
                            </li>
                        </ul>
                    </li>


                    <li class="nav-item has-treeview
                    {{$isMathActive?' menu-open':''}}
                        ">
                        <a href="#" class="nav-link
                    {{$isMathActive?' active':''}}
                            ">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Мат инженеринг
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('admin.video.index','engineer')}}"
                                   class="nav-link {{request()->route()->named('admin.video.index')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Видео</p>
                                </a>
                            </li>
                            {{--                            <li class="nav-item">--}}
                            {{--                                <a href="{{route('admin.feature','engineer')}}"--}}
                            {{--                                   class="nav-link {{request()->route()->named('admin.feature')?'active':''}}">--}}
                            {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                            {{--                                    <p>Преимущества</p>--}}
                            {{--                                </a>--}}
                            {{--                            </li>--}}
                            <li class="nav-item">
                                <a href="{{route('admin.feature',['engineer'])}}"
                                   class="nav-link
{{(request()->route()->named('admin.feature')&&
request()->route()->parameters()['model']=='engineer')?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>Плюсы</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.default',['engineer',1])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')
&&request()->route()->parameters()['model'] == 'engineer'
&&request()->route()->parameters()['block'] == 1)?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>1 Блок</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.default',['engineer',2])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&request()->route()->parameters()['block'] == 2 &&request()->route()->parameter('model') =='engineer'  )?'active':''}}">
                                    <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>2 Блок</p>
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="{{route('admin.default',['engineer',3])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&
request()->route()->parameters()['model']=='engineer'&&
request()->route()->parameters()['block'] == 3)?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>3 Блок</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.content',['engineer'])}}"
                                   class="nav-link
{{(request()->route()->named('admin.content')&&
request()->route()->parameters()['model']=='engineer')?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>4 Блок</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.default',['engineer',5])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&
request()->route()->parameters()['model']=='engineer'&&
request()->route()->parameters()['block'] == 4)?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>5 Блок</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.feature','eng')}}"
                                   class="nav-link {{request()->route()->named('admin.feature')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Курсы</p>
                                </a>
                            </li>


                        </ul>
                    </li>


                    <li class="nav-item has-treeview
                    {{$isProgActive?' menu-open':''}}
                        ">
                        <a href="#" class="nav-link
                    {{$isProgActive?' active':''}}
                            ">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Программистам
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('admin.video.index','prog')}}"
                                   class="nav-link {{request()->route()->named('admin.video.index')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Видео</p>
                                </a>
                            </li>
                            {{--                            <li class="nav-item">--}}
                            {{--                                <a href="{{route('admin.feature','prog')}}"--}}
                            {{--                                   class="nav-link {{request()->route()->named('admin.feature')?'active':''}}">--}}
                            {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                            {{--                                    <p>Преимущества</p>--}}
                            {{--                                </a>--}}
                            {{--                            </li>--}}
                            <li class="nav-item">
                                <a href="{{route('admin.default',['prog',1])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')
&&request()->route()->parameters()['model'] == 'prog'
&&request()->route()->parameters()['block'] == 1)?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>1 Блок</p>
                                </a>
                            </li>
                            {{--                            @dd(request()->route()->parameters())--}}
                            <li class="nav-item">
                                <a href="{{route('admin.default',['prog',2])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&request()->route()->parameter('block') == 2
&&request()->route()->parameter('model') == 'prog')?'active':''}}">
                                    <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>2 Блок</p>
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="{{route('admin.default',['prog',4])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&
request()->route()->parameters()['model']=='prog'&&
request()->route()->parameters()['block'] == 4)?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>3 Блок</p>
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="{{route('admin.default',['prog',5])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&
request()->route()->parameters()['model']=='prog'&&
request()->route()->parameters()['block'] == 5)?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>4 Блок</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.default',['prog',6])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&
request()->route()->parameters()['model']=='prog'&&
request()->route()->parameters()['block'] == 6)?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>5 Блок</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.prog.advantage')}}"
                                   class="nav-link
{{--{{(request()->route()->name=='admin.prog.advantage'?'active':''}}--}}
                                       "> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>Плюсы</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.feature','prog')}}"
                                   class="nav-link {{request()->route()->named('admin.feature')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Курсы</p>
                                </a>
                            </li>


                        </ul>
                    </li>

                    <li class="nav-item has-treeview
                    {{$isEveryoneActive?' menu-open':''}}
                        ">
                        <a href="#" class="nav-link
                    {{$isEveryoneActive?' active':''}}
                            ">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Для всех
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('admin.video.index','everyone')}}"
                                   class="nav-link {{request()->route()->named('admin.video.index')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Видео</p>
                                </a>
                            </li>
                            {{--                            <li class="nav-item">--}}
                            {{--                                <a href="{{route('admin.feature','everyone')}}"--}}
                            {{--                                   class="nav-link {{request()->route()->named('admin.feature')?'active':''}}">--}}
                            {{--                                    <i class="far fa-circle nav-icon"></i>--}}
                            {{--                                    <p>Преимущества</p>--}}
                            {{--                                </a>--}}
                            {{--                            </li>--}}

                            <li class="nav-item">
                                <a href="{{route('admin.default',['everyone',1])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')
&&request()->route()->parameters()['model'] == 'everyone'
&&request()->route()->parameters()['block'] == 1)?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>1 Блок</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.default',['everyone',2])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&request()->route()->parameters()['block'] == 2 &&request()->route()->parameter('model') =='everyone'  )?'active':''}}">
                                    <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>2 Блок</p>
                                </a>
                            </li>


                            <li class="nav-item">
                                <a href="{{route('admin.default',['everyone',3])}}"
                                   class="nav-link
{{(request()->route()->named('admin.default')&&
request()->route()->parameters()['model']=='everyone'&&
request()->route()->parameters()['block'] == 3)?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>3 Блок</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.content',['everyone'])}}"
                                   class="nav-link
{{(request()->route()->named('admin.content')&&
request()->route()->parameters()['model']=='everyone')?'active':''}}"> <i
                                        class="far fa-circle nav-icon"></i>
                                    <p>Истории</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{route('admin.feature','everyone')}}"
                                   class="nav-link {{request()->route()->named('admin.feature')?'active':''}}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Курсы</p>
                                </a>
                            </li>

                        </ul>
                    </li>


                    <li class="nav-item has-treeview
{{request()->route()->named('admin.category.*')||request()->route()->named('admin.course.*')?'menu-open':''}}
                        ">
                        <a href="#" class="nav-link
{{request()->route()->named('admin.category.*')||request()->route()->named('admin.course.*')?'active':''}}

                            ">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                                Курсы/Расписание
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('admin.category.index')}}" class="nav-link
                                 {{request()->route()->named('admin.category.*')?'active':''}}
                                    ">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Категории</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('admin.course.index')}}" class="nav-link
                                    {{request()->route()->named('admin.course.*')?'active':''}}
                                    ">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Все курсы</p>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('admin.video.index','schedule')}}"
                           class="nav-link {{request()->route()->named('admin.video.index')?'active':''}}">
                            <i class="nav-icon fas fa-th"></i>
                            <p>Статьи видео</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('admin.article.index','articles')}}" class="nav-link
                            {{request()->route()->named('admin.article.*')&&
request()->route()->parameter('type') =='articles'?'active':''}}
                            ">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Статьи
                            </p>
                        </a>
                    </li>



                    <li class="nav-item">
                        <a href="{{route('admin.article.index','news')}}" class="nav-link
                            {{request()->route()->named('admin.news.*')&&
request()->route()->parameter('type') =='news'?'active':''}}
                            ">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Новости
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('admin.video.index','news')}}"
                           class="nav-link {{request()->route()->named('admin.video.index')?'active':''}}">
                            <i class="nav-icon fas fa-th"></i>
                            <p>Новости Видео</p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('admin.default',['schedule',0])}}"
                           class="nav-link
{{(request()->route()->named('admin.default')&&
request()->route()->parameters()['model']=='schedule'&&
request()->route()->parameters()['block'] == 0)?'active':''}}">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Описание таблицы
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('admin.contact.index')}}" class="nav-link
                            {{request()->route()->named('admin.contact.*')?'active':''}}
                            ">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Контакты
                            </p>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="content-header">

        </div>
        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>

    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
        <div class="p-3">
            <h5>Title</h5>
            <p>Sidebar content</p>
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
            Anything you want
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
    </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/css/fileinput.min.css"
      integrity="sha512-KrsXJaSKHqHogNzPCOHPvkyvH4ZQGzUcR/Q6R3qywbdtrvOHPuPz9iRIoJoiKguuIgkDGsj+PwPh3QKnlwwQPA=="
      crossorigin="anonymous"/>
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/themes/explorer-fas/theme.min.css"
      integrity="sha512-Q7SKgUGiPCFaYNglTRq5JQ7Eaeg9wFhfEjO3VnZqN7UQ7RTPGJkTCT80aSHCccqDtRqKqtfKJrFt3d3qRgB4ig=="
      crossorigin="anonymous"/>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/themes/fas/theme.min.js"
        integrity="sha512-/NeWygjEalLr1Yk/Qe0aYaZ73Y5aIbPFF6J825FyprdqAoZNqUzCGf/4wIwVDsVwuJmS9SrF/tYWmRRn4r7qPA=="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.1.2/js/fileinput.min.js"
        integrity="sha512-wvbv0QlgtUZ1jkgRfB7HNUICt+27sqAUh2IwVJXfN9q7rtrmgbdI6LQjhzurdLo1+vxO645+GY+Kq8Vop0WA4w=="
        crossorigin="anonymous"></script>


<script src="https://cdn.ckeditor.com/4.11.2/standard/ckeditor.js"></script>

@livewireScripts

<script src="{{asset('js/admin.js')}}"></script>
@stack('scripts')

</body>
</html>
