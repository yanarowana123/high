@extends('admin.layouts.main')

@section('content')
    <div>
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between mt-3">
                    <h2>Курсы</h2>
                    <a href="{{route('admin.course.create')}}" class="btn btn-success mb-2">Добавить</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table table-responsive">
                    <table class="table table-hover">
                        <thead>

                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Заголовок</th>
                            <th scope="col">Title</th>
                            <th scope="col">Расписание</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody class="">
                        @foreach($courses as $index=>$course)
                            <tr>
                                <th scope="row"><a
                                        href="{{route('admin.course.edit',$course->id)}}">{{$index+1}} </a>
                                </th>
                                <th scope="row"><a
                                        href="{{route('admin.course.edit',$course->id)}}">{{$course->title_ru}} </a>
                                </th>
                                <th scope="row"><a
                                        href="{{route('admin.course.edit',$course->id)}}">{{$course->title_en}} </a>
                                </th>
                                <th scope="row"><a
                                        href="{{route('admin.schedule.edit',$course->id)}}">
                                        <i class="fas fa-calendar-alt"></i>
                                    </a>
                                </th>
                                <td><a onclick="return confirm('Are you sure?')"
                                       href="{{route('admin.course.delete',$course)}}"><i
                                            class="fa fa-trash fa-fw"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>

                </div>
            </div>
            <div class="card-footer">
                {{ $courses->links() }}
            </div>
        </div>
    </div>

@endsection
