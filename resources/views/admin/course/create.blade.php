@extends('admin.layouts.main')

@section('content')
    <div class="card">
        <div class="card-header">
            <h1>Добавить курс</h1></div>
        <form method="POST" action="{{route('admin.course.index')}}" enctype="multipart/form-data" novalidate>
            @csrf
            <div class="card-body">

                <div class="form-group">
                    <label class="label" for="title_ru">
                        Заголовок
                    </label>

                    <div class="control">
                        <input type="text"
                               class="form-control @error('title_ru') is-invalid @enderror"
                               name="title_ru"
                               id="title_ru"
                               value="{{old('title_ru')}}"
                               required>
                    </div>

                    @error('title_ru')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="label" for="title_en">
                        Title
                    </label>
                    <div class="control">
                        <input type="text"
                               class="form-control @error('title_en') is-invalid @enderror"
                               name="title_en"
                               id="title_en"
                               value="{{old('title_en')}}"
                               required>
                    </div>

                    @error('title_en')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="label" for="hours">
                        Продолжительность
                    </label>
                    <div class="control">
                        <input type="text"
                               class="form-control @error('hours') is-invalid @enderror"
                               name="hours"
                               id="hours"
                               value="{{old('hours')}}"
                               required>
                    </div>

                    @error('hours')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="label" for="price_month">
                        Цена за месяц
                    </label>
                    <div class="control">
                        <input type="text"
                               class="form-control @error('price_month') is-invalid @enderror"
                               name="price_month"
                               id="price_month"
                               value="{{old('price_month')}}"
                               required>
                    </div>

                    @error('price_month')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label class="label" for="price_year">
                        Цена за год
                    </label>
                    <div class="control">
                        <input type="text"
                               class="form-control @error('price_year') is-invalid @enderror"
                               name="price_year"
                               id="price_year"
                               value="{{old('price_year')}}"
                               required>
                    </div>

                    @error('price_year')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>


                <div class="form-group">
                    <label class="label" for="course_category_id">
                        Категория
                    </label>
                    <div class="control">
                        <select class="form-control" id="course_category_id" name="course_category_id" required>
                            <option value="" selected disabled>Выберите категорию курса</option>
                            @foreach(\App\Models\CourseCategory::all() as $category)
                                <option
                                    @if(old('course_category_id'))selected
                                    @endif
                                    value="{{$category->id}}">{{$category->title_ru}}</option>
                            @endforeach
                        </select>
                    </div>

                    @error('course_category_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>


            </div>
            <div class="card-footer">
                <div class="form-group">
                    <div class="control">
                        <button class="btn btn-success float-right" type="submit">Сохранить</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
