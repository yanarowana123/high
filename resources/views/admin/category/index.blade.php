@extends('admin.layouts.main')

@section('content')
    <div>
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between mt-3">
                    <h2>Категории курсов</h2>
                    <a href="{{route('admin.category.create')}}" class="btn btn-success mb-2">Добавить</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table table-responsive">
                    <table class="table table-hover">
                        <thead>

                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Заголовок</th>
                            <th scope="col">Title</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody class="">
                        @foreach($categories as $index=>$category)
                            <tr>
                                <th scope="row"><a
                                        href="{{route('admin.category.edit',$category->id)}}">{{$index+1}} </a>
                                </th>
                                <th scope="row"><a
                                        href="{{route('admin.category.edit',$category->id)}}">{{$category->title_ru}} </a>
                                </th>
                                <th scope="row"><a
                                        href="{{route('admin.category.edit',$category->id)}}">{{$category->title_en}} </a>
                                </th>
                                <td><a onclick="return confirm('Are you sure?')"
                                       href="{{route('admin.category.delete',$category)}}"><i
                                            class="fa fa-trash fa-fw"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>

                </div>
            </div>
            <div class="card-footer">
                {{ $categories->links() }}
            </div>
        </div>
    </div>

@endsection
