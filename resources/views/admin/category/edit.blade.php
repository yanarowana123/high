@extends('admin.layouts.main')
@section('content')
    <div class="card">
        <div class="card-header">
            <h1>Редактировать категорию</h1>
        </div>
        <form method="POST" action="{{route('admin.category.update',$category->id)}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card-body">


                <div class="form-group">
                    <label class="label" for="title_ru">
                        Заголовок
                    </label>

                    <div class="control">
                        <input type="text"
                               class="form-control @error('title_ru') is-invalid @enderror"
                               name="title_ru"
                               id="title_ru"
                               value="{{$category->title_ru}}"
                               required>

                    </div>

                    @error('title_ru')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>


                <div class="form-group">
                    <label class="label" for="title_en">
                        Title
                    </label>

                    <div class="control">
                        <input type="text"
                               class="form-control @error('title_en') is-invalid @enderror"
                               name="title_en"
                               id="title_en"
                               value="{{$category->title_en}}"
                               required>

                    </div>

                    @error('title_en')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>


            </div>
            <div class="card-footer">
                <div class="form-group">
                    <div class="control">
                        <button class="btn btn-success float-right" type="submit">Сохранить</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
