<?php

use App\Http\Livewire\Home\ProgramComponent;
use App\Http\Livewire\Home\WelcomeComponent;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\HomeController@index')->name('home');

Route::get('/all', 'App\Http\Controllers\HomeController@all')->name('all');

Route::get('/fin', 'App\Http\Controllers\HomeController@fin')->name('fin');

Route::get('/contacts', 'App\Http\Controllers\HomeController@contacts')->name('contacts');

Route::get('/math', 'App\Http\Controllers\HomeController@math')->name('math');

Route::get('/news','App\Http\Controllers\HomeController@news' )->name('news');

Route::get('/news/{slug}', 'App\Http\Controllers\HomeController@newsShow' )->name('news.show');

Route::get('/prog', 'App\Http\Controllers\HomeController@prog')->name('prog');

Route::get('/about', 'App\Http\Controllers\HomeController@about')->name('about');

Route::get('/stud', 'App\Http\Controllers\HomeController@stud')->name('stud');


Route::get('/schedule', 'App\Http\Controllers\HomeController@schedule')->name('schedule');

Route::middleware(['auth:sanctum', 'verified'])->prefix('/admin')->namespace('App\Http\Controllers\Admin')
    ->name('admin.')
    ->group(function () {
        Route::get('/dashboard', function () {
            return view('dashboard');
        })->name('dashboard');

        Route::get('/', function () {
            Storage::deleteDirectory('livewire-tmp');
            return redirect()->route('admin.contact.index');
        })->name('index');

        Route::prefix('home/')->name('home.')->group(function () {
            Route::get('/', 'HomeController@index')->name('index');
        });

        Route::prefix('/contact')->name('contact.')->group(function () {
            Route::get('/', 'ContactController@index')->name('index');
            Route::get('/{contact}/delete', 'ContactController@delete')->name('delete');
            Route::get('/create', 'ContactController@create')->name('create');
            Route::post('/', 'ContactController@store')->name('index');
            Route::get('/{contact}/edit', 'ContactController@edit')->name('edit');
            Route::put('/{contact}/update', 'ContactController@update')->name('update');
        });

        Route::prefix('/category')->name('category.')->group(function () {
            Route::get('/', 'CategoryController@index')->name('index');
            Route::get('/create', 'CategoryController@create')->name('create');
            Route::post('/', 'CategoryController@store')->name('index');
            Route::get('/{category}/edit', 'CategoryController@edit')->name('edit');
            Route::put('/{category}', 'CategoryController@update')->name('update');
            Route::get('/{category}/delete', 'CategoryController@destroy')->name('delete');
        });

        Route::prefix('/course')->name('course.')->group(function () {
            Route::get('/', 'CourseController@index')->name('index');
            Route::get('/create', 'CourseController@create')->name('create');
            Route::post('/', 'CourseController@store')->name('index');
            Route::get('/{course}/edit', 'CourseController@edit')->name('edit');
            Route::put('/{course}', 'CourseController@update')->name('update');
            Route::get('/{course}/delete', 'CourseController@destroy')->name('delete');
        });

    });

Route::middleware(['auth:sanctum', 'verified'])->prefix('/admin')->name('admin.')->group(function () {


    Route::get('/default/{model}/{block}', \App\Http\Livewire\About\DefaultComponent::class)->name('default');
    Route::get('/feature/{model}', \App\Http\Livewire\Home\FeatureComponent::class)->name('feature');
    Route::get('/content/{model}', \App\Http\Livewire\ContentComponent::class)->name('content');

    Route::prefix('home/')->name('home.')->group(function () {
        Route::get('/program', ProgramComponent::class)->name('program');
        Route::get('/welcome', WelcomeComponent::class)->name('welcome');
        Route::get('/help', \App\Http\Livewire\Home\HelpComponent::class)->name('help');
        Route::get('/logo', \App\Http\Livewire\Home\LogoComponent::class)->name('logo');
        Route::get('/footer', \App\Http\Livewire\Home\FooterComponent::class)->name('footer');
    });


    Route::prefix('video/')->name('video.')->group(function () {
        Route::get('{name}', \App\Http\Livewire\VideoComponent::class)->name('index');
    });


    Route::prefix('about/')->name('about.')->group(function () {
        Route::get('/feature', \App\Http\Livewire\About\FeatureComponent::class)->name('feature');
        Route::get('/staff', \App\Http\Livewire\About\StaffComponent::class)->name('staff');
    });

    Route::prefix('prog/')->name('prog.')->group(function () {
        Route::get('/advantage', \App\Http\Livewire\ProgAdvantageComponent::class)->name('advantage');
    });

    Route::prefix('schedule/')->name('schedule.')->group(function () {
        Route::get('/edit/{course}', \App\Http\Livewire\ScheduleComponent::class)->name('edit');
    });

    Route::prefix('articles/')->name('article.')->group(function () {
        Route::get('/create', \App\Http\Livewire\AddArticleComponent::class)->name('create');
        Route::get('/edit/{article}', \App\Http\Livewire\AddArticleComponent::class)->name('edit');

        Route::get('/{type}', \App\Http\Livewire\AllArticlesComponent::class)->name('index');
    });

});
