<?php

namespace Database\Seeders;

use App\Models\Engineer;
use App\Models\Everyone;
use App\Models\Fin;
use App\Models\Prog;
use App\Models\Stud;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('engineers')->insert(
            [
                'title_ru' => 'I – Microeconomics',
                'title_en' => 'I – Microeconomics',
                'content_ru' => 'Экономическая Кинематика, Экономическая Динамика, Финансовая Механика. Пространства Товаров, Трудозатрат и Инвестиций',
                'content_en' => 'Economic Kinematics, Economic Dynamics, Financial Mechanics. Spaces of Goods, Labor and Investment',
                'block' => Engineer::FOOTER
            ]
        );

        DB::table('engineers')->insert(
            [
                'title_ru' => 'II – Macroeconomics',
                'title_en' => 'II – Macroeconomics',
                'content_ru' => 'Экономическая Статика, Законы Сохранения Экономики, Математическая Теория Цены и Заработной Платы, Математическая Теория Денег, Количественный и Качественный Анализ',
                'content_en' => 'Economic Statics, Conservation Laws of Economics, Mathematical Theory of Price and Wages, Mathematical Theory of Money, Quantitative and Qualitative Analysis',
                'block' => Engineer::FOOTER
            ]
        );

        DB::table('progs')->insert(
            [
                'title_ru' => 'HM Basic',
                'title_en' => 'HM Basic',
                'content_ru' => 'HM Basic дает базовый аппарат Высшей Математики. Курс состоит из Аналитической Геометрии и Линейной Алгебры, где описаны операции с векторами, на языке которых построена вся современная математика. Также в курсе даны основы анализа Непрерывных Объектов: Математический Анализ, и анализ Дискретных (не непрерывных) Объектов: Комбинаторика, Математическая Логика, Теория Множеств и Теория Вероятности',
                'content_en' => 'The HM Basic course provides the basic of Higher Mathematics. The course consists of Analytical Geometry and Linear Algebra, which describe operations with vectors, which all modern mathematics is built. The course also covers the basics of analyzing continuous objects: Mathematical Analysis, and the analysis of discrete (non-continuous) objects: Combinatorics, Mathematical Logic, Theory of Sets and Theory of Probability',
                'block' => Prog::FOOTER
            ]
        );

        DB::table('progs')->insert(
            [
                'title_ru' => 'HM Digital',
                'title_en' => 'HM Digital',
                'content_ru' => 'Лекции HM Digital дают более углубленное изучение Дискретного Анализа, который описывает процесс Выбора в цифровых системах. В курс включены: Высшая Алгебра — дискретные числовые системы; Теории Алгоритмов, Графов и Игр, а также, Теория Математической Статистики, изучающая случайные процессы',
                'content_en' => 'HM Digital\'s Lectures provide a more in-depth study of Discrete Analysis, which describes the Choice process in digital systems. The course includes: Higher Algebra - discrete number systems; Theory of Algorithms, Graphs and Games, as well as Theory of Mathematical Statistics, which studies stochastic processes',
                'block' => Prog::FOOTER
            ]
        );

        DB::table('fins')->insert(
            [
                'title_ru' => 'HM Basic',
                'title_en' => 'HM Basic',
                'content_ru' => 'HM Basic дает базовый аппарат Высшей Математики. Курс состоит из Аналитической Геометрии и Линейной Алгебры, где описаны операции с векторами, на языке которых построена вся современная математика. Также в курсе даны основы анализа Непрерывных Объектов: Математический Анализ, и анализ Дискретных (не непрерывных) Объектов: Комбинаторика, Математическая Логика, Теория Множеств и Теория Вероятности',
                'content_en' => 'The HM Basic course provides the basic of Higher Mathematics. The course consists of Analytical Geometry and Linear Algebra, which describe operations with vectors, which all modern mathematics is built. The course also covers the basics of analyzing continuous objects: Mathematical Analysis, and the analysis of discrete (non-continuous) objects: Combinatorics, Mathematical Logic, Theory of Sets and Theory of Probability',
                'block' => FIN::FOOTER
            ]
        );

        DB::table('fins')->insert(
            [
                'title_ru' => 'HM Profit',
                'title_en' => 'HM Profit',
                'content_ru' => 'В лекции HM Profit включены дисциплины Непрерывных Объектов, которые описывают финансово-экономические процессы. А именно, Теории Дифференциальных Уравнений (обыкновенных и в частных производных), Теории Функций (действительных и комплексных), Функционального Анализа, Вариационный Анализ и Теория Оптимального Управления. В курсе даны основы дифференциального, интегрального и вариационного исчисления, а также, анализ функций и систем уравнений.',
                'content_en' => 'The HM Profit lectures include Continuous Objects disciplines that describe financial and economic processes. Namely, the Theory of Differential Equations (ordinary and partial derivatives), Theory of Functions (real and complex), Functional Analysis, Variational Analysis and Optimal Control Theory. The course covers the basics of differential, integral and variational calculus, as well as analysis of functions and systems of equations.',
                'block' => FIN::FOOTER
            ]
        );
        DB::table('fins')->insert(
            [
                'title_ru' => 'HM Risk Management',
                'title_en' => 'HM Risk Management',
                'content_ru' => 'Модели MA, AR и ARMA. Современная теория портфельных инвестиций. The Capital Asset Pricing Model. Метод оценки риска VaR (risk metric: Value-at-Risk (VaR)). Модели ARCH и GARCH. Динамическое (нестахостическое) прогнозирование.',
                'content_en' => 'MA, AR and ARMA models. Modern theory of portfolio investments. The Capital Asset Pricing Model. VaR (risk metric: Value-at-Risk (VaR)) method. ARCH and GARCH models. Dynamic (non-stochastic) forecasting.',
                'block' => FIN::FOOTER
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => 'HM Basic',
                'title_en' => 'HM Basic',
                'content_ru' => 'HM Basic дает базовый аппарат Высшей Математики. Курс состоит из Аналитической Геометрии и Линейной Алгебры, где описаны операции с векторами, на языке которых построена вся современная математика. Также в курсе даны основы анализа Непрерывных Объектов: Математический Анализ, и анализ Дискретных (не непрерывных) Объектов: Комбинаторика, Математическая Логика, Теория Множеств и Теория Вероятности',
                'content_en' => 'The HM Basic course provides the basic of Higher Mathematics. The course consists of Analytical Geometry and Linear Algebra, which describe operations with vectors, which all modern mathematics is built. The course also covers the basics of analyzing continuous objects: Mathematical Analysis, and the analysis of discrete (non-continuous) objects: Combinatorics, Mathematical Logic, Theory of Sets and Theory of Probability',
                'block' => Stud::FOOTER
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => 'HM Digital',
                'title_en' => 'HM Digital',
                'content_ru' => 'Лекции HM Digital дают более углубленное изучение Дискретного Анализа, который описывает процесс Выбора в цифровых системах. В курс включены: Высшая Алгебра — дискретные числовые системы; Теории Алгоритмов, Графов и Игр, а также, Теория Математической Статистики, изучающая случайные процессы',
                'content_en' => 'HM Digital\'s Lectures provide a more in-depth study of Discrete Analysis, which describes the Choice process in digital systems. The course includes: Higher Algebra - discrete number systems; Theory of Algorithms, Graphs and Games, as well as Theory of Mathematical Statistics, which studies stochastic processes',
                'block' => Stud::FOOTER
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => 'HM Profit',
                'title_en' => 'HM Profit',
                'content_ru' => 'В лекции HM Profit включены дисциплины Непрерывных Объектов, которые описывают финансово-экономические процессы. А именно, Теории Дифференциальных Уравнений (обыкновенных и в частных производных), Теории Функций (действительных и комплексных), Функционального Анализа, Вариационный Анализ и Теория Оптимального Управления. В курсе даны основы дифференциального, интегрального и вариационного исчисления, а также, анализ функций и систем уравнений.',
                'content_en' => 'The HM Profit lectures include Continuous Objects disciplines that describe financial and economic processes. Namely, the Theory of Differential Equations (ordinary and partial derivatives), Theory of Functions (real and complex), Functional Analysis, Variational Analysis and Optimal Control Theory. The course covers the basics of differential, integral and variational calculus, as well as analysis of functions and systems of equations.',
                'block' => Stud::FOOTER
            ]
        );



        DB::table('everyones')->insert(
            [
                'title_ru' => 'HM Basic',
                'title_en' => 'HM Basic',
                'content_ru' => 'HM Basic дает базовый аппарат Высшей Математики. Курс состоит из Аналитической Геометрии и Линейной Алгебры, где описаны операции с векторами, на языке которых построена вся современная математика. Также в курсе даны основы анализа Непрерывных Объектов: Математический Анализ, и анализ Дискретных (не непрерывных) Объектов: Комбинаторика, Математическая Логика, Теория Множеств и Теория Вероятности',
                'content_en' => 'The HM Basic course provides the basic of Higher Mathematics. The course consists of Analytical Geometry and Linear Algebra, which describe operations with vectors, which all modern mathematics is built. The course also covers the basics of analyzing continuous objects: Mathematical Analysis, and the analysis of discrete (non-continuous) objects: Combinatorics, Mathematical Logic, Theory of Sets and Theory of Probability',
                'block' => Everyone::FOOTER
            ]
        );

        DB::table('everyones')->insert(
            [
                'title_ru' => 'HM Digital',
                'title_en' => 'HM Digital',
                'content_ru' => 'Лекции HM Digital дают более углубленное изучение Дискретного Анализа, который описывает процесс Выбора в цифровых системах. В курс включены: Высшая Алгебра — дискретные числовые системы; Теории Алгоритмов, Графов и Игр, а также, Теория Математической Статистики, изучающая случайные процессы',
                'content_en' => 'HM Digital\'s Lectures provide a more in-depth study of Discrete Analysis, which describes the Choice process in digital systems. The course includes: Higher Algebra - discrete number systems; Theory of Algorithms, Graphs and Games, as well as Theory of Mathematical Statistics, which studies stochastic processes',
                'block' => Everyone::FOOTER
            ]
        );

        DB::table('everyones')->insert(
            [
                'title_ru' => 'HM Profit',
                'title_en' => 'HM Profit',
                'content_ru' => 'В лекции HM Profit включены дисциплины Непрерывных Объектов, которые описывают финансово-экономические процессы. А именно, Теории Дифференциальных Уравнений (обыкновенных и в частных производных), Теории Функций (действительных и комплексных), Функционального Анализа, Вариационный Анализ и Теория Оптимального Управления. В курсе даны основы дифференциального, интегрального и вариационного исчисления, а также, анализ функций и систем уравнений.',
                'content_en' => 'The HM Profit lectures include Continuous Objects disciplines that describe financial and economic processes. Namely, the Theory of Differential Equations (ordinary and partial derivatives), Theory of Functions (real and complex), Functional Analysis, Variational Analysis and Optimal Control Theory. The course covers the basics of differential, integral and variational calculus, as well as analysis of functions and systems of equations.',
                'block' => Everyone::FOOTER
            ]
        );

    }
}
