<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fins', function (Blueprint $table) {
            $table->id();
            $table->string('title_ru')->nullable();
            $table->string('title_en')->nullable();
            $table->text('text_ru')->nullable();
            $table->text('text_en')->nullable();
            $table->longText('content_ru')->nullable();
            $table->longText('content_en')->nullable();
            $table->string('file')->nullable();
            $table->unsignedInteger('block');
            $table->timestamps();
        });

        DB::table('fins')->insert(
            [
                'file' => 'img/School-page/depositphotos_203145164-stock-video-robot-hand-and-butterfly.mp4',
                'block' => \App\Models\About::VIDEO
            ]
        );

        DB::table('fins')->insert(
            [
                'title_ru' => '7',
                'title_en' => '7',
                'content_ru' => 'инновационных курсов',
                'content_en' => 'инновационных курсов',
                'block' => \App\Models\Fin::FEATURE
            ]
        );

        DB::table('fins')->insert(
            [
                'title_ru' => '15',
                'title_en' => '15',
                'content_ru' => 'модулей высшей математики',
                'content_en' => 'модулей высшей математики',
                'block' => \App\Models\Fin::FEATURE
            ]
        );

        DB::table('fins')->insert(
            [
                'title_ru' => '48 ч.',
                'title_en' => '48 ч.',
                'content_ru' => 'продолжительность курса',
                'content_en' => 'продолжительность курса',
                'block' => \App\Models\Fin::FEATURE
            ]
        );

        DB::table('fins')->insert(
            [
                'title_ru' => '',
                'title_en' => '',
                'content_ru' => '
          <div class="fin-texts__descr">
            Практика
            <span>
              Финансового и Проектного Анализа, SWOT и ABC Анализа, Риск
              Менеджмент и Финансовый Инжиниринг
            </span>
            все больше и глубже используют аппарат Высшей Математики.
          </div>
          <div class="fin-texts__descr">
            Все чаще такие понятия как
            <span>
              Стохастический Анализ, Степенные и Временные Ряды, Обращение
              Функциональной Зависимости
            </span>
            и многое другое становятся повседневными и необходимыми
            инструментами.
          </div>
        ',
                'content_en' => '
          <div class="fin-texts__descr">
            Практика
            <span>
              Финансового и Проектного Анализа, SWOT и ABC Анализа, Риск
              Менеджмент и Финансовый Инжиниринг
            </span>
            все больше и глубже используют аппарат Высшей Математики.
          </div>
          <div class="fin-texts__descr">
            Все чаще такие понятия как
            <span>
              Стохастический Анализ, Степенные и Временные Ряды, Обращение
              Функциональной Зависимости
            </span>
            и многое другое становятся повседневными и необходимыми
            инструментами.
          </div>
       ',
                'block' => \App\Models\Fin::INFO
            ]
        );


        DB::table('fins')->insert(
            [
                'title_ru' => 'Требования крупных мировых компаний',
                'title_en' => 'Требования крупных мировых компаний',
                'content_ru' => '<p>
              Сегодня требования крупных мировых Финансовых институтов,
              Консалтинговых и Холдинговых компаний к высокому уровню знаний
              Высшей Математики для Экономистов и Финансистов из категории
              желательных переходят в категорию обязательных
            </p>
            <p>
              Предлагаемые Школой Высшей Математики лекции помогут вам
              восполнить пробелы в математике и повысить профессиональный
              уровень. Курсы подобраны таким образом, чтобы в легкой и простой
              форме научить слушателей тем инструментам, без которых современная
              экономическая и финансовая деятельность невозможна
            </p>',
                'content_en' => '<p>
              Сегодня требования крупных мировых Финансовых институтов,
              Консалтинговых и Холдинговых компаний к высокому уровню знаний
              Высшей Математики для Экономистов и Финансистов из категории
              желательных переходят в категорию обязательных
            </p>
            <p>
              Предлагаемые Школой Высшей Математики лекции помогут вам
              восполнить пробелы в математике и повысить профессиональный
              уровень. Курсы подобраны таким образом, чтобы в легкой и простой
              форме научить слушателей тем инструментам, без которых современная
              экономическая и финансовая деятельность невозможна
            </p>',
                'block' => \App\Models\Fin::DEMAND
            ]
        );


        DB::table('fins')->insert(
            [
                'title_ru' => '',
                'title_en' => '',
                'content_ru' => 'Обучение на наших курсах, имея глубокое понимание предмета,
позволит в последующем успешно сертифицироваться по мировым
финансовым программам, таким как FRM и CFA (GARP).',
                'content_en' => 'Обучение на наших курсах, имея глубокое понимание предмета,
позволит в последующем успешно сертифицироваться по мировым
финансовым программам, таким как FRM и CFA (GARP).',
                'block' => \App\Models\Fin::PROGS
            ]
        );


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fins');
    }
}
