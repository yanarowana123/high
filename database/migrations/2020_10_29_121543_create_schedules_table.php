<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->id();
            $table->string('title_ru')->nullable();
            $table->string('title_en')->nullable();
            $table->text('text_ru')->nullable();
            $table->text('text_en')->nullable();
            $table->longText('content_ru')->nullable();
            $table->longText('content_en')->nullable();
            $table->string('file')->nullable();
            $table->unsignedInteger('block');
            $table->timestamps();
        });

        \Illuminate\Support\Facades\DB::table('schedules')->insert(
            [
                'title_ru' => 'Расписание осенний семестр 2020',
                'title_en' => 'Расписание осенний семестр 2020',
                'content_ru' => 'Набор Старшеклассников в семестр — только 3 группы, минимальный возраст — 15 лет',
                'content_en' => 'Набор Старшеклассников в семестр — только 3 группы, минимальный возраст — 15 лет',
                'block' => \App\Models\Schedule::TABLETITLE,

            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
