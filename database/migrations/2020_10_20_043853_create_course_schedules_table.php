<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCourseSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_schedules', function (Blueprint $table) {
            $table->id();
            $table->string('title_ru');
            $table->string('title_en');
            $table->string('content_ru');
            $table->string('content_en');
            $table->foreignId('course_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });

        DB::table('course_schedules')->insert(
            [
                'title_ru' => 'Сб',
                'title_en' => 'Сб',
                'content_ru' => '16:00 - 18:00',
                'content_en' => '16:00 - 18:00',
                'course_id' => 1,
            ]
        );

        DB::table('course_schedules')->insert(
            [
                'title_ru' => 'Сб',
                'title_en' => 'Сб',
                'content_ru' => '15:00 - 17:00',
                'content_en' => '15:00 - 17:00',
                'course_id' => 2,
            ]
        );

        DB::table('course_schedules')->insert(
            [
                'title_ru' => 'Чт',
                'title_en' => 'Чт',
                'content_ru' => '17:30 - 19:30',
                'content_en' => '17:30 - 19:30',
                'course_id' => 3,
            ]
        );


        DB::table('course_schedules')->insert(
            [
                'title_ru' => 'Пт',
                'title_en' => 'Пт',
                'content_ru' => '17:30 - 19:30',
                'content_en' => '17:30 - 19:30',
                'course_id' => 4,
            ]
        );

        DB::table('course_schedules')->insert(
            [
                'title_ru' => 'Ср',
                'title_en' => 'Ср',
                'content_ru' => '17:30 - 19:30',
                'content_en' => '17:30 - 19:30',
                'course_id' => 5,
            ]
        );

        DB::table('course_schedules')->insert(
            [
                'title_ru' => 'Вс',
                'title_en' => 'Вс',
                'content_ru' => '17:30 - 19:30',
                'content_en' => '17:30 - 19:30',
                'course_id' => 6,
            ]
        );


        DB::table('course_schedules')->insert(
            [
                'title_ru' => 'Вт',
                'title_en' => 'Вт',
                'content_ru' => '15:30 - 17:00',
                'content_en' => '15:30 - 17:00',
                'course_id' => 7,
            ]
        );

        DB::table('course_schedules')->insert(
            [
                'title_ru' => 'Cб',
                'title_en' => 'Cб',
                'content_ru' => '11:00 - 12:30',
                'content_en' => '11:00 - 12:30',
                'course_id' => 7,
            ]
        );

        DB::table('course_schedules')->insert(
            [
                'title_ru' => 'Вт',
                'title_en' => 'Вт',
                'content_ru' => '17:30 - 19:00',
                'content_en' => '17:30 - 19:00',
                'course_id' => 8,
            ]
        );


        DB::table('course_schedules')->insert(
            [
                'title_ru' => 'Cб',
                'title_en' => 'Cб',
                'content_ru' => '13:00 - 14:30',
                'content_en' => '13:00 - 14:30',
                'course_id' => 8,
            ]
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_schedules');
    }
}
