<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('title_ru');
            $table->string('title_en');
            $table->string('hours')->nullable();;
            $table->string('price_month')->nullable();;
            $table->string('price_year')->nullable();
            $table->foreignId('course_category_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });

        DB::table('courses')->insert(
            [
                'title_ru' => 'HM MEng',
                'title_en' => 'HM MEng',
                'hours' => '8',
                'price_month' => '116 000',
                'price_year' => '313 000',
                'course_category_id' => 1,
            ]
        );


        DB::table('courses')->insert(
            [
                'title_ru' => 'HM Lab ',
                'title_en' => 'HM Lab ',
                'hours' => '2',
                'price_month' => '38 000',
                'price_year' => '',
                'course_category_id' => 2,
            ]
        );

        DB::table('courses')->insert(
            [
                'title_ru' => 'HM Basic',
                'title_en' => 'HM Basic',
                'hours' => '8',
                'price_month' => '64 000',
                'price_year' => '173 000',
                'course_category_id' => 3,
            ]
        );

        DB::table('courses')->insert(
            [
                'title_ru' => 'HM Digital',
                'title_en' => 'HM Digital',
                'hours' => '8',
                'price_month' => '68 000',
                'price_year' => '184 000',
                'course_category_id' => 3,
            ]
        );

        DB::table('courses')->insert(
            [
                'title_ru' => 'HM Profit',
                'title_en' => 'HM Profit',
                'hours' => '8',
                'price_month' => '70 000',
                'price_year' => '189 000',
                'course_category_id' => 3,
            ]
        );

        DB::table('courses')->insert(
            [
                'title_ru' => 'FRM',
                'title_en' => 'FRM',
                'hours' => '8',
                'price_month' => '104 000',
                'price_year' => '281 000',
                'course_category_id' => 3,
            ]
        );

        DB::table('courses')->insert(
            [
                'title_ru' => 'HM Teen I',
                'title_en' => 'HM Teen I',
                'hours' => '12',
                'price_month' => '78 000',
                'price_year' => '211 000',
                'course_category_id' => 4,
            ]
        );

        DB::table('courses')->insert(
            [
                'title_ru' => 'HM Teen II',
                'title_en' => 'HM Teen II',
                'hours' => '12',
                'price_month' => '78 000',
                'price_year' => '211 000',
                'course_category_id' => 4,
            ]
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
