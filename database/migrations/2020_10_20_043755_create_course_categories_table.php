<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCourseCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_categories', function (Blueprint $table) {
            $table->id();
            $table->string('title_ru');
            $table->string('title_en');
            $table->timestamps();
        });

        DB::table('course_categories')->insert(
            [
          'title_ru' => 'Math Eng',
          'title_en' => 'Math Eng',
            ]
        );

        DB::table('course_categories')->insert(
            [
                'title_ru' => 'HM Laboratory ',
                'title_en' => 'HM Laboratory ',
            ]
        );


        DB::table('course_categories')->insert(
            [
                'title_ru' => 'Основные курсы',
                'title_en' => 'Основные курсы',
            ]
        );

        DB::table('course_categories')->insert(
            [
                'title_ru' => 'Старшеклассники',
                'title_en' => 'Старшеклассники',
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_categories');
    }
}
