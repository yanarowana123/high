<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateEngineersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('engineers', function (Blueprint $table) {
            $table->id();
            $table->string('title_ru')->nullable();
            $table->string('title_en')->nullable();
            $table->text('text_ru')->nullable();
            $table->text('text_en')->nullable();
            $table->longText('content_ru')->nullable();
            $table->longText('content_en')->nullable();
            $table->string('file')->nullable();
            $table->unsignedInteger('block');
            $table->timestamps();
        });

        DB::table('engineers')->insert(
            [
                'file' => 'img/School-page/depositphotos_203145164-stock-video-robot-hand-and-butterfly.mp4',
                'block' => \App\Models\Engineer::VIDEO
            ]
        );

        DB::table('engineers')->insert(
            [
                'title_ru' => '7',
                'title_en' => '7',
                'content_ru' => 'инновационных курсов',
                'content_en' => 'инновационных курсов',
                'block' => \App\Models\Engineer::FEATURE
            ]
        );

        DB::table('engineers')->insert(
            [
                'title_ru' => '7',
                'title_en' => '7',
                'content_ru' => 'инновационных курсов',
                'content_en' => 'инновационных курсов',
                'block' => \App\Models\Engineer::FEATURE
            ]
        );

        DB::table('engineers')->insert(
            [
                'title_ru' => '7',
                'title_en' => '7',
                'content_ru' => 'инновационных курсов',
                'content_en' => 'инновационных курсов',
                'block' => \App\Models\Engineer::FEATURE
            ]
        );


        DB::table('engineers')->insert(
            [
                'title_ru' => 'Не имеет аналогов
',
                'title_en' => 'Не имеет аналогов
',
                'content_ru' => '  <span
            >Курс Математический Инжиниринг носит инновационный характер</span
            >, не имеет аналогов <br/>
                    и предназначен для тех, кто хочет понять Законы Экономики',
                'content_en' => '  <span
            >Курс Математический Инжиниринг носит инновационный характер</span
            >, не имеет аналогов <br/>
                    и предназначен для тех, кто хочет понять Законы Экономики',
                'block' => \App\Models\Engineer::BLOCK1
            ]
        );

        DB::table('engineers')->insert(
            [
                'title_ru' => 'В течение всего курса
',
                'title_en' => 'В течение всего курса
',
                'content_ru' => 'слушатели получат знания в области <span>Математического</span>
                    <br/>
                    и <span>Финансового Инжиниринга</span> в экономике, что включает в
                    себя:',
                'content_en' => 'слушатели получат знания в области <span>Математического</span>
                    <br/>
                    и <span>Финансового Инжиниринга</span> в экономике, что включает в
                    себя:',
                'block' => \App\Models\Engineer::BLOCK2
            ]
        );

        DB::table('engineers')->insert(
            [
                'content_ru' => '  <span>В результате</span> прослушивания лекций
                        <span>студенты научатся</span>',
                'content_en' => '  <span>В результате</span> прослушивания лекций
                        <span>студенты научатся</span>',
                'block' => \App\Models\Engineer::BLOCK3
            ]
        );

        DB::table('engineers')->insert(
            [
                'content_ru' => 'Осуществлять математическое описание деятельности субъектов экономики',
                'content_en' => 'Осуществлять математическое описание деятельности субъектов экономики',
                'block' => \App\Models\Engineer::BLOCK4
            ]
        );

        DB::table('engineers')->insert(
            [
                'content_ru' => 'Осуществлять математическое описание деятельности субъектов экономики',
                'content_en' => 'Осуществлять математическое описание деятельности субъектов экономики',
                'block' => \App\Models\Engineer::BLOCK4
            ]
        );

        DB::table('engineers')->insert(
            [
                'content_ru' => 'Осуществлять математическое описание деятельности субъектов экономики',
                'content_en' => 'Осуществлять математическое описание деятельности субъектов экономики',
                'block' => \App\Models\Engineer::BLOCK4
            ]
        );

        DB::table('engineers')->insert(
            [
                'content_ru' => 'Осуществлять математическое описание деятельности субъектов экономики',
                'content_en' => 'Осуществлять математическое описание деятельности субъектов экономики',
                'block' => \App\Models\Engineer::BLOCK4
            ]
        );


        DB::table('engineers')->insert(
            [
                'content_ru' => 'Овладев знаниями Математического Инжиниринга вы сможете разрабатывать и внедрять Тензорные Управленческие Математические Модели как в области производства, маркетинга и финансов, так и в области стратегического планирования',
                'content_en' => 'Овладев знаниями Математического Инжиниринга вы сможете разрабатывать и внедрять Тензорные Управленческие Математические Модели как в области производства, маркетинга и финансов, так и в области стратегического планирования',
                'block' => \App\Models\Engineer::BLOCK5
            ]
        );


        DB::table('engineers')->insert(
            [
                'title_ru' => 'Математические Законы',
                'title_en' => 'Математические Законы',
                'content_ru' => 'развития микроэкономических и макроэкономических процессов.',
                'content_en' => 'развития микроэкономических и макроэкономических процессов.',
                'block' => \App\Models\Engineer::ADVANTAGE
            ]
        );

        DB::table('engineers')->insert(
            [
                'title_ru' => 'Кинематическая, Динамическая и Статическая',
                'title_en' => 'Кинематическая, Динамическая и Статическая',
                'content_ru' => 'взаимосвязь субъектов экономики на рынках товаров, труда и финансов',
                'content_en' => 'взаимосвязь субъектов экономики на рынках товаров, труда и финансов',
                'block' => \App\Models\Engineer::ADVANTAGE
            ]
        );

        DB::table('engineers')->insert(
            [
                'title_ru' => 'Тензорные Математические',
                'title_en' => 'Тензорные Математические',
                'content_ru' => 'модели бизнес-решений и их эволюция',
                'content_en' => 'модели бизнес-решений и их эволюция',
                'block' => \App\Models\Engineer::ADVANTAGE
            ]
        );


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('engineers');
    }
}
