<?php

use App\Models\About;
use App\Models\Fin;
use App\Models\Prog;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('progs', function (Blueprint $table) {
            $table->id();
            $table->string('title_ru')->nullable();
            $table->string('title_en')->nullable();
            $table->text('text_ru')->nullable();
            $table->text('text_en')->nullable();
            $table->longText('content_ru')->nullable();
            $table->longText('content_en')->nullable();
            $table->string('file')->nullable();
            $table->unsignedInteger('block');
            $table->timestamps();
        });

        DB::table('progs')->insert(
            [
                'file' => 'img/School-page/depositphotos_203145164-stock-video-robot-hand-and-butterfly.mp4',
                'block' => About::VIDEO
            ]
        );

        DB::table('progs')->insert(
            [
                'title_ru' => '48 ч.',
                'title_en' => '48 ч.',
                'content_ru' => 'продолжительность курса',
                'content_en' => 'продолжительность курса',
                'block' => Prog::FEATURE
            ]
        );

        DB::table('progs')->insert(
            [
                'title_ru' => '48 ч.',
                'title_en' => '48 ч.',
                'content_ru' => 'продолжительность курса',
                'content_en' => 'продолжительность курса',
                'block' => Prog::FEATURE
            ]
        );


        DB::table('progs')->insert(
            [
                'title_ru' => '48 ч.',
                'title_en' => '48 ч.',
                'content_ru' => 'продолжительность курса',
                'content_en' => 'продолжительность курса',
                'block' => Prog::FEATURE
            ]
        );

        DB::table('progs')->insert(
            [
                'title_ru' => 'Программирование — это творческая деятельность',
                'title_en' => 'Программирование — это творческая деятельность',
                'content_ru' => 'Однако, в отличии от музыки и живописи она базируется не на эмоциональном восприятии человека,
а на прагматичном моделировании и структурировании объектов программировании.',
                'content_en' => 'Однако, в отличии от музыки и живописи она базируется не на эмоциональном восприятии человека,
а на прагматичном моделировании и структурировании объектов программировании.',
                'block' => Prog::ART
            ]
        );

        DB::table('progs')->insert(
            [
                'title_ru' => 'Хороший программист',
                'title_en' => 'Хороший программист',
                'content_ru' => '<p>
                        Хороший программист наряду со знанием языков программирования
                        должен уметь хорошо понимать и оперировать такими понятиями как
                        алгоритмы и модули, графика и геометрия, логика и конфигурация, и
                        многое тому подобное.
                    </p>
                    <p>
                        Уровень владения этими понятиями определяется уровнем знания
                        Высшей Математики.
                    </p>',
                'content_en' => '<p>
                        Хороший программист наряду со знанием языков программирования
                        должен уметь хорошо понимать и оперировать такими понятиями как
                        алгоритмы и модули, графика и геометрия, логика и конфигурация, и
                        многое тому подобное.
                    </p>
                    <p>
                        Уровень владения этими понятиями определяется уровнем знания
                        Высшей Математики.
                    </p>',
                'block' => Prog::GOOD
            ]
        );


        DB::table('progs')->insert(
            [
                'title_ru' => '',
                'title_en' => '',
                'content_ru' => '<div class="quote__title">
                    Повышай свой IQ уровень — <span>повышай свой уровень Жизни</span>
                </div>',
                'content_en' => '<div class="quote__title">
                    Повышай свой IQ уровень — <span>повышай свой уровень Жизни</span>
                </div>',
                'block' => Prog::IQ
            ]
        );

        DB::table('progs')->insert(
            [
                'title_ru' => 'Машинное Обучение',
                'title_en' => 'Машинное Обучение',
                'content_ru' => '<div class="prog-meaching__descr">
                    Машинное Обучение, задачей которой является определение
                    функциональной зависимости одних параметров от других, просто
                    невозможна без Теории Функций и Функционального Анализа. А для более
                    глубокого понимания и создания Машинного Обучения необходима Теория
                    Векторных Полей
                </div>
                <div class="prog-meaching__subtitle">
                    Ну и куда без Математической Логики
                    <span>в работе с Искусственным Интеллектом</span>
                </div>',
                'content_en' => '<div class="prog-meaching__descr">
                    Машинное Обучение, задачей которой является определение
                    функциональной зависимости одних параметров от других, просто
                    невозможна без Теории Функций и Функционального Анализа. А для более
                    глубокого понимания и создания Машинного Обучения необходима Теория
                    Векторных Полей
                </div>
                <div class="prog-meaching__subtitle">
                    Ну и куда без Математической Логики
                    <span>в работе с Искусственным Интеллектом</span>
                </div>',
                'block' => Prog::ML
            ]
        );


        DB::table('progs')->insert(
            [
                'title_ru' => 'Проанализировав эти тесты, Школа Высшей Математики подготовила лекции, которые позволят вам успешно пройти отбор и осуществить свою мечту.',
                'title_en' => 'Проанализировав эти тесты, Школа Высшей Математики подготовила лекции, которые позволят вам успешно пройти отбор и осуществить свою мечту.',
                'content_ru' => '
                        Если вы хотите получить работу в одной из крупнейших международных
                        HiTech-компаний, таких как
                        <span>Apple, Google, Dell, Microsoft и IBM,</span>
                        то вам надо пройти тестирование, большая часть которых требует
                        глубоких математических знаний и навыков нестандартного мышления.
                   ',
                'content_en' => '
                        Если вы хотите получить работу в одной из крупнейших международных
                        HiTech-компаний, таких как
                        <span>Apple, Google, Dell, Microsoft и IBM,</span>
                        то вам надо пройти тестирование, большая часть которых требует
                        глубоких математических знаний и навыков нестандартного мышления.
                    ',
                'block' => Prog::CORP
            ]
        );

        DB::table('progs')->insert(
            [
                'title_ru' => 'Базы Данных',
                'title_en' => 'Базы Данных',
                'content_ru' => '<div class="prog-products__subtitle">
                            Если принять, что данные – это вектор, а их преобразования –
                            операторы, то <span>Линейная Алгебра</span> сделает работу с
                            Базами Данных легкой и понятной
                        </div>',
                'content_en' => '<div class="prog-products__subtitle">
                            Если принять, что данные – это вектор, а их преобразования –
                            операторы, то <span>Линейная Алгебра</span> сделает работу с
                            Базами Данных легкой и понятной
                        </div>',
                'file' => 'img/prog-page/icon-1.png',
                'block' => Prog::ADVANTAGE
            ]
        );

        DB::table('progs')->insert(
            [
                'title_ru' => 'Базы Данных',
                'title_en' => 'Базы Данных',
                'content_ru' => '<div class="prog-products__subtitle">
                            Если принять, что данные – это вектор, а их преобразования –
                            операторы, то <span>Линейная Алгебра</span> сделает работу с
                            Базами Данных легкой и понятной
                        </div>',
                'content_en' => '<div class="prog-products__subtitle">
                            Если принять, что данные – это вектор, а их преобразования –
                            операторы, то <span>Линейная Алгебра</span> сделает работу с
                            Базами Данных легкой и понятной
                        </div>',
                'file' => 'img/prog-page/icon-1.png',
                'block' => Prog::ADVANTAGE
            ]
        );

        DB::table('progs')->insert(
            [
                'title_ru' => 'Базы Данных',
                'title_en' => 'Базы Данных',
                'content_ru' => '<div class="prog-products__subtitle">
                            Если принять, что данные – это вектор, а их преобразования –
                            операторы, то <span>Линейная Алгебра</span> сделает работу с
                            Базами Данных легкой и понятной
                        </div>',
                'content_en' => '<div class="prog-products__subtitle">
                            Если принять, что данные – это вектор, а их преобразования –
                            операторы, то <span>Линейная Алгебра</span> сделает работу с
                            Базами Данных легкой и понятной
                        </div>',
                'file' => 'img/prog-page/icon-1.png',
                'block' => Prog::ADVANTAGE
            ]
        );

        DB::table('progs')->insert(
            [
                'title_ru' => 'Базы Данных',
                'title_en' => 'Базы Данных',
                'content_ru' => '<div class="prog-products__subtitle">
                            Если принять, что данные – это вектор, а их преобразования –
                            операторы, то <span>Линейная Алгебра</span> сделает работу с
                            Базами Данных легкой и понятной
                        </div>',
                'content_en' => '<div class="prog-products__subtitle">
                            Если принять, что данные – это вектор, а их преобразования –
                            операторы, то <span>Линейная Алгебра</span> сделает работу с
                            Базами Данных легкой и понятной
                        </div>',
                'file' => 'img/prog-page/icon-1.png',
                'block' => Prog::ADVANTAGE
            ]
        );

        DB::table('progs')->insert(
            [
                'title_ru' => 'Базы Данных',
                'title_en' => 'Базы Данных',
                'content_ru' => '<div class="prog-products__subtitle">
                            Если принять, что данные – это вектор, а их преобразования –
                            операторы, то <span>Линейная Алгебра</span> сделает работу с
                            Базами Данных легкой и понятной
                        </div>',
                'content_en' => '<div class="prog-products__subtitle">
                            Если принять, что данные – это вектор, а их преобразования –
                            операторы, то <span>Линейная Алгебра</span> сделает работу с
                            Базами Данных легкой и понятной
                        </div>',
                'file' => 'img/prog-page/icon-1.png',
                'block' => Prog::ADVANTAGE
            ]
        );

        DB::table('progs')->insert(
            [
                'title_ru' => 'Базы Данных',
                'title_en' => 'Базы Данных',
                'content_ru' => '<div class="prog-products__subtitle">
                            Если принять, что данные – это вектор, а их преобразования –
                            операторы, то <span>Линейная Алгебра</span> сделает работу с
                            Базами Данных легкой и понятной
                        </div>',
                'content_en' => '<div class="prog-products__subtitle">
                            Если принять, что данные – это вектор, а их преобразования –
                            операторы, то <span>Линейная Алгебра</span> сделает работу с
                            Базами Данных легкой и понятной
                        </div>',
                'file' => 'img/prog-page/icon-1.png',
                'block' => Prog::ADVANTAGE
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('progs');
    }
}
