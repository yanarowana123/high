<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateStudsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studs', function (Blueprint $table) {
            $table->id();
            $table->string('title_ru')->nullable();
            $table->string('title_en')->nullable();
            $table->text('text_ru')->nullable();
            $table->text('text_en')->nullable();
            $table->longText('content_ru')->nullable();
            $table->longText('content_en')->nullable();
            $table->string('file')->nullable();
            $table->unsignedInteger('block');
            $table->timestamps();
        });

        DB::table('studs')->insert(
            [
                'file' => 'img/School-page/depositphotos_203145164-stock-video-robot-hand-and-butterfly.mp4',
                'block' => \App\Models\Stud::VIDEO
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => '48 ч.',
                'title_en' => '48 ч.',
                'content_ru' => 'продолжительность курса',
                'content_en' => 'продолжительность курса',
                'block' => \App\Models\Stud::FEATURE
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => '48 ч.',
                'title_en' => '48 ч.',
                'content_ru' => 'продолжительность курса',
                'content_en' => 'продолжительность курса',
                'block' => \App\Models\Stud::FEATURE
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => '48 ч.',
                'title_en' => '48 ч.',
                'content_ru' => 'продолжительность курса',
                'content_en' => 'продолжительность курса',
                'block' => \App\Models\Stud::FEATURE
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => 'Истина
всегда проста',
                'title_en' => 'Истина
всегда проста',
                'content_ru' => 'Если вы не понимаете предмет или вам не интересно, значит вы не нашли своего учителя',
                'content_en' => 'Если вы не понимаете предмет или вам не интересно, значит вы не нашли своего учителя',
                'block' => \App\Models\Stud::TRUTH
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => 'Школа Высшей Математики
',
                'title_en' => 'Школа Высшей Математики
',
                'content_ru' => 'предлагает получить начальные знания и практические навыки
в основных разделах Высшей Математики',
                'content_en' => 'предлагает получить начальные знания и практические навыки
в основных разделах Высшей Математики',
                'block' => \App\Models\Stud::OFFER
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => 'α',
                'title_en' => 'α',
                'content_ru' => '
                    <p>
                        Вы <span>старшеклассник</span>, который хочет развивать свои
                        навыки и постигать новые вершины:
                    </p>
                    <p>
                        научиться <span>нестандартно мыслить</span>, не принимать все на
                        веру, с ходу решать любые проблемы используя красивые
                        математические модели
                    </p>
                    <p>
                        или просто оказаться
                        <span>в кругу единомышленникови обрести друзей</span>, быть
                        интересным для окружающих
                    </p>
               ',
                'content_en' => '
                    <p>
                        Вы <span>старшеклассник</span>, который хочет развивать свои
                        навыки и постигать новые вершины:
                    </p>
                    <p>
                        научиться <span>нестандартно мыслить</span>, не принимать все на
                        веру, с ходу решать любые проблемы используя красивые
                        математические модели
                    </p>
                    <p>
                        или просто оказаться
                        <span>в кругу единомышленникови обрести друзей</span>, быть
                        интересным для окружающих
                    </p>
               ',
                'block' => \App\Models\Stud::ADVANTAGE
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => 'α',
                'title_en' => 'α',
                'content_ru' => '
                   <p>
                        Вы хотите поступить или уже учитесь в специализированной школе,
                        такой как
                        <span>РФМШ, НИШ (NIS)</span> и другие:
                    </p>
                    <p>
                        но считаете что Вы готовы выйти за рамки школьной программы и
                        получить азы Высшей Математики, например в Комбинаторике, Теории
                        Чисел и Линейной Алгебре
                    </p>
               ',
                'content_en' => '
                            <p>
                        Вы хотите поступить или уже учитесь в специализированной школе,
                        такой как
                        <span>РФМШ, НИШ (NIS)</span> и другие:
                    </p>
                    <p>
                        но считаете что Вы готовы выйти за рамки школьной программы и
                        получить азы Высшей Математики, например в Комбинаторике, Теории
                        Чисел и Линейной Алгебре
                    </p>
               ',
                'block' => \App\Models\Stud::ADVANTAGE
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => 'α',
                'title_en' => 'α',
                'content_ru' => '
                   <p>
                        Вы хотите с легкостью сдать такие экзамены как
                        <span>SAT, GRE и GMAT</span> и стать студентами таких
                        университетов как Cambridge, Harvard, MIT и получить
                        максимальные возможности, чтобы влиться в мировую элиту
                    </p>
               ',
                'content_en' => '
                  <p>
                        Вы хотите с легкостью сдать такие экзамены как
                        <span>SAT, GRE и GMAT</span> и стать студентами таких
                        университетов как Cambridge, Harvard, MIT и получить
                        максимальные возможности, чтобы влиться в мировую элиту
                    </p>
               ',
                'block' => \App\Models\Stud::ADVANTAGE
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => 'α',
                'title_en' => 'α',
                'content_ru' => '
                    <p>
                        Вы хотите понять с чем связать свою жизнь и какую профессию
                        выбрать:
                    </p>
                    <p>
                        писать программы c <span>IBM</span>, собирать атомные двигатели
                        в <span>Rolls-Royce</span>, запускать ракеты, торговать на бирже
                    </p>
               ',
                'content_en' => '
                    <p>
                        Вы хотите понять с чем связать свою жизнь и какую профессию
                        выбрать:
                    </p>
                    <p>
                        писать программы c <span>IBM</span>, собирать атомные двигатели
                        в <span>Rolls-Royce</span>, запускать ракеты, торговать на бирже
                    </p>
               ',
                'block' => \App\Models\Stud::ADVANTAGE
            ]
        );


        DB::table('studs')->insert(
            [
                'title_ru' => '',
                'title_en' => '',
                'content_ru' => '

                Мы поможем вам <span>осуществить вашу мечту</span>

               ',
                'content_en' => '

                Мы поможем вам <span>осуществить вашу мечту</span>

               ',
                'block' => \App\Models\Stud::HELP
            ]
        );


        DB::table('studs')->insert(
            [
                'title_ru' => ' Программа курса для Старшеклассников',
                'title_en' => ' Программа курса для Старшеклассников',
                'content_ru' => '
                <div class="stud-program__subtitle">
                    <i>
                        В связи с тем, что курсы Для Старшеклассников экспериментальные и
                        не являются основным направлением центра, количество групп для
                        школьников <span>ограничено</span>
                    </i>
                </div>
                <div class="stud-program__descr">
                    <p>
                        В курсах, наряду со школьной программой, будут даны основы Высшей
                        Математики. При этом, знакомство и освоение элементов Высшей
                        Математики будет происходить естественным образом. Например, как
                        решая алгебраическое уравнение можно использовать принципы Теории
                        Чисел или как легко решается геометрическая задача при помощи
                        векторов Аналитической Геометрии.
                    </p>
                    <p>
                        Основной упор в курсах сделан на решение ключевых задач и
                        примеров, что позволит легко освоить сложные математические методы
                        и приемы без заучивания теорем и их доказательств.
                    </p>
                    <p>
                        Курсы <span>HM Teen I, HM Teen II</span> отличаются друг от друга
                        по углубленности изучаемых модулей Высшей Математики.
                    </p>
                </div>
               ',
                'content_en' => '
                <div class="stud-program__subtitle">
                    <i>
                        В связи с тем, что курсы Для Старшеклассников экспериментальные и
                        не являются основным направлением центра, количество групп для
                        школьников <span>ограничено</span>
                    </i>
                </div>
                <div class="stud-program__descr">
                    <p>
                        В курсах, наряду со школьной программой, будут даны основы Высшей
                        Математики. При этом, знакомство и освоение элементов Высшей
                        Математики будет происходить естественным образом. Например, как
                        решая алгебраическое уравнение можно использовать принципы Теории
                        Чисел или как легко решается геометрическая задача при помощи
                        векторов Аналитической Геометрии.
                    </p>
                    <p>
                        Основной упор в курсах сделан на решение ключевых задач и
                        примеров, что позволит легко освоить сложные математические методы
                        и приемы без заучивания теорем и их доказательств.
                    </p>
                    <p>
                        Курсы <span>HM Teen I, HM Teen II</span> отличаются друг от друга
                        по углубленности изучаемых модулей Высшей Математики.
                    </p>
                </div>
                 ',
                'block' => \App\Models\Stud::HIGH
            ]
        );


        DB::table('studs')->insert(
            [
                'title_ru' => 'В ВУЗе, обычно, на студентов сваливается лавина новых знаний, которые часто даются в тяжелой, перегруженной форме и студенты всю учёбу страдают от непонимания.',
                'title_en' => 'В ВУЗе, обычно, на студентов сваливается лавина новых знаний, которые часто даются в тяжелой, перегруженной форме и студенты всю учёбу страдают от непонимания.',
                'content_ru' => '
                    Занятия в нашей школе дадут вам понимание
                    <span>Высшей Математики</span> в простой и легкой форме и научат
                    вас решать задачи, которые до этого вам казались очень сложными
               ',
                'content_en' => '
                    Занятия в нашей школе дадут вам понимание
                    <span>Высшей Математики</span> в простой и легкой форме и научат
                    вас решать задачи, которые до этого вам казались очень сложными
               ',
                'block' => \App\Models\Stud::STUDENT
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => '',
                'title_en' => '',
                'content_ru' => '  <p>
                            <span>Экзамены</span> по математике (и наверняка – по физике и
                            химии) в университете <span>станут простыми</span>
                        </p>',
                'content_en' => '  <p>
                            <span>Экзамены</span> по математике (и наверняка – по физике и
                            химии) в университете <span>станут простыми</span>
                        </p>',
                'block' => \App\Models\Stud::GRADUATE
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => '',
                'title_en' => '',
                'content_ru' => '<p>
                            Если вы решите продолжить обучение в лучших университетах мира,
                            то вам не составит большого труда успешно и с легкостью сдать
                            такие экзамены как <span>SAT, GRE и GMAT</span>
                        </p>',
                'content_en' => '<p>
                            Если вы решите продолжить обучение в лучших университетах мира,
                            то вам не составит большого труда успешно и с легкостью сдать
                            такие экзамены как <span>SAT, GRE и GMAT</span>
                        </p>',
                'block' => \App\Models\Stud::GRADUATE
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => '',
                'title_en' => '',
                'content_ru' => '<p>
                            Если вы хотите получить работу в одной из крупнейших
                            международных HiTech-компаний, таких как Apple, Google, Dell,
                            Microsoft и IBM, то вам надо пройти тестирование, большая часть
                            которых требует глубоких математических знаний и навыков
                            нестандартного мышления.
                        </p>',
                'content_en' => '<p>
                            Если вы хотите получить работу в одной из крупнейших
                            международных HiTech-компаний, таких как Apple, Google, Dell,
                            Microsoft и IBM, то вам надо пройти тестирование, большая часть
                            которых требует глубоких математических знаний и навыков
                            нестандартного мышления.
                        </p>',
                'block' => \App\Models\Stud::GRADUATE
            ]
        );

        DB::table('studs')->insert(
            [
                'title_ru' => '',
                'title_en' => '',
                'content_ru' => '
                            Проанализировав эти тесты, Школа Высшей Математики подготовила
                            лекции, которые позволят вам успешно пройти отбор и осуществить
                            свою мечту.
                        ',
                'content_en' => '
                            Проанализировав эти тесты, Школа Высшей Математики подготовила
                            лекции, которые позволят вам успешно пройти отбор и осуществить
                            свою мечту.
                        ',
                'block' => \App\Models\Stud::GRADUATE
            ]
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studs');
    }
}
