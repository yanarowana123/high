<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->id();
            $table->string('title_ru')->nullable();
            $table->string('title_en')->nullable();
            $table->text('text_ru')->nullable();
            $table->text('text_en')->nullable();
            $table->longText('content_ru')->nullable();
            $table->longText('content_en')->nullable();
            $table->string('file')->nullable();
            $table->unsignedInteger('block');
            $table->timestamps();
        });

        DB::table('abouts')->insert(
            [
                'file' => 'img/School-page/depositphotos_203145164-stock-video-robot-hand-and-butterfly.mp4',
                'block' => \App\Models\About::VIDEO
            ]
        );

        DB::table('abouts')->insert(
            [
                'title_ru' => 'Красота математики',
                'title_en' => 'Красота математики',
                'content_ru' => 'Предлагаемые курсы Высшей Математики предназначены и нацелены на то, чтобы показать людям всю красоту математики и научить применять математику в жизни, на работе, дома.',
                'content_en' => 'Предлагаемые курсы Высшей Математики предназначены и нацелены на то, чтобы показать людям всю красоту математики и научить применять математику в жизни, на работе, дома.',
                'block' => \App\Models\About::BEAUTY
            ]
        );

        DB::table('abouts')->insert(
            [
                'title_ru' => 'Легко и просто',
                'title_en' => 'Легко и просто',
                'content_ru' => '        <p>
                        В течение жизни, в школе, в университете, вы сталкиваетесь с
                        математикой, но неумелое и формальное преподавание с большим
                        объёмом теорем, абстрактными задачами вызывает у вас неприятие и
                        стойкое мнение что Математика, а тем более Высшая Математика – это
                        очень сложно и мало кому нужно.
                    </p>
                    <p>
                        Мы нацелены на то, чтобы показать людям всю красоту и практичность
                        Высшей математики и научить применять математику в жизни.
                    </p>',
                'content_en' => '        <p>
                        В течение жизни, в школе, в университете, вы сталкиваетесь с
                        математикой, но неумелое и формальное преподавание с большим
                        объёмом теорем, абстрактными задачами вызывает у вас неприятие и
                        стойкое мнение что Математика, а тем более Высшая Математика – это
                        очень сложно и мало кому нужно.
                    </p>
                    <p>
                        Мы нацелены на то, чтобы показать людям всю красоту и практичность
                        Высшей математики и научить применять математику в жизни.
                    </p>',
                'block' => \App\Models\About::EASY
            ]
        );


        DB::table('abouts')->insert(
            [
                'content_ru' => 'Мы разработали для Вас 7 инновационных курсов с учетом специфики Вашей деятельности',
                'content_en' => 'Мы разработали для Вас 7 инновационных курсов с учетом специфики Вашей деятельности',
                'block' => \App\Models\About::DEV
            ]
        );

        DB::table('abouts')->insert(
            [
                'title_ru' => 'Высшая Математика
станет для Вас доступной и привлекательной',
                'title_en' => 'Высшая Математика
станет для Вас доступной и привлекательной',
                'block' => \App\Models\About::AVAILABLE
            ]
        );


        DB::table('abouts')->insert(
            [
                'title_ru' => 'В курсах нет тяжелых определений, формальных теорем и их доказательств',
                'title_en' => 'В курсах нет тяжелых определений, формальных теорем и их доказательств',
                'block' => \App\Models\About::FEATURE
            ]
        );

        DB::table('abouts')->insert(
            [
                'title_ru' => 'От Вас не не требуется наизусть знать все математические законы и формулы, но мы научим ими пользоваться',
                'title_en' => 'От Вас не не требуется наизусть знать все математические законы и формулы, но мы научим ими пользоваться',
                'block' => \App\Models\About::FEATURE
            ]
        );

        DB::table('abouts')->insert(
            [
                'title_ru' => 'Занятия ведутся в небольших группах, чтобы максимального овладеть материалом и доступно общаться с преподавателем',
                'title_en' => 'Занятия ведутся в небольших группах, чтобы максимального овладеть материалом и доступно общаться с преподавателем',
                'block' => \App\Models\About::FEATURE
            ]
        );

        DB::table('abouts')->insert(
            [
                'title_ru' => 'Набиев Самат Жаугаштиевич',
                'title_en' => 'Набиев Самат Жаугаштиевич',
                'content_ru' => '   <div class="school-staff__item-link">
                                PhD Математика, Профессор
                            </div>
                            <div class="school-staff__item-link">
                                Образование — математика, Ph.D. МГУ, Механико-математический
                                факультет, Москва
                            </div>
                            <div class="school-staff__item-link">
                                Организовал и руководил финансовыми, производственными и
                                строительными структурами:
                                <p>— «Standard Bank London» в Алматы</p>
                                <p>— БТА Лизинг</p>
                                <p>— Девелоперская компания «Алматы СитиСтрой»</p>
                                <p>— Завод напитков «Терра Нова»</p>
                            </div>
                            <div class="school-staff__item-link">
                                Разработал собственную Теорию «Математический Инжиниринг в
                                Экономике»
                            </div>
                            <div class="school-staff__item-link">
                                В настоящее время - профессор КБТУ, где преподает, в том
                                числе, Математический Инжиниринг в Экономике
                            </div>',
                'content_en' => '   <div class="school-staff__item-link">
                                PhD Математика, Профессор
                            </div>
                            <div class="school-staff__item-link">
                                Образование — математика, Ph.D. МГУ, Механико-математический
                                факультет, Москва
                            </div>
                            <div class="school-staff__item-link">
                                Организовал и руководил финансовыми, производственными и
                                строительными структурами:
                                <p>— «Standard Bank London» в Алматы</p>
                                <p>— БТА Лизинг</p>
                                <p>— Девелоперская компания «Алматы СитиСтрой»</p>
                                <p>— Завод напитков «Терра Нова»</p>
                            </div>
                            <div class="school-staff__item-link">
                                Разработал собственную Теорию «Математический Инжиниринг в
                                Экономике»
                            </div>
                            <div class="school-staff__item-link">
                                В настоящее время - профессор КБТУ, где преподает, в том
                                числе, Математический Инжиниринг в Экономике
                            </div>',
                'file' => 'img/School-page/staff-1.png',
                'block' => \App\Models\About::STAFF
            ]
        );


        DB::table('abouts')->insert(
            [
                'title_ru' => 'Сулейменова Лира Жусиповна',
                'title_en' => 'Сулейменова Лира Жусиповна',
                'content_ru' => '    <div class="school-staff__item-link">
                                Образование — математика, Механико-математический факультет,
                                Москва Факультет Нефти и Горного дела, Политехнический
                                Национальный Университет, Алматы
                            </div>
                            <div class="school-staff__item-link">
                                Была организатором и руководителем финансовых и
                                производственных структур:
                                <p>— Проектный анализ ЭксимБанк Казахстан</p>
                                <p>— Корпоративные финансы АТФ Банк,</p>
                                <p>— Завод по переработке газа «КазТрансГаз LNG»,</p>
                                <p>— Инвестиционная компания «Student Land Invest»</p>
                            </div>',
                'content_en' => '   <div class="school-staff__item-link">
                                Образование — математика, Механико-математический факультет,
                                Москва Факультет Нефти и Горного дела, Политехнический
                                Национальный Университет, Алматы
                            </div>
                            <div class="school-staff__item-link">
                                Была организатором и руководителем финансовых и
                                производственных структур:
                                <p>— Проектный анализ ЭксимБанк Казахстан</p>
                                <p>— Корпоративные финансы АТФ Банк,</p>
                                <p>— Завод по переработке газа «КазТрансГаз LNG»,</p>
                                <p>— Инвестиционная компания «Student Land Invest»</p>
                            </div>',
                'file' => 'img/School-page/staff-2.png',
                'block' => \App\Models\About::STAFF
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abouts');
    }
}
