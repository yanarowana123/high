<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->text('content_ru');
            $table->text('content_en');
            $table->unsignedInteger('type');
            $table->timestamps();
        });

        DB::table('contacts')->insert(
            [
                'content_ru' => '+7 705 111 23 34',
                'content_en' => '+7 705 111 23 34',
                'type' => \App\Models\Contact::MOBILE,
            ]
        );

        DB::table('contacts')->insert(
            [
                'content_ru' => 'Алматы, пр. Достык 202 (уг. Аль-Фараби), БЦ Форум',
                'content_en' => 'Алматы, пр. Достык 202 (уг. Аль-Фараби), БЦ Форум',
                'type' => \App\Models\Contact::ADDRESS,
            ]
        );

        DB::table('contacts')->insert(
            [
                'content_ru' => 'Предоставляется стоянка для слушателей школы',
                'content_en' => 'Предоставляется стоянка для слушателей школы',
                'type' => \App\Models\Contact::TEXT,
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
