<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class CreateHomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homes', function (Blueprint $table) {
            $table->id();
            $table->string('title_ru')->nullable();
            $table->string('title_en')->nullable();
            $table->text('text_ru')->nullable();
            $table->text('text_en')->nullable();
            $table->longText('content_ru')->nullable();
            $table->longText('content_en')->nullable();
            $table->string('file')->nullable();
            $table->unsignedInteger('block');
            $table->timestamps();
        });

        DB::table('homes')->insert(
            [
                'file' => 'img/depositphotos_268600780-stock-video-digital-composite-robotic-hand-holding.mp4',
                'block' => \App\Models\Home::VIDEO
            ]
        );

        DB::table('homes')->insert(
            [
                'text_ru' => 'HIGH MATH SCHOOL',
                'text_en' => 'HIGH MATH SCHOOL',
                'title_ru' => 'Школа Высшей Математики',
                'title_en' => Str::slug('Школа Высшей Математики'),
                'content_ru' => 'предлагает обучение Высшей математике в простой
и доступной форме профессионалам в любых областях',
                'content_en' => 'предлагает обучение Высшей математике в простой
и доступной форме профессионалам в любых областях',
                'block' => \App\Models\Home::WELCOME
            ]
        );

        DB::table('homes')->insert(
            [
                'title_ru' => 'Мы хотим помочь людям повысить свои знания',
                'title_en' => 'Мы хотим помочь людям повысить свои знания',
                'content_ru' => 'Получить новые возможности,
перейти на новый жизненный уровень.',
                'content_en' => Str::slug('Получить новые возможности,
перейти на новый жизненный уровень.'),
                'block' => \App\Models\Home::HELP
            ]
        );

        DB::table('homes')->insert(
            [
                'title_ru' => '7',
                'title_en' => '7',
                'content_ru' => 'инновационных курсов',
                'content_en' => Str::slug('инновационных курсов'),
                'block' => \App\Models\Home::FEATURE
            ]
        );

        DB::table('homes')->insert(
            [
                'title_ru' => '15',
                'title_en' => '15',
                'content_ru' => 'модулей высшей математики',
                'content_en' => Str::slug('модулей высшей математики'),
                'block' => \App\Models\Home::FEATURE
            ]
        );

        DB::table('homes')->insert(
            [
                'title_ru' => '48 ч.',
                'title_en' => '48 h.',
                'content_ru' => 'продолжительность курса',
                'content_en' => Str::slug('продолжительность курса'),
                'block' => \App\Models\Home::FEATURE
            ]
        );

        DB::table('homes')->insert(
            [
                'content_ru' => 'Занятия разработаны на основе курсов
Высшей Математики университетов',
                'content_en' => Str::slug('Занятия разработаны на основе курсов
Высшей Математики университетов'),
                'block' => \App\Models\Home::FOOTER
            ]
        );

        DB::table('homes')->insert(
            [
                'file' => 'img/Main-page/footer-img.png',
                'block' => \App\Models\Home::LOGO
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homes');
    }
}
