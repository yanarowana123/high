$(function () {
    let curStr = ".news-content__check1";
    $('.close_navbar').on('click', function () {
        $('.active__menu').fadeOut();
    });

    $('.hamburger').on('click', function (e) {
        e.preventDefault;
        $(this).toggleClass('hamburger_active');
        return;
    });

    $(".active__header-title").click(function () {
        if (!$(".active__header-display").is(":visible")) {
            $(".active__header-display").slideDown("slow");
        } else {
            $(".active__header-display").slideUp("slow");
        }

    });

    $('.hamburger').click(function () {
        if ($(".active__menu").is(":visible") == true) {
            $('.active__menu').hide();
            $('.header').removeClass("slick_active")
            $(".cover_class").css({
                "position": "relative"
            })
            $("body").css("overflow", "scroll");

        } else {
            $('.active__menu').show();
            $(".header").addClass("slick_active")
            $(".cover_class").css({
                "background": "rgba(0, 0, 0, 0.1)",
                "backdrop-filter": "blur(14px)",
                "position": "absolute",
                "width": "100%",
                "min-height": "100%",
                "z-index": "90"
            })
            $("body").css("overflow", "hidden");
        }

    });

    $('.questions__item-title').on('click', function () {
        $('.questions__item').removeClass('questions__item--active');
        $(this).parent().addClass('questions__item--active');
    });

    $('.news-content__btn').on('click', function (index) {
        let cls = "." + index.target.classList[0].toString();
        $(curStr).removeClass("news-content__btn-active");
        $(cls).addClass("news-content__btn-active");
        curStr = cls;
        let curName = $(cls).text().trim().toString();

        if (curName === "Все") {
            $(".news_content_reports").fadeIn();
            $(".news_content_news").fadeIn();
        }
        else if (curName === "Статьи") {
            $(".news_content_reports").fadeOut();
            $(".news_content_news").fadeIn();
        }
        else {
            $(".news_content_reports").fadeIn();
            $(".news_content_news").fadeOut();
        }
    });


    $(window).scroll(function () {
        var elemScrollTop = $(window).scrollTop();
        var blockHeight = $('.header').height() + $('.main').height();

        if (!$('.active__menu').is(':visible')) {
            if (elemScrollTop >= (blockHeight / 3)) {
                $('.header').addClass('slick');
            } else {
                $('.header').removeClass('slick');
            }
        }
    });

});
