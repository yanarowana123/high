<?php
$isHomeActive = request()->route()->parameter('name') =='home'
    || request()->route()->named('admin.home.*')
    ||request()->route()->parameter('model') =='home';

$isAboutActive = request()->route()->parameter('name') =='about'
    || request()->route()->named('admin.about.*')
    ||request()->route()->parameter('model') =='about';

$isFinActive = request()->route()->parameter('name') =='fin'
    || request()->route()->named('admin.fin.*')
    ||request()->route()->parameter('model') =='fin';

$isStudActive = request()->route()->parameter('name') =='stud'
    || request()->route()->named('admin.stud.*')
    ||request()->route()->parameter('model') =='stud';
